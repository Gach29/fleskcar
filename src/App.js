import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Home from './Home';
import Sell from './pages/Sell/Sell';
import { Route, Routes } from 'react-router-dom';
import About from './components/About/About';
import Car from './components/Cars/Car';
import Expert from './components/Expert/Expert';
import FormExpert from './pages/FormExpert/FormExpert';
import SellForm from './pages/SellForm/SellForm';
import Profil from './components/Profil/Profil';
import Paiement from './pages/ProfilDetails/Paiement/Paiement';
import Historique from './pages/ProfilDetails/Historique/Historique'
import ModelLogin from './components/ModelLogin/ModelLogin';
import ListCars from './components/ListCars/ListCars';
import GestionProfil from './pages/ProfilDetails/GestionProfil/GestionProfil';
import { AuthProvider } from './context/AuthContext';
import Notify from './pages/ProfilDetails/Notifications/Notify';
import Favoris from './pages/ProfilDetails/Favoris/Favoris';
import Annonces from './pages/ProfilDetails/Annonces/Annonces';
import Demandes from './pages/ProfilDetails/Demandes/Demandes';
import Fail from './pages/Payment/Fail';
import Success from './pages/Payment/Success';

function App() {  
  return (
    <AuthProvider>
      <div>
        <Routes>
          <Route path='/' element={<Home />} />
          <Route path='/about' element={<About />} />
          <Route path='/anonces' element={<Car />} />
          <Route path='/voitures' element={<ListCars />} />
          <Route path='/expert' element={<Expert />} />
          <Route path='/connexion' element={<ModelLogin />} />
          <Route path='/voitures/:name' element={<Sell />} />
          <Route path="/devenirexpert" element={<FormExpert />} />
          <Route path="/vendrevoiture" element={<SellForm />} />
          <Route path="/profil" element={<Profil />}/>
          <Route path="/profilpayment" element={<Paiement />} />
          <Route path="/historique" element={<Historique />} />
          <Route path="/donneesprofil" element={<GestionProfil />} />
          <Route path="/notifications" element={<Notify />} />
          <Route path="/favoris" element={<Favoris />} />
          <Route path="/mesannonces" element={<Annonces />} />
          <Route path="/mesdemandes" element={<Demandes />} />
          <Route path="/successpayment" element={<Success/>} />
          <Route path="/failpayment" element={<Fail />} />


        </Routes>
      </div>
    </AuthProvider>
  );
}

export default App;
