/* eslint-disable no-unused-vars */
import React, { useState, useEffect, useCallback } from 'react';
import './Comments.css';
import user from '../../images/user.png'

import axios from 'axios';

const Comments = () => {
    const [commentContent, setCommentContent] = useState('');
    const [replyContent, setReplyContent] = useState('');
    const [comments, setComments] = useState([]);
    const [userData, setUserData] = useState(null);
    const [commentError, setCommentError] = useState('');
    const [replyingToCommentId, setReplyingToCommentId] = useState(null);
    const [replyingToReplyId, setReplyingToReplyId] = useState(null); 
    const [updatingCommentId, setUpdatingCommentId] = useState(null);
    const [updatedCommentContent, setUpdatedCommentContent] = useState('');
    const [loggedInUserId, setLoggedInUserId] = useState('');



    useEffect(() => {
        fetchData();
        const userId = localStorage.getItem('userId');
        setLoggedInUserId(userId);
    }, []);

    
    const fetchData = async () => {
        try {
            const userId = localStorage.getItem('userId');
            const postId = localStorage.getItem('postId');
            if (userId) {
                const response = await axios.get(`http://localhost:8000/auth/users/${userId}`);
                setUserData(response.data);
            }
            const commentsResponse = await axios.get(`http://localhost:8000/comments/${postId}`);
            setComments(commentsResponse.data);
        } catch (error) {
            console.error('Error fetching data:', error);
        }
    };



    const handleCommentChange = (event) => {
        setCommentContent(event.target.value);
    };

    const handleReplyChange = (event) => {
        setReplyContent(event.target.value);
    };

    const formatDateTime = (dateString) => {
        const date = new Date(dateString);
        const formattedDate = date.toLocaleDateString('fr-FR');
        const formattedTime = date.toLocaleTimeString('fr-FR', { hour: '2-digit', minute: '2-digit' });
        return `${formattedDate} ${formattedTime}`;
    };

    const handleSubmit = async (event) => {
        event.preventDefault();

        try {
            const token = localStorage.getItem('token');
            const postId = localStorage.getItem('postId');

            if (!token) {
                throw new Error("Vous devez être connecté pour commenter.");
            }

            const newData = {
                content: commentContent,
                postId: postId,
            };

            setCommentContent('');

            await axios.post('http://localhost:8000/comments/create', newData, {
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            });

            fetchData(); 

            console.log('Le commentaire a été créé avec succès!');
        } catch (error) {
            console.error('Une erreur est survenue :', error);
            setCommentError(error.message);
        }
    };

    const handleReplySubmit = async (commentId, event) => {
        event.preventDefault();
        
        try {
            const token = localStorage.getItem('token');
            const postId = localStorage.getItem('postId');
    
            if (!token) {
                throw new Error("Vous devez être connecté pour commenter.");
            }
    
            const newData = {
                content: replyContent,
                postId: postId,
                parentCommentId: commentId, 
            };
    
            setReplyContent('');
    
            await axios.post('http://localhost:8000/comments/create', newData, {
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            });
    
            const updatedComments = comments.map(comment => {
                if (comment._id === commentId) {
                    return {
                        ...comment,
                        replies: [...(comment.replies || []), { content: newData.content, createdAt: new Date().toISOString() }]
                    };
                }
                return comment;
            });
            setComments(updatedComments);
            fetchData(); 
    
            console.log('La réponse a été créée avec succès!');
        } catch (error) {
            console.error('Une erreur est survenue lors de la réponse :', error);
            setCommentError(error.message);
        }
    };

    const deleteComment = async (commentId, userId) => {
        if (userId === loggedInUserId) {
            try {
                const token = localStorage.getItem('token');
                const postId = localStorage.getItem('postId');
                if (!token) {
                    throw new Error("Vous devez être connecté pour supprimer un commentaire.");
                }

                await axios.delete(`http://localhost:8000/comments/delete/${commentId}`, {
                    headers: {
                        'Authorization': `Bearer ${token}`
                    },
                    data: {
                        postId: postId
                    }
                });

                setComments(comments.filter(comment => comment._id !== commentId));

                console.log('Le commentaire a été supprimé avec succès!');
            } catch (error) {
                console.error('Une erreur est survenue lors de la suppression du commentaire :', error);
            }
        } else {
            alert('Vous n\'êtes pas autorisé à supprimer ce commentaire.');
        }
    };


    const handleUpdate = async (commentId) => {
        try {
            const token = localStorage.getItem('token');
            const postId = localStorage.getItem('postId');
            
            if (!token) {
                throw new Error("Vous devez être connecté pour mettre à jour un commentaire.");
            }

            const updatedData = {
                content: updatedCommentContent,
                postId: postId,
            };

            await axios.put(`http://localhost:8000/comments/update/${commentId}`, updatedData, {
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            });

            fetchData();

            setUpdatedCommentContent('');
            setUpdatingCommentId(null);

            console.log('Le commentaire a été mis à jour avec succès!');
        } catch (error) {
            console.error('Une erreur est survenue lors de la mise à jour du commentaire :', error);
        }
    };

    const handleCommentContentChange = (event) => {
        setUpdatedCommentContent(event.target.value);
    };

    const handleEditClick = (commentId, userId) => {
        if (userId === loggedInUserId) {
            setUpdatingCommentId(commentId);
        } else {
            alert('Vous n\'êtes pas autorisé à modifier ce commentaire.');
        }
    };


    useEffect(() => {
        if (updatingCommentId !== null) {
            const commentToUpdate = comments.find(comment => comment._id === updatingCommentId);
            if (commentToUpdate) {
                setUpdatedCommentContent(commentToUpdate.content);
            }
        }
    }, [updatingCommentId, comments]);
    

    const handleReplyClick = (commentId, isReply) => {
        if (isReply) {
            setReplyingToReplyId(commentId); 
        } else {
            setReplyingToCommentId(commentId); 
        }
    };

    const handleCancelReply = () => {
        setReplyingToCommentId(null);
        setReplyingToReplyId(null); 
    };

    const handleCancelEdit = () => {
        setUpdatedCommentContent(null);
        setUpdatingCommentId(null); 
    };

    const handleFavorite = async (commentId) => {
        try {
            const token = localStorage.getItem('token');
            const userId = localStorage.getItem('userId');
    
            if (!token || !userId) {
                throw new Error("Vous devez être connecté pour ajouter un commentaire aux favoris.");
            }
    
            const hasLiked = comments.find(comment => comment._id === commentId && comment.favorites.includes(userId));
    
            if (hasLiked) {
                console.log("Vous avez déjà aimé ce commentaire.");
                return;
            }
    
            await axios.post(`http://localhost:8000/comments/${commentId}/favorite`, null, {
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            });
    
            setComments(prevComments => {
                return prevComments.map(comment => {
                    if (comment._id === commentId) {
                        return {
                            ...comment,
                            favorites: [...comment.favorites, userId]
                        };
                    }
                    return comment;
                });
            });
    
            console.log('Le commentaire a été ajouté aux favoris avec succès!');
        } catch (error) {
            console.error('Une erreur est survenue lors de l\'ajout du commentaire aux favoris :', error);
        }
    };
    
    

    // const handleUnfavorite = async (commentId) => {
    //     try {
    //         const token = localStorage.getItem('token');
    //         const userId = localStorage.getItem('userId');
    
    //         if (!token || !userId) {
    //             throw new Error("Vous devez être connecté pour supprimer un commentaire des favoris.");
    //         }
    
    //         await axios.delete(`http://localhost:8000/comments/${commentId}/unfavorite`, {
    //             headers: {
    //                 'Authorization': `Bearer ${token}`
    //             }
    //         });
    
    //         setComments(prevComments => {
    //             return prevComments.map(comment => {
    //                 if (comment._id === commentId) {
    //                     return {
    //                         ...comment,
    //                         favorites: comment.favorites.filter(fav => fav !== userId)
    //                     };
    //                 }
    //                 return comment;
    //             });
    //         });
    
    //         console.log('Le commentaire a été supprimé des favoris avec succès!');
    //     } catch (error) {
    //         console.error('Une erreur est survenue lors de la suppression du commentaire des favoris :', error);
    //     }
    // };
    
    

    return (
        <section className="content-item" id="comments">
            <div className="container">
                <div className="row">
                    <div className="col-lg-12">
                        <form className='create-comment' onSubmit={handleSubmit}>
                            <fieldset>
                                <div className="row">
                                    <div className="form-group col-xs-12 col-sm-9 col-lg-12 ">
                                        <textarea className="form-control" id="message" placeholder="Participer à la discussion" required="" value={commentContent} onChange={handleCommentChange}></textarea>
                                    </div>
                                </div>
                                <button type="submit" className="btn btn-normal pull-right">Envoyer</button>
                            </fieldset>
                            {commentError && <span className="error-box"><i className="bi bi-exclamation-triangle-fill" style={{ color: "#c31111", paddingRight: "10px" }}></i>{commentError}</span>}
                            </form>
                            {comments.length > 0 ? (
                            <div>
                                <h3 className='ps-5 comments-list'>{comments.length} Commentaires</h3>
                                {comments.map((comment, index) => (
                                <div key={index} className="media">
                                    <div className="media-body">
                                        <a className="pull-left" href><img className="media-object ps-2" style={{marginRight:'20px',width: '100px', height: '100px', borderRadius:'50%' }} src={comment.user && comment.user.profileImage ? `http://localhost:8000/auth/pictures/${comment.user.profileImage}` : user} alt="" /></a>
                                        <h4 className="media-heading">{comment.user ? comment.user.name : 'Utilisateur inconnu'}</h4>
                                        <p>{comment.content}</p>
                                        <ul className="list-unstyled list-inline media-detail pull-left">
                                            <li><i className="fa fa-pencil fa-fw" onClick={() => handleEditClick(comment._id, comment.user._id)}></i></li>
                                            <li><a onClick={() => deleteComment(comment._id, comment.user._id, comment.postId)} href><i className="fa fa-trash"></i></a></li>
                                            <li className="" style={{ cursor: "pointer" }}><a onClick={() => handleReplyClick(comment._id)} href><i class="fa fa-reply"></i></a></li>
                                            <li><i className="fa fa-thumbs-up" onClick={() => handleFavorite(comment._id)}></i>{comment.favorites.length}</li>
                                            {/* <li><i className="fa fa-thumbs-down" onClick={() => handleUnfavorite(comment._id)}></i></li> */}
                                        </ul>
                                        <ul className="list-unstyled list-inline media-detail pull-right ">
                                            {/* <li><i className="fa fa-calendar"></i>{formatDateTime(comment.createdAt)}</li> */}
                                            <li>{formatDateTime(comment.createdAt)}</li>
                                        </ul>
                                        {updatingCommentId === comment._id && (
                                            <form onSubmit={() => handleUpdate(comment._id)}>
                                                <div className="form-group">
                                                    <textarea className="form-control" style={{width:"86%"}} value={updatedCommentContent} onChange={handleCommentContentChange}></textarea>
                                                </div>
                                                <button type="submit" className="btn btn-normal pull-right" style={{ marginRight: '75px' }}>Enregistrer</button>
                                                <button type="button" className="btn btn-normal pull-right" style={{ marginRight: '10px' }} onClick={handleCancelEdit}>Annuler</button>
                                            </form>
                                        )}
                                        {replyingToCommentId === comment._id && (
                                            <form className="reply-form" onSubmit={(e) => handleReplySubmit(comment._id, e)}>
                                                <div className="form-group">
                                                    <textarea className="form-control"  style={{width:"86%"}} placeholder="Votre réponse" value={replyContent} onChange={handleReplyChange}></textarea>
                                                </div>
                                                <button type="submit" className="btn btn-normal pull-right" style={{ marginRight: '75px' }}>Envoyer</button>
                                                <button type="button" className="btn btn-normal pull-right" style={{ marginRight: '10px' }} onClick={handleCancelReply}>Annuler</button>
                                            </form>
                                        )}
                                        {/* Affichage des réponses */}
                                        {comment.replies && comment.replies.map((reply, replyIndex) => (
                                            <div key={replyIndex} className="media mt-5 ml-5">
                                                <div className="media-body">
                                                    <a className="pull-left" href><img className="media-object ps-2" style={{marginRight:'20px',width: '100px', height: '100px', borderRadius:'50%' }} src={reply.user && reply.user.profileImage ? `http://localhost:8000/auth/pictures/${reply.user.profileImage}` : user} alt="" /></a>
                                                    <h4 className="media-heading">{reply.user ? reply.user.name : 'Utilisateur inconnu'}</h4>
                                                    <p>{reply.content}</p>
                                                    <ul className="list-unstyled list-inline media-detail pull-left">
                                                        <li><i className="fa fa-pencil fa-fw"></i></li>
                                                        <li><a onClick={() => deleteComment(reply._id, comment.postId)} href><i className="fa fa-trash"></i></a></li>                                                
                                                        <li className="" style={{ cursor: "pointer" }}><a onClick={() => handleReplyClick(reply._id)} href><i class="fa fa-reply"></i></a></li>
                                                        <li><i className="fa fa-thumbs-up" onClick={() => handleFavorite(reply._id)}></i>{reply.favorites.length}</li>
                                                        {/* <li><i className="fa fa-thumbs-down" onClick={() => handleUnfavorite(reply._id)}></i></li> */}
                                                    </ul>
                                                    <ul className="list-unstyled list-inline media-detail pull-right">
                                                        {/* <li><i className="fa fa-calendar"></i>{formatDateTime(reply.createdAt)}</li> */}
                                                        <li>{formatDateTime(reply.createdAt)}</li>
                                                    </ul>
                                                    {replyingToReplyId === reply._id && (
                                                        <form className="reply-form" onSubmit={(e) => handleReplySubmit(reply._id, e)}>
                                                            <div className="form-group">
                                                                <textarea className="form-control" style={{width:"86%"}} placeholder="Votre réponse" value={replyContent} onChange={handleReplyChange}></textarea>
                                                            </div>
                                                            <button type="submit" className="btn btn-normal pull-right" style={{ marginRight: '75px' }}>Envoyer</button>
                                                            <button type="button" className="btn btn-normal pull-right" style={{ marginRight: '10px' }} onClick={handleCancelReply}>Annuler</button>
                                                        </form>
                                                    )}
                                                </div>
                                            </div>
                                        ))}
                                    </div>
                                    
                                </div>
                                ))}
                            </div>
                            ) : (
                                <div className="container">
                                    <p className="ps-5 comments-list">Aucun commentaire pour le moment.</p>
                                </div>
                            )}

                        </div>
                    </div>
                </div>
            </section>
        );
    };

export default Comments;
