import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Stepper, Step, StepLabel } from '@material-ui/core';
import Step1 from '../Steps/Step1';
import Step2 from '../Steps/Step2';
import Step3 from '../Steps/Step3';
import Success from '../Steps/Success';
import Header from '../../components/Header/Header';
import axios from 'axios';

const useStyles = makeStyles({
    root: {
        width: "60%",
        fontSize: "30px",
        margin: "10rem auto",
        paddingTop: "3rem",
        boxShadow: "-10px -5px 40px 0 rgba(92, 92, 92, 0.1)",
        "& .MuiStepIcon-root.MuiStepIcon-active": {
            color: "#cc7b3d",
        },
        "& .MuiStepIcon-root.MuiStepIcon-completed": {
            color: "#cc7b3d",
        },

        "& .MuiStepIcon-root.MuiStepIcon-disabled": {
            fontSize: "40px"
        },
        "& .MuiStepLabel-label": {
            fontSize: "16px",
            fontWeight: "bold",
            color: "#cc7b3d"
        },

    }
});

const SellForm = () => {
    const [activeStep, setActiveStep] = useState(0);
    const [formData, setFormData] = useState({
        marque: '',
        annee:0,
        modele: '',
        energie: '',
        boiteDeVitesse: '',
        nombrePortes:0,
        kilometrage:0,
        couleurExterieure: '',
        couleurInterieure: '',
        age:0,
        carrosserie:'',
        transmission:'',
        puissance:'',
        sellerie:'',
        location:'',
        files:[],
        selectedOptions:[],
        prix:0,
    });

    // // eslint-disable-next-line no-unused-vars
    // const [filesFromStep2, setFilesFromStep2] = useState([])


    // // eslint-disable-next-line no-unused-vars
    // const [selectedOptions, setSelectedOptions] = useState({
    //     entretien: '',
    //     moteur: '',
    //     boiteDeVitesse: '',
    //     voyants: '',
    //     accident: ''
    // });

    const [files,setFiles]=useState([])
    const [selectedOptions, setSelectedOptions] = useState([])

    const [prix,setPrix] = useState(100000);
    
    

    const handleNext = () => {
        setActiveStep(prevActiveStep => prevActiveStep + 1);
    };

    const handlePrevious = () => {
        setActiveStep(prevActiveStep => prevActiveStep - 1);
    };

    const handleFormDataChange = (data) => {
        setFormData({ ...formData, ...data });
    };

    const saveFormData = (data) => {
        console.log("Données du formulaire sauvegardées :", data);
        setFormData(data);
    };


    const createPost = async () => {
        const token = localStorage.getItem('token');
        if (!token) {
            throw new Error("Vous devez être connecté pour créer une publication.");
        }
    
        const formattedData = {
            ...formData,
            selectedOptions: Array.isArray(selectedOptions) ? selectedOptions : [selectedOptions],
            files:  Object.keys(files)?.length ? Object.values(files).map((v) => {return v}) : files,

            // files:  typeof files === 'object' && files?.length ? files : [files]
        };
        console.log(formattedData)
    
        if (files.length === 0) {
            console.log("Aucun fichier sélectionné.");
        } else {
            console.log("Fichier sélectionné.");
        }
    
        try {
            const response = await axios.post('http://localhost:8000/posts/create', formattedData, {
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            });
            const post = response.data.newPost._id; 
            console.log(post)
            localStorage.setItem('post', post);
        } catch (error) {
            console.error('Erreur lors de la création de la publication de la voiture :', error);
        }
    };
    
    

    const steps = ['Caractéristiques du véhicule', 'Etat du véhicule', 'Estimation' , 'Annonce'];

    const classes = useStyles();

    return (
        <>
            <Header activeLink="vente" setActiveLink={() => { }} />
            <div className={classes.root}>
                <Stepper activeStep={activeStep} alternativeLabel>
                    {steps.map(label => (
                        <Step key={label}>
                            <StepLabel>
                                {label}
                            </StepLabel>
                        </Step>
                    ))}
                </Stepper>
                <>
                    {activeStep === 3 ? (
                        <Success
                            formData={formData} 
                            prix={prix}
                        />
                    ) : (
                        <>
                            {activeStep === 0 && (
                                <Step1
                                    handleNext={handleNext}
                                    activeStep={activeStep}
                                    steps={steps}
                                    formData={formData}
                                    handleFormDataChange={handleFormDataChange}
                                    saveFormData={saveFormData}
                                />
                            )}
                            {activeStep === 1 && (
                                <Step2
                                handleNext={handleNext}
                                handlePrevious={handlePrevious}
                                activeStep={activeStep}
                                steps={steps}
                                formData={formData}
                                saveFormData={saveFormData}
                                handleFormDataChange={handleFormDataChange}
                                files={files}
                                setFiles={setFiles}
                                selectedOptions={selectedOptions}
                                setSelectedOptions={setSelectedOptions}
                            />
                            )}
                            {activeStep === 2 && (
                                <Step3
                                    handleNext={handleNext}
                                    handlePrevious={handlePrevious}
                                    activeStep={activeStep}
                                    steps={steps}
                                    formData={formData}
                                    handleFormDataChange={handleFormDataChange}
                                    saveFormData={saveFormData}
                                    files={files}
                                    setFiles={setFiles}
                                    selectedOptions={selectedOptions}
                                    setSelectedOptions={setSelectedOptions}
                                    createPost={createPost}
                                    prix={prix}
                                    setPrix={setPrix}
                                    />
                            )}
                        </>
                    )}
                </>
            </div>
        </>
    );
};

export default SellForm;
