/* eslint-disable no-unused-vars */
import React, { useEffect, useRef, useState } from 'react'
import user from '../../../images/user.png'
import './Notify.css';
import { Link }from 'react-router-dom'
import Header from '../../../components/Header/Header';
import Footer from '../../../components/Footer/Footer'
import axios from 'axios'


const Notify = () => {
    const [formData, setUserData] = useState(null);
    const [notifications, setNotifications] = useState([]);

    useEffect(() => {
        const fetchData = async () => {
            try {
                const userId = localStorage.getItem('userId');
                if (userId) {
                    const userResponse = await axios.get(`http://localhost:8000/auth/users/${userId}`);
                    setUserData(userResponse.data);

                    const notificationsResponse = await axios.get(`http://localhost:8000/notifications/${userId}`);
                    console.log(notificationsResponse)
                    setNotifications(notificationsResponse.data);
                }
            } catch (error) {
                console.log(error.message);
            }
        };

        fetchData();
    }, []);


    const deleteNotification = async (notificationId) => {
        try {
            const token = localStorage.getItem('token'); 
            await axios.delete(`http://localhost:8000/notifications/${notificationId}`, {
                headers: {
                    'Authorization': `Bearer ${token}` 
                }
            });
            setNotifications(notifications.filter(notification => notification._id !== notificationId));
        } catch (error) {
            console.log(error);
        }
    };


    const inputRef = useRef(null);


    const uploadImage = async (file) => {
        const formData = new FormData();
        formData.append('file', file);

        try {
            const token = localStorage.getItem('token'); 
            const response = await axios.post('http://localhost:8000/auth/upload', formData, {
            headers: {
                'Content-Type': 'multipart/form-data',
                'Authorization': `Bearer ${token}` 
            }
            });
            console.log(response.data);
            setUserData({ ...formData, profileImage: response.data.profileImage });
        } catch (error) {
            console.log(error);
        }
    };

    const handleImageClick = () => {
        inputRef.current.click();
    };

    const handleImageChange = (event) => {
        const file = event.target.files[0];
        console.log(file);
        uploadImage(file);
    };

    const [activeItem, setActiveItem] = useState('notifs');

    const handleItemClick = (itemName) => {
        setActiveItem(itemName);
    };
    return (
        <>
            <Header />
            <section id="notify">
                <div className="container py-5 mt-5">
                    <div className="row">
                        <div className="col-lg-4">
                            <div className="card1" style={{ height: "auto" }}>
                                <div className="card1-body text-center" style={{ paddingTop: '5px', paddingBottom: '5px' }}>
                                    <div className='profimg d-flex align-items-center mb-4' style={{ position: 'relative' }}>
                                        <img
                                            src={formData && formData.profileImage ? `http://localhost:8000/auth/pictures/${formData.profileImage}` : user}
                                            alt="avatar"
                                            className="rounded-circle img-fluid me-2"
                                            style={{ width: '110px', height: '100px', border: '2.5px solid #eea943' }}
                                        />
                                        <div>
                                            <h5 className="my-3">{formData && formData.name}</h5>
                                        </div>
                                    </div>
                                    <input type="file" ref={inputRef} onChange={handleImageChange} style={{ display: "none" }} />
                                    <ul className="list-group list-group-flush rounded-3" style={{margin: '0', padding: '0'}}>
                                    <li className={`list-group-item d-flex align-items-center border-0 ${activeItem === 'profil' ? 'active' : ''}`} 
                                        onClick={() => handleItemClick('profil')}>
                                        <i class="fa-solid fa-gauge-high p-2"></i>
                                        <Link to='/profil' className="link mb-0">Tableau de bord</Link>
                                    </li>
                                    <li className={`list-group-item d-flex align-items-center border-0 ${activeItem === 'mesads' ? 'active' : ''}`} 
                                        onClick={() => handleItemClick('mesads')}>
                                        <i class="fa-solid fa-car-rear p-2 "></i> 
                                        <Link className="link mb-0 ">Annonces</Link>
                                    </li>
                                    <li className={`list-group-item d-flex align-items-center border-0 ${activeItem === 'profilpayment' ? 'active' : ''}`} 
                                        onClick={() => handleItemClick('profilpayment')}>
                                        <i class="fa-solid fa-coins p-2 "></i>                                        
                                        <Link to='/profilpayment' className=" link mb-0 ">Paiement</Link>
                                    </li>
                                    <li className={`list-group-item d-flex align-items-center border-0 ${activeItem === 'favoris' ? 'active' : ''}`} 
                                        onClick={() => handleItemClick('favoris')}>
                                        <i class="fa-solid fa-heart p-2 "></i>         
                                        <Link className=" link mb-0 ">Favoris</Link>
                                    </li>
                                    <li className={`list-group-item d-flex align-items-center border-0 ${activeItem === 'historique' ? 'active' : ''}`} 
                                        onClick={() => handleItemClick('historique')}>
                                        <i class="fa-solid fa-clock-rotate-left p-2 "></i>
                                        <Link to='/historique' className="link mb-0 ">Historique d'achat</Link>
                                    </li>
                                    <li className={`list-group-item d-flex align-items-center border-0 ${activeItem === 'notifs' ? 'active' : ''}`} 
                                        onClick={() => handleItemClick('notifs')}>
                                        <i class="fa-solid fa-bell p-2"></i>         
                                        <Link to='/notifications' className=" link mb-0 ">Notifications</Link>
                                    </li>
                                    <li className={`list-group-item d-flex align-items-center border-0 ${activeItem === 'messages' ? 'active' : ''}`} 
                                        onClick={() => handleItemClick('messages')}>
                                        <i class="fa-solid fa-message p-2 "></i>       
                                        <Link className="link mb-0 ">Messages</Link>
                                    </li>
                                    <li className={`list-group-item d-flex align-items-center border-0 ${activeItem === 'donneesprofil' ? 'active' : ''}`} 
                                        onClick={() => handleItemClick('donneesprofil')}>
                                        <i class="fa-solid fa-user-pen p-2 "></i>   
                                        <Link to="/donneesprofil" className="link mb-0">Profil</Link>
                                    </li>
                                </ul>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-8">
                            <div className="card1 mb-4 mb-8">
                                <div className="">
                                <ul class="list-group list-group-flush">
                                        {/* Notifications list */}
                                        {notifications.map(notification => (
                                            <li key={notification._id} className="list-group-item p-3">
                                                <div className="d-flex justify-content-between align-items-center">
                                                    <div className="d-flex align-items-center">
                                                        {/* <div>
                                                            <img src={notification.user && notification.user.profileImage `http://localhost:8000/auth/pictures/${notification.user.profileImage}`} style={{marginRight:'20px',width: '60px', height: '60px', borderRadius:'50%' }}alt="" className="avatar-sm rounded-circle" />
                                                        </div> */}
                                                        <div className="ms-3">
                                                            <p className="mb-0 font-weight-medium">{notification.content}</p>
                                                        </div>
                                                    </div>
                                                    <div>
                                                    {/* <div className="btn-group">
                                                        <a class="btn btn-secondary dropdown-toggle" href role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                                            <a href="#!" className="btn btn-ghost btn-icon btn-sm rounded-circle" aria-haspopup="true" aria-expanded="false">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" className="feather feather-more-vertical icon-xs"><circle cx="12" cy="12" r="1"></circle><circle cx="12" cy="5" r="1"></circle><circle cx="12" cy="19" r="1"></circle></svg>
                                                            </a>
                                                        </a> 
                                                        <ul class="dropdown-menu">
                                                            <li><a class="dropdown-item" href>Action</a></li>
                                                            <li><a className="dropdown-item d-flex align-items-center" href="#!" onClick={() => deleteNotification(notification._id)}>Supprimer</a></li>
                                                            <li><a class="dropdown-item" href>Another action</a></li>
                                                        </ul>
                                                    </div> */}
                                                    <a className="dropdown-item d-flex align-items-center" href="#supprimer" onClick={() => deleteNotification(notification._id)}><i className='fa fa-trash'></i></a>
                                                    </div>
                                                </div>
                                            </li>
                                        ))}
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <Footer />
        </>
    )
}

export default Notify;
