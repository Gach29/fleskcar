/* eslint-disable no-unused-vars */
import axios from 'axios';
import React, { useEffect, useRef, useState } from 'react'
import user from '../../../images/user.png'
import {FaPen } from 'react-icons/fa';
import Header from '../Header/ProfilHeader'
import './GestionProfil.css'
import { Link } from 'react-router-dom';

const GestionProfil = () => {

    
    const [formData, setFormData] = useState(null);
    const [isSaved, setIsSaved] = useState(false);
    const [isEditing, setIsEditing] = useState(false);

    useEffect(() => {
        const fetchData = async () => {
            try {
                const userId = localStorage.getItem('userId');
                if (userId) {
                    const response = await axios.get(`http://localhost:8000/auth/users/${userId}`); 
                    setFormData(response.data);
                } 
            } catch (error) {
                console.log(error.message);
            }
        };
    
        fetchData();
    }, []);


    const [activeItem, setActiveItem] = useState('donneesprofil');

    const handleItemClick = (itemName) => {
        setActiveItem(itemName);
    };

    const [imageUrls, setImageUrls] = useState([]);
    const [message, setMessage] = useState('');
    const [imagePreview, setImagePreview] = useState(null);
    

    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            const userId = localStorage.getItem('userId');
            if (userId) {
                await axios.put(`http://localhost:8000/auth/${userId}`, formData);
                setIsEditing(false);
                setIsSaved(true); 
            }
        } catch (error) {
            console.log(error.message);
        }
    };


    const handleSubmitPassword = async (e) => {
        e.preventDefault();
        try {
            const userId = localStorage.getItem('userId');           
            const response = await axios.put(`http://localhost:8000/auth/${userId}/password` , formData)
            setIsEditing(false);
            setIsSaved(true);
            console.log(response.data); 
        } catch (error) {
            console.error(error.response.data); 
        }
    };


    const handleEdit = (e) => {
        e.preventDefault();
        setIsEditing(true);
        setIsSaved(false);
    };

    const inputRef = useRef(null);

    const uploadImage = async (file) => {
        const formData = new FormData();
        formData.append('file', file);
    
        try {
            const token = localStorage.getItem('token');
            const response = await axios.post('http://localhost:8000/auth/upload', formData, {
                headers: {
                    'Content-Type': 'multipart/form-data',
                    'Authorization': `Bearer ${token}`
                }
            });
            console.log(response.data);
            setFormData({ ...formData, profileImage: response.data.filename }); 
        } catch (error) {
            console.log(error);
        }
    };
    
    const handleChange = (e) => {
        const { name, value, files } = e.target;
        if (files) {
            const file = files[0];
            uploadImage(file);
        } else {
            setFormData({ ...formData, [name]: value });
        }
    };

    const handleImageClick = () => {
        inputRef.current.click();
    };

    const handleImageChange = (event) => {
        const file = event.target.files[0];
        console.log(file);
        uploadImage(file)

    };
    



    const updateUserProfile = async () => {
        try {
            const userId = localStorage.getItem('userId');
            if (userId) {
                await axios.put(`http://localhost:8000/auth/${userId}`, formData); 
                setIsEditing(false);
                setIsSaved(true);
            }
        } catch (error) {
            console.log(error.message);
        }
    };

    const resetForm = () => {
        setFormData({
            password: '',
            newPassword: '',
            confirmPassword: '',
        });
    };

    const updateUserPassword = async () => {
        try {
            const userId = localStorage.getItem('userId');
            if (userId) {
                await axios.put(`http://localhost:8000/auth/${userId}/password`, formData); 
                setIsEditing(false);
                setIsSaved(true);
                resetForm(); 
            }
        } catch (error) {
            console.log(error.message);
        }
    };


    // useEffect(() => {
    //     const filename = localStorage.getItem('filename');
    //     if (filename) {
    //         const imageURL = `http://localhost:8000/auth/pictures/${filename}`;
    //         setImageUrls([imageURL]);
    //         setImagePreview(imageURL);
    //     }
    // }, []);

    return (
        <>
        <Header />
        <section>
            <div className="container py-5 mt-5">
                <div className="row">
                    <div className="col-lg-4 ">
                    <div className="card1 " style={{height: "auto"}}>
                        <div className="card1-body text-center " style={{ paddingTop: '5px', paddingBottom: '5px' }}>
                                <div className='profimg d-flex align-items-center mb-4' style={{ position: 'relative' }}>
                                        <img
                                            src={formData && formData.profileImage ? `http://localhost:8000/auth/pictures/${formData.profileImage}` : user}
                                            alt="avatar"
                                            className="rounded-circle img-fluid me-2"
                                            style={{ width: '110px', height: '100px', border: '2.5px solid #eea943' }}
                                        />  
                                            <div>
                                                <h5 className="my-3">{formData && formData.name}</h5>
                                            </div>
                                </div>
                                <input type="file" name='file' ref={inputRef} onChange={handleImageChange} style={{ display: "none" }} />
                                <ul className="list-group list-group-flush rounded-3" style={{margin: '0', padding: '0'}}>
                                    <li className={`list-group-item d-flex align-items-center border-0 ${activeItem === 'profil' ? 'active' : ''}`} 
                                        onClick={() => handleItemClick('profil')}>
                                        <i class="fa-solid fa-gauge-high p-2"></i>
                                        <Link to='/profil' className="link mb-0">Tableau de bord</Link>
                                    </li>
                                    <li className={`list-group-item d-flex align-items-center border-0 ${activeItem === 'mesads' ? 'active' : ''}`} 
                                        onClick={() => handleItemClick('mesads')}>
                                        <i class="fa-solid fa-car-rear p-2 "></i> 
                                        <Link className="link mb-0 ">Annonces</Link>
                                    </li>
                                    <li className={`list-group-item d-flex align-items-center border-0 ${activeItem === 'profilpayment' ? 'active' : ''}`} 
                                        onClick={() => handleItemClick('profilpayment')}>
                                        <i class="fa-solid fa-coins p-2 "></i>                                        
                                        <Link to='/profilpayment' className=" link mb-0 ">Paiement</Link>
                                    </li>
                                    <li className={`list-group-item d-flex align-items-center border-0 ${activeItem === 'favoris' ? 'active' : ''}`} 
                                        onClick={() => handleItemClick('favoris')}>
                                        <i class="fa-solid fa-heart p-2 "></i>         
                                        <Link className=" link mb-0 ">Favoris</Link>
                                    </li>
                                    <li className={`list-group-item d-flex align-items-center border-0 ${activeItem === 'historique' ? 'active' : ''}`} 
                                        onClick={() => handleItemClick('historique')}>
                                        <i class="fa-solid fa-clock-rotate-left p-2 "></i>
                                        <Link to='/historique' className="link mb-0 ">Historique d'achat</Link>
                                    </li>
                                    <li className={`list-group-item d-flex align-items-center border-0 ${activeItem === 'notifs' ? 'active' : ''}`} 
                                        onClick={() => handleItemClick('notifs')}>
                                        <i class="fa-solid fa-bell p-2"></i>         
                                        <Link to='/notifications' className=" link mb-0 ">Notifications</Link>
                                    </li>
                                    <li className={`list-group-item d-flex align-items-center border-0 ${activeItem === 'messages' ? 'active' : ''}`} 
                                        onClick={() => handleItemClick('messages')}>
                                        <i class="fa-solid fa-message p-2 "></i>       
                                        <Link className="link mb-0 ">Messages</Link>
                                    </li>
                                    <li className={`list-group-item d-flex align-items-center border-0 ${activeItem === 'donneesprofil' ? 'active' : ''}`} 
                                        onClick={() => handleItemClick('donneesprofil')}>
                                        <i class="fa-solid fa-user-pen p-2 "></i>   
                                        <Link to="/donneesprofil" className="link mb-0">Profil</Link>
                                    </li>

                                    
                                </ul>
                            </div>
                        </div>
                    </div>
                        <div className="col-lg-8">
                            <div className="card1 mb-4">
                                <div className="card1-body">
                                    <h5 className='mb-5'>Informations personnelles</h5>
                                    <form onSubmit={handleSubmit}>
                                        <div className="row">
                                            <div className="col-sm-3">
                                                    <p className="mb-0">Image</p>
                                            </div>
                                            <div className="col-sm-9">
                                            <form>
                                            <div className='profimg d-flex align-items-center mb-2' style={{ position: 'relative' }}>
                                                {/* {imagePreview ? ( */}
                                                    <img
                                                        src={formData && formData.profileImage ? `http://localhost:8000/auth/pictures/${formData.profileImage}` : user}                                                        alt="avatar"
                                                        className="rounded-circle img-fluid me-2"
                                                        style={{ width: '110px', height: '100px', border: '2.5px solid #eea943' }}
                                                    />
                                                {/* // ) : (
                                                //     <img src={user} alt="avatar" className="rounded-circle img-fluid me-2" style={{ width: '130px', height: '120px',  }} />
                                                // )} */}
                                                {isEditing && (
                                                    <div style={{ position: 'absolute', top: 90, right: 470 }}>
                                                        <div style={{
                                                            width: '23px',
                                                            height: '23px',
                                                            borderRadius: '50%',
                                                            backgroundColor: '#d38041',
                                                            display: 'flex',
                                                            justifyContent: 'center',
                                                            alignItems: 'center',
                                                            cursor: 'pointer'
                                                        }}>
                                                            <FaPen style={{ fontSize: '13px', color: '#fff' }} onClick={handleImageClick} />
                                                        </div>
                                                    </div>
                                                )}
                                            </div>
                                            <input type="file" ref={inputRef} name='file' onChange={handleImageChange} style={{ display: "none" }} />
                                            </form>
                                            </div>
                                        </div>
                                        <hr />
                                        <div className="row">
                                            <div className="col-sm-3">
                                                <p className="mb-0">Nom d'utilisateur</p>
                                            </div>
                                            <div className="col-sm-9">
                                                {isEditing ? (
                                                    <input
                                                        type="text"
                                                        name="name"
                                                        value={formData && formData.name}
                                                        className="input-no-border"
                                                        onChange={handleChange}
                                                    />
                                                ) : (
                                                    <p className="text-muted mb-0">{formData && formData.name}</p>
                                                )}
                                            </div>
                                        </div>
                                        <hr />
                                <div className="row">
                                    <div className="col-sm-3">
                                        <p className="mb-0">Email</p>
                                    </div>
                                    <div className="col-sm-9">
                                                {isEditing ? (
                                                    <input
                                                        type="email"
                                                        name="email"
                                                        value={formData && formData.email}
                                                        className="input-no-border"
                                                        onChange={handleChange}
                                                />
                                                ) : (
                                                <p className="text-muted mb-0">{formData && formData.email}</p>
                                                )}
                                            </div>
                                </div>
                                <hr />
                                <div className="row">
                                    <div className="col-sm-3">
                                        <p className="mb-0">Telephone</p>
                                    </div>
                                    <div className="col-sm-9">
                                                {isEditing ? (
                                                    <input
                                                        type="phone"
                                                        name="phone"
                                                        value={formData && formData.phone}
                                                        className="input-no-border"
                                                        onChange={handleChange}
                                                    />
                                                ) : (
                                                    <p className="text-muted mb-0">{formData && formData.phone}</p>
                                                )}
                                            </div>
                                </div>
                                <hr />
                                <div className="row">
                                    <div className="col-sm-3">
                                        <p className="mb-0">Adresse</p>
                                    </div>
                                    <div className="col-sm-9">
                                                {isEditing ? (
                                                    <input
                                                        type="text"
                                                        name="address"
                                                        value={formData && formData.address}
                                                        className="input-no-border"
                                                        onChange={handleChange}
                                                    />
                                                ) : (
                                                    <p className="text-muted mb-0">{formData && formData.address}</p>
                                                )}
                                    </div>
                                </div>
                                <div className="row mt-4">
                                    <div className="col-sm-9">
                                            {isEditing ? (
                                                <button type="submit" onClick={updateUserProfile} className="ms-auto btn-ed">Enregistrer</button>
                                                ) : (
                                                <button onClick={handleEdit} className="btn-ed">Modifier</button>
                                            )}
                                        </div>
                                </div>
                                    </form>
                                </div>
                            </div>

                            <div className="card1 mt-5 mb-4">
                                <div className="card1-body">
                                    <h5 className='mb-5'>Sécurité</h5>
                                    <form onSubmit={handleSubmitPassword}>
                                        <div className="row">
                                            <div className="col-sm-3">
                                                <p className="mb-3">Mot de passe actuel *</p>
                                            </div>
                                            <div className="col-sm-12">
                                                    <input
                                                        type="password"
                                                        // value={formData && formData.password}
                                                        name="password"
                                                        onChange={handleChange}
                                                        placeholder='Mot de passe actuel'
                                                        className="input-no-border"
                                                    />
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-sm-6">
                                                <p className="mb-3 mt-3">Nouveau mot de passe *</p>
                                                <input
                                                    type="password"
                                                    name="newPassword"
                                                    value={formData && formData.newPassword}
                                                    onChange={handleChange}
                                                    className="input-no-border1 mb-3"
                                                    placeholder='Nouveau mot de passe'
                                                />
                                            </div>
                                            <div className="col-sm-6">
                                                <p className="mb-3 mt-3">Confirmez votre mot de passe *</p>
                                                <input
                                                    type="password"
                                                    name="confirmPassword"
                                                    value={formData && formData.confirmPassword}
                                                    onChange={handleChange}
                                                    className="input-no-border1"
                                                    placeholder='Confirmez votre mot de passe'
                                                />
                                            </div>
                                        </div>
                                        <div className="row mt-4">
                                            <div className="col-sm-9">
                                                    
                                                    <button type="submit" onClick={updateUserPassword} className="ms-auto btn-ed">Enregistrer</button>

                                                </div>
                                            </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    )
}

export default GestionProfil;
