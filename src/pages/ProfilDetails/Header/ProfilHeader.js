
import React, { useEffect, useState } from 'react';
import './ProfilHeader.css';
import user from '../../../images/userwhite.png'
import { NavLink } from 'react-router-dom';
import axios from 'axios';
// import user from '../../../images/woman.png'

const ProfilHeader = ({ activeLink, setActiveLink }) => {
    const [clicked, setClicked] = useState(false);
    const [open, setOpen]=useState(false)
    // eslint-disable-next-line no-unused-vars
    const handleClick = (linkId) => {
        setClicked(!clicked);
        setActiveLink(linkId); 
    };

    const [formData, setFormData] = useState(null);


    useEffect(() => {
        const fetchData = async () => {
            try {
                const userId = localStorage.getItem('userId');
                if (userId) {
                    const response = await axios.get(`http://localhost:8000/auth/users/${userId}`); 
                    setFormData(response.data);
                } 
            } catch (error) {
                console.log(error.message);
            }
        };
    
        fetchData();
    }, []);


    return (
        <header id="header" className="header fixed-top d-flex align-items-center">
            <div className="container d-flex align-items-center justify-content-between">
                <a href="/" className="logo d-flex me-auto me-lg-0"><img src="../../logo4.png" alt="" className="img-fluid" /></a>
                <nav className="navbar justify-content-end ms-auto">
                    <div>
                        <ul id="navbar" className={clicked ? "navbar active" : "navbar"}>
                            <li>
                                <NavLink to="/"
                                >Acceuil</NavLink>
                            </li>
                            <li>
                                <NavLink to="/about" spy="true" smooth="true" 
                                >A propos</NavLink>
                            </li>
                            <li>
                                <NavLink to="/voitures" spy="true" smooth="true" 
                                >Voitures</NavLink>
                            </li>
                            <li>
                                <NavLink to="/devenirexpert"
                                >Devenir Expert</NavLink>
                            </li>
                            <li>
                                <NavLink to="/vendrevoiture" spy="true" smooth="true" 
                                >Vendre votre voiture</NavLink>
                            </li>
                            <li className='profile'>
                                    <img  src={formData && formData.profileImage ? `http://localhost:8000/auth/pictures/${formData.profileImage}` : user}
                                    alt='user' 
                                    onClick={() => setOpen(!open)} className='profile-icon-small ' /> 
                                {open &&
                                    <div className='drop '>
                                        <ul>
                                            <li className='text'>Profil</li>
                                            <li className='text'>Se déconnecter</li>
                                        </ul>
                                    </div>
                                }
                            </li>
                        </ul>
                    </div>
                    <div id='mobile' onClick={() => setClicked(!clicked)}>
                        <i id="bar" className={clicked ? 'fas fa-times' : 'fas fa-bars'}></i>
                    </div>
                </nav>
            </div>
        </header>
    );
}

export default ProfilHeader;
