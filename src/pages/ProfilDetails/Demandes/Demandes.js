/* eslint-disable no-unused-vars */
import React, { useEffect, useRef, useState } from 'react'
import user from '../../../images/user.png'
import './Demandes.css'
import { Link } from 'react-router-dom'
import Footer from '../../../components/Footer/Footer'
import Header from '../../ProfilDetails/Header/ProfilHeader'
import axios from 'axios'

const Demandes = () => {

    const [formData, setUserData] = useState(null);
    const [experts, setExperts]=useState(null)


    useEffect(() => {
        const fetchData = async () => {
            try {
                const userId = localStorage.getItem('userId');
                if (userId) {
                    const response = await axios.get(`http://localhost:8000/auth/users/${userId}`);
                    setUserData(response.data);
                    handleExpertise()
                }
            } catch (error) {
                console.log(error.message);
            }
        };
        fetchData();
    }, []);

    const fetchUserDetails = async (userId) => {
        try {
            const response = await axios.get(`http://localhost:8000/auth/users/${userId}`);
            return response.data;
        } catch (error) {
            console.error('Error fetching user details:', error);
            return null;
        }
    };


    const [usersDetails, setUsersDetails] = useState({});

    useEffect(() => {
        if (experts && experts.demandes) {
            const userDetailsPromises = experts.demandes.map((demande) => fetchUserDetails(demande.user));
            Promise.all(userDetailsPromises)
                .then((userDetails) => {
                    const usersDetailsObj = userDetails.reduce((acc, curr, idx) => {
                        acc[experts.demandes[idx].user] = curr;
                        return acc;
                    }, {});
                    setUsersDetails(usersDetailsObj);
                })
                .catch((error) => {
                    console.error('Error fetching users details:', error);
                });
        }
    }, [experts]);


    const inputRef = useRef(null);


    const uploadImage = async (file) => {
        const formData = new FormData();
        formData.append('file', file);

        try {
            const token = localStorage.getItem('token'); 
            const response = await axios.post('http://localhost:8000/auth/upload', formData, {
            headers: {
                'Content-Type': 'multipart/form-data',
                'Authorization': `Bearer ${token}` 
            }
            });
            console.log(response.data);
            setUserData({ ...formData, profileImage: response.data.profileImage });
        } catch (error) {
            console.log(error);
        }
    };

    const handleImageClick = () => {
        inputRef.current.click();
    };

    const handleImageChange = (event) => {
        const file = event.target.files[0];
        console.log(file);
        uploadImage(file);
    };

    const [activeItem, setActiveItem] = useState('mesdemandes');

    const handleItemClick = (itemName) => {
        setActiveItem(itemName);
    };

    const [expertise, setExpertise]=useState(false)

    const handleExpertise = () => {
        const role = localStorage.getItem('role');
        if (role === 'expert') {
            setExpertise(true);
        }
    };


    const getExpertById = async (expertId) => {
        try {
            const response = await axios.get(`http://localhost:8000/auth/experts/${expertId}`);
            setExperts(response.data)
            console.log(response.data); 
        } catch (error) {
            console.error('Error fetching expert details:', error);
        }
    };
    

    useEffect(() => {
        const expertId = localStorage.getItem('idExpert');
        if (expertId) {
            getExpertById(expertId);
        }
    }, []);
    

    const [post, setPost] = useState([]);
    
    useEffect(() => {
        const fetchPostDetails = async (postId) => {
            try {
                const response = await axios.get(`http://localhost:8000/posts/post/${postId}`);
                return response.data;
            } catch (error) {
                console.error('Error fetching post:', error);
                return null;
            }
        };

        const fetchDemandesDetails = async () => {
            if (experts && experts.demandes) {
                const demandesDetails = await Promise.all(
                    experts.demandes.map(async (demande) => {
                        const details = await fetchPostDetails(demande.post);
                        return details;
                    })
                );
                setPost(demandesDetails.filter((detail) => detail !== null));
            }
        };
            
        if (experts && experts.demandes) {
            fetchDemandesDetails();
        }
    }, [experts]);

    
    

    const handleLinkClick = (postId) => {
        localStorage.setItem('postId', postId);
    };

    

    const [expandedDemandes, setExpandedDemandes] = useState({});


    const toggleDemandeExpansion = (idx) => {
        setExpandedDemandes((prevState) => ({
            ...prevState,
            [idx]: !prevState[idx],
        }));
    };

        const expertId = localStorage.getItem('idExpert');
        const [acceptedDemands, setAcceptedDemands]=useState([]);
        const [rejectedDemands, setRejectedDemands]=useState([]);

        const storeAcceptedDemand = (postId) => {
            const acceptedDemandsKey = `acceptedDemands_${expertId}`;
            let acceptedDemands = JSON.parse(localStorage.getItem(acceptedDemandsKey)) || [];
            if (!acceptedDemands.includes(postId)) {
                acceptedDemands.push(postId);
                localStorage.setItem(acceptedDemandsKey, JSON.stringify(acceptedDemands));
            }
        };
        
        const storeRejectedDemand = (postId) => {
            const rejectedDemandsKey = `rejectedDemands_${expertId}`;
            let rejectedDemands = JSON.parse(localStorage.getItem(rejectedDemandsKey)) || [];
            if (!rejectedDemands.includes(postId)) {
                rejectedDemands.push(postId);
                localStorage.setItem(rejectedDemandsKey, JSON.stringify(rejectedDemands));
            }
        };

        const acceptDemand = async (expertId, postId, userId) => {
            try {
                await axios.post('http://localhost:8000/auth/choix/status', {
                    expertId,
                    postId,
                    userId,
                    accepted: true
                });
                storeAcceptedDemand(postId); 
                setAcceptedDemands([...acceptedDemands, postId]);
            } catch (error) {
                console.error('Erreur lors de l\'acceptation de la demande :', error);
            }
        };

        const rejectDemand = async (expertId, postId, userId) => {
            try {
                await axios.post('http://localhost:8000/auth/choix/status', {
                    expertId,
                    postId,
                    userId,
                    accepted: false
                });
                storeRejectedDemand(postId); 
                setRejectedDemands([...rejectedDemands, postId]);
            } catch (error) {
                console.error('Erreur lors du refus de la demande :', error);
            }
        };

        useEffect(() => {
            const acceptedDemandsKey = `acceptedDemands_${expertId}`;
            const rejectedDemandsKey = `rejectedDemands_${expertId}`;
            const storedAcceptedDemands = JSON.parse(localStorage.getItem(acceptedDemandsKey)) || [];
            const storedRejectedDemands = JSON.parse(localStorage.getItem(rejectedDemandsKey)) || [];
            setAcceptedDemands(storedAcceptedDemands);
            setRejectedDemands(storedRejectedDemands);
        }, [expertId]); 
        


    return (
        <>
            <Header />
            <section id="mesads">
                <div className="container py-5 mt-5">
                    <div className="row">
                        <div className="col-lg-4">
                            <div className="card1" style={{ height: "auto" }}>
                                <div className="card1-body text-center" style={{ paddingTop: '5px', paddingBottom: '5px' }}>
                                    <div className='profimg d-flex align-items-center mb-4' style={{ position: 'relative' }}>
                                        <img
                                            src={formData && formData.profileImage ? `http://localhost:8000/auth/pictures/${formData.profileImage}` : user}
                                            alt="avatar"
                                            className="rounded-circle img-fluid me-2"
                                            style={{ width: '110px', height: '100px', border: '2.5px solid #eea943' }}
                                        />
                                        <div>
                                            <h5 className="my-3">{formData && formData.name}</h5>
                                        </div>
                                    </div>
                                    <input type="file" ref={inputRef} onChange={handleImageChange} style={{ display: "none" }} />
                                    <ul className="list-group list-group-flush rounded-3" style={{margin: '0', padding: '0'}}>
                                    <li className={`list-group-item d-flex align-items-center border-0 ${activeItem === 'profil' ? 'active' : ''}`} 
                                        onClick={() => handleItemClick('profil')}>
                                        <i class="fa-solid fa-gauge-high p-2"></i>
                                        <Link to='/profil' className="link mb-0">Tableau de bord</Link>
                                    </li>
                                    {expertise && (
                                            <li
                                                className={`list-group-item d-flex align-items-center border-0 ${
                                                    activeItem === 'mesdemandes' ? 'active' : ''
                                                }`}
                                                onClick={() => handleItemClick('mesdemandes')}
                                            >
                                                <i className="fa-solid fa-screwdriver-wrench p-2 "></i>
                                                <Link to="/mesdemandes" className="link mb-0 ">
                                                    Demandes d'expertise
                                                </Link>
                                            </li>
                                        )}
                                    <li className={`list-group-item d-flex align-items-center border-0 ${activeItem === 'mesads' ? 'active' : ''}`} 
                                        onClick={() => handleItemClick('mesads')}>
                                        <i class="fa-solid fa-car-rear p-2 "></i> 
                                        <Link to="/mesannonces "className="link mb-0 ">Annonces</Link>
                                    </li>
                                    <li className={`list-group-item d-flex align-items-center border-0 ${activeItem === 'profilpayment' ? 'active' : ''}`} 
                                        onClick={() => handleItemClick('profilpayment')}>
                                        <i class="fa-solid fa-coins p-2 "></i>                                        
                                        <Link to='/profilpayment' className=" link mb-0 ">Paiement</Link>
                                    </li>
                                    <li className={`list-group-item d-flex align-items-center border-0 ${activeItem === 'favoris' ? 'active' : ''}`} 
                                        onClick={() => handleItemClick('favoris')}>
                                        <i class="fa-solid fa-heart p-2 "></i>         
                                        <Link to='/favoris' className=" link mb-0 ">Favoris</Link>
                                    </li>
                                    <li className={`list-group-item d-flex align-items-center border-0 ${activeItem === 'historique' ? 'active' : ''}`} 
                                        onClick={() => handleItemClick('historique')}>
                                        <i class="fa-solid fa-clock-rotate-left p-2 "></i>
                                        <Link to='/historique' className="link mb-0 ">Historique d'achat</Link>
                                    </li>
                                    <li className={`list-group-item d-flex align-items-center border-0 ${activeItem === 'notifs' ? 'active' : ''}`} 
                                        onClick={() => handleItemClick('notifs')}>
                                        <i class="fa-solid fa-bell p-2"></i>         
                                        <Link to='/notifications' className=" link mb-0 ">Notifications</Link>
                                    </li>
                                    <li className={`list-group-item d-flex align-items-center border-0 ${activeItem === 'messages' ? 'active' : ''}`} 
                                        onClick={() => handleItemClick('messages')}>
                                        <i class="fa-solid fa-message p-2 "></i>       
                                        <Link className="link mb-0 ">Messages</Link>
                                    </li>
                                    <li className={`list-group-item d-flex align-items-center border-0 ${activeItem === 'donneesprofil' ? 'active' : ''}`} 
                                        onClick={() => handleItemClick('donneesprofil')}>
                                        <i class="fa-solid fa-user-pen p-2 "></i>   
                                        <Link to="/donneesprofil" className="link mb-0">Profil</Link>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-8">
                        <div className>
                            <div className="">
                                <div class="card1 mb-4 mb-8">
                                <div class="tab-pane active show" id="tasks-tab" role="tabpanel">
                                    <h4 class="card-title mb-4">Mes demandes d'expertise</h4>
                                    <div class="row">
                                    <div class="col-xl-12">
                                    {experts && experts.demandes && (
                                                <div className="task-list-box" id="landing-task">
                                                    {experts.demandes.map((demande, idx) => (
                                                    <div key={idx} className={`card mb-4 task-box rounded-3 ${expandedDemandes[idx] ? 'expanded' : ''}`} style={{ height: expandedDemandes[idx] ? 'auto' : '70px' }}>
                                                        <div className="card-body">
                                                            <div className="row align-items-center">
                                                                <div className="col-xl-6 col-sm-5">
                                                                    <div className="checklist form-check ">
                                                                        <label className="form-check-label ms-1 task-title" htmlFor="customCheck">
                                                                            <Link className="lien" to={`/voitures/${encodeURIComponent(demande.post)}`} onClick={() => handleLinkClick(demande.post)}>
                                                                            { usersDetails[demande.user] && usersDetails[demande.user].name} : { post[idx] && post[idx].marque} {post[idx] && post[idx].modele} 
                                                                            </Link>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <div className="col-xl-6 col-sm-7">
                                                                    <div className="row align-items-center">
                                                                        <div className="col-xl-11 col-md-7 col-sm-7">
                                                                            <div className="d-flex flex-wrap gap-3 mt-3 mt-xl-0 justify-content-md-end">
                                                                                {rejectedDemands.includes(demande.post) && (
                                                                                    <div className="badge bg-danger text-white">Refusée</div>
                                                                                )}
                                                                                {acceptedDemands.includes(demande.post) && (
                                                                                    <div className="badge bg-success text-white">Acceptée</div>
                                                                                )}
                                                                                    <div>
                                                                                    <a
                                                                                        href
                                                                                        className="mb-0 text-muted fw-medium pe-1 text-secondary "
                                                                                        onClick={() => toggleDemandeExpansion(idx)} style={{fontSize:15 ,cursor:'pointer'}}
                                                                                    >
                                                                                        Details
                                                                                    </a>
                                                                                </div>
                                                                                <div>
                                                                                    <a href className="mb-0 text-muted fw-medium" onClick={() => acceptDemand(experts._id, demande.post, demande.user)}>
                                                                                        <i className={`fa-solid fa-check pe-1 align-middle text-secondary${acceptedDemands.includes(demande.post) ? ' accepted' : ''}`} style={{fontSize: 15, cursor: 'pointer'}}></i>
                                                                                    </a>
                                                                                </div>
                                                                                <div>
                                                                                <div>
                                                                                    <a href className="mb-0 text-muted fw-medium" onClick={() => rejectDemand(experts._id, demande.post, demande.user)}>
                                                                                        <i className={`bi bi-trash pe-1 align-middle text-secondary${rejectedDemands.includes(demande.post) ? ' rejected' : ''}`} style={{fontSize: 15, cursor: 'pointer'}}></i>
                                                                                    </a>
                                                                                </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            {expandedDemandes[idx] && usersDetails[demande.user] && (
                                                                <div className='ms-4 mt-3 mb-1'>
                                                                    <h6>Utilisateur qui a demandé l'expertise : </h6>
                                                                    <span>Nom : {usersDetails[demande.user].name}</span><br/>
                                                                    <span>Email : {usersDetails[demande.user].email}</span><br/>
                                                                    <span>Telephone: {usersDetails[demande.user].phone}</span>
                                                                </div>
                                                            )}

                                                        </div>
                                                    </div>
                                                ))}
                                            </div>
                                        )}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <Footer />
    </>
    )
}

export default Demandes