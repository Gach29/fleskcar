/* eslint-disable no-unused-vars */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useEffect, useRef, useState } from 'react'
import user from '../../../images/user.png'
import './Paiement.css'
import { Link } from 'react-router-dom'
import Header from '../../ProfilDetails/Header/ProfilHeader'
import Footer from '../../../components/Footer/Footer'
import axios from 'axios'

const Paiement = () => {

    const [formData, setUserData] = useState(null);

    useEffect(() => {
        const fetchData = async () => {
            try {
                const userId = localStorage.getItem('userId');
                if (userId) {
                    const response = await axios.get(`http://localhost:8000/auth/users/${userId}`);
                    setUserData(response.data);
                }
            } catch (error) {
                console.log(error.message);
            }
        };

        fetchData();
    }, []);

    const inputRef = useRef(null);


    const uploadImage = async (file) => {
        const formData = new FormData();
        formData.append('file', file);

        try {
            const token = localStorage.getItem('token'); 
            const response = await axios.post('http://localhost:8000/auth/upload', formData, {
            headers: {
                'Content-Type': 'multipart/form-data',
                'Authorization': `Bearer ${token}` 
            }
            });
            console.log(response.data);
            setUserData({ ...formData, profileImage: response.data.profileImage });
        } catch (error) {
            console.log(error);
        }
    };

    const handleImageClick = () => {
        inputRef.current.click();
    };

    const handleImageChange = (event) => {
        const file = event.target.files[0];
        console.log(file);
        uploadImage(file);
    };

    const [activeItem, setActiveItem] = useState('profilpayment');

    const handleItemClick = (itemName) => {
        setActiveItem(itemName);
    };
    return (
        <>
            <Header />
            <section id="favoris">
                <div className="container py-5 mt-5">
                    <div className="row">
                        <div className="col-lg-4">
                            <div className="card1" style={{ height: "auto" }}>
                                <div className="card1-body text-center" style={{ paddingTop: '5px', paddingBottom: '5px' }}>
                                    <div className='profimg d-flex align-items-center mb-4' style={{ position: 'relative' }}>
                                        <img
                                            src={formData && formData.profileImage ? `http://localhost:8000/auth/pictures/${formData.profileImage}` : user}
                                            alt="avatar"
                                            className="rounded-circle img-fluid me-2"
                                            style={{ width: '110px', height: '100px', border: '2.5px solid #eea943' }}
                                        />
                                        <div>
                                            <h5 className="my-3">{formData && formData.name}</h5>
                                        </div>
                                    </div>
                                    <input type="file" ref={inputRef} onChange={handleImageChange} style={{ display: "none" }} />
                                    <ul className="list-group list-group-flush rounded-3" style={{margin: '0', padding: '0'}}>
                                    <li className={`list-group-item d-flex align-items-center border-0 ${activeItem === 'profil' ? 'active' : ''}`} 
                                        onClick={() => handleItemClick('profil')}>
                                        <i class="fa-solid fa-gauge-high p-2"></i>
                                        <Link to='/profil' className="link mb-0">Tableau de bord</Link>
                                    </li>
                                    <li className={`list-group-item d-flex align-items-center border-0 ${activeItem === 'mesads' ? 'active' : ''}`} 
                                        onClick={() => handleItemClick('mesads')}>
                                        <i class="fa-solid fa-car-rear p-2 "></i> 
                                        <Link to="/mesannonces" className="link mb-0 ">Annonces</Link>
                                    </li>
                                    <li className={`list-group-item d-flex align-items-center border-0 ${activeItem === 'profilpayment' ? 'active' : ''}`} 
                                        onClick={() => handleItemClick('profilpayment')}>
                                        <i class="fa-solid fa-coins p-2 "></i>                                        
                                        <Link to='/profilpayment' className=" link mb-0 ">Paiement</Link>
                                    </li>
                                    <li className={`list-group-item d-flex align-items-center border-0 ${activeItem === 'favoris' ? 'active' : ''}`} 
                                        onClick={() => handleItemClick('favoris')}>
                                        <i class="fa-solid fa-heart p-2 "></i>         
                                        <Link to='/favoris' className=" link mb-0 ">Favoris</Link>
                                    </li>
                                    <li className={`list-group-item d-flex align-items-center border-0 ${activeItem === 'historique' ? 'active' : ''}`} 
                                        onClick={() => handleItemClick('historique')}>
                                        <i class="fa-solid fa-clock-rotate-left p-2 "></i>
                                        <Link to='/historique' className="link mb-0 ">Historique d'achat</Link>
                                    </li>
                                    <li className={`list-group-item d-flex align-items-center border-0 ${activeItem === 'notifs' ? 'active' : ''}`} 
                                        onClick={() => handleItemClick('notifs')}>
                                        <i class="fa-solid fa-bell p-2"></i>         
                                        <Link to='/notifications' className=" link mb-0 ">Notifications</Link>
                                    </li>
                                    <li className={`list-group-item d-flex align-items-center border-0 ${activeItem === 'messages' ? 'active' : ''}`} 
                                        onClick={() => handleItemClick('messages')}>
                                        <i class="fa-solid fa-message p-2 "></i>       
                                        <Link className="link mb-0 ">Messages</Link>
                                    </li>
                                    <li className={`list-group-item d-flex align-items-center border-0 ${activeItem === 'donneesprofil' ? 'active' : ''}`} 
                                        onClick={() => handleItemClick('donneesprofil')}>
                                        <i class="fa-solid fa-user-pen p-2 "></i>   
                                        <Link to="/donneesprofil" className="link mb-0">Profil</Link>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-8">
                        <div className="card1">
                            <div className="">
                                <div class="row">
                                    <div class="">
                                        <div class="osahan-account-page-right bg-white p-4 h-100">
                                            <div class="tab-content" id="myTabContent">
                                                <div class="tab-pane fade active show" id="payments" role="tabpanel" aria-labelledby="payments-tab">
                                                    <h4 class="font-weight-bold mt-0 mb-4">Paiements</h4>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="bg-white card payments-item mb-4 shadow-sm">
                                                                <div class="gold-members p-4">
                                                                    <a href>
                                                                    </a>
                                                                    <div class="media">
                                                                        <div class="media-body">
                                                                        <a href>
                                                                            <i class="bi bi-wallet mb-1"></i>
                                                                        </a>
                                                                            <a href>
                                                                                <h6 class="mb-1">6070-XXXXXXXX-0666</h6>
                                                                                <p>VALID TILL 10/2025</p>
                                                                            </a>
                                                                            <p class="mb-0 text-black font-weight-bold">
                                                                                <a href>
                                                                                </a><a class="text-danger" data-toggle="modal" data-target="#delete-address-modal" href><i class="icofont-ui-delete"></i> DELETE</a></p>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="bg-white card payments-item mb-4 shadow-sm">
                                                                <div class="gold-members p-4">
                                                                    <a href="#">
                                                                    </a>
                                                                    <div class="media">
                                                                        <div class="media-body">
                                                                            <a href="#">
                                                                                <i class="bi bi-wallet"></i>
                                                                            </a>
                                                                            <a href="#">
                                                                                <h6 class="mb-1">6070-XXXXXXXX-0666</h6>
                                                                                <p>VALID TILL 10/2025</p>
                                                                            </a>
                                                                            <p class="mb-0 text-black font-weight-bold">
                                                                                <a href="#">
                                                                                </a><a class="text-danger" data-toggle="modal" data-target="#delete-address-modal" href="#"><i class="icofont-ui-delete"></i> DELETE</a></p>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row pt-2 pb-2">
                                                        <div class="col-md-6">
                                                            <div class="bg-white card payments-item mb-4 shadow-sm">
                                                                <div class="gold-members p-4">
                                                                    <a href="#">
                                                                    </a>
                                                                    <div class="media">
                                                                        <div class="media-body">
                                                                            <a href="#">
                                                                                <i class="bi bi-wallet"></i>
                                                                            </a>
                                                                            <a href="#">
                                                                                <h6 class="mb-1">6070-XXXXXXXX-0666</h6>
                                                                                <p>VALID TILL 10/2025</p>
                                                                            </a>
                                                                            <p class="mb-0 text-black font-weight-bold">
                                                                                <a href="#">
                                                                                </a><a class="text-danger" data-toggle="modal" data-target="#delete-address-modal" href="#"><i class="icofont-ui-delete"></i> DELETE</a></p>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="bg-white card payments-item mb-4 shadow-sm">
                                                                <div class="gold-members p-4">
                                                                    <a href="#">
                                                                    </a>
                                                                    <div class="media">
                                                                        <div class="media-body">
                                                                            <a href="#">
                                                                                <i class="bi bi-wallet"></i>
                                                                            </a>
                                                                            <a href="#">
                                                                                <h6 class="mb-1">6070-XXXXXXXX-0666</h6>
                                                                                <p>VALID TILL 10/2025</p>
                                                                            </a>
                                                                            <p class="mb-0 text-black font-weight-bold">
                                                                                <a href="#">
                                                                                </a><a class="text-danger" data-toggle="modal" data-target="#delete-address-modal" href="#"><i class="icofont-ui-delete"></i> DELETE</a></p>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="bg-white card payments-item mb-4 shadow-sm">
                                                                <div class="gold-members p-4">
                                                                    <a href="#">
                                                                    </a>
                                                                    <div class="media">
                                                                        <div class="media-body">
                                                                            <a href="#">
                                                                                <i class="bi bi-wallet"></i>
                                                                            </a>
                                                                            <a href="#">
                                                                                <h6 class="mb-1">6070-XXXXXXXX-0666</h6>
                                                                                <p>VALID TILL 10/2025</p>
                                                                            </a>
                                                                            <p class="mb-0 text-black font-weight-bold">
                                                                                <a href="#">
                                                                                </a><a class="text-danger" data-toggle="modal" data-target="#delete-address-modal" href="#"><i class="icofont-ui-delete"></i> DELETE</a></p>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="bg-white card payments-item mb-4 shadow-sm">
                                                                <div class="gold-members p-4">
                                                                    <a href="#">
                                                                    </a>
                                                                    <div class="media">
                                                                        <div class="media-body">
                                                                            <a href="#">
                                                                                <i class="bi bi-wallet"></i>
                                                                            </a>
                                                                            <a href="#">
                                                                                <h6 class="mb-1">6070-XXXXXXXX-0666</h6>
                                                                                <p>VALID TILL 10/2025</p>
                                                                            </a>
                                                                            <p class="mb-0 text-black font-weight-bold">
                                                                                <a href="#">
                                                                                </a><a class="text-danger" data-toggle="modal" data-target="#delete-address-modal" href="#"><i class="icofont-ui-delete"></i> DELETE</a></p>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row pt-2">
                                                        <div class="col-md-6">
                                                            <div class="bg-white card payments-item shadow-sm">
                                                                <div class="gold-members p-4">
                                                                    <a href="#">
                                                                    </a>
                                                                    <div class="media">
                                                                        <div class="media-body">
                                                                            <a href="#">
                                                                                <i class="bi bi-wallet"></i>
                                                                            </a>
                                                                            <a href="#">
                                                                                <h6 class="mb-1">6070-XXXXXXXX-0666</h6>
                                                                                <p class="text-black">VALID TILL 10/2025</p>
                                                                            </a>
                                                                            <p class="mb-0 text-black font-weight-bold">
                                                                                <a href="#">
                                                                                </a><a class="text-danger" data-toggle="modal" data-target="#delete-address-modal" href="#"><i class="icofont-ui-delete"></i> DELETE</a></p>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="bg-white card payments-item shadow-sm">
                                                                <div class="gold-members p-4">
                                                                    <a href="#">
                                                                    </a>
                                                                    <div class="media">
                                                                        <div class="media-body">
                                                                            <a href="#">
                                                                                <i class="icofont-visa icofont-4x"></i>
                                                                            </a>
                                                                            <a href="#">
                                                                                <h6 class="mb-1">6070-XXXXXXXX-0666</h6>
                                                                                <p>VALID TILL 10/2025</p>
                                                                            </a>
                                                                            <p class="mb-0 text-black font-weight-bold">
                                                                                <a href="#">
                                                                                </a><a class="text-danger" data-toggle="modal" data-target="#delete-address-modal" href="#"><i class="icofont-ui-delete"></i> DELETE</a></p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <Footer />
        </>

    )
    }

export default Paiement