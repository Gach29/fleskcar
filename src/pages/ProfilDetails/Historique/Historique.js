/* eslint-disable no-unused-vars */
import React, { useEffect, useRef, useState } from 'react'
import user from '../../../images/user.png'
import './Historique.css'
import { Link } from 'react-router-dom'
import Footer from '../../../components/Footer/Footer'
import Header from '../../ProfilDetails/Header/ProfilHeader'
import axios from 'axios'

const Historique = () => {

    const [formData, setUserData] = useState(null);

    useEffect(() => {
        const fetchData = async () => {
            try {
                const userId = localStorage.getItem('userId');
                if (userId) {
                    const response = await axios.get(`http://localhost:8000/auth/users/${userId}`);
                    setUserData(response.data);
                }
            } catch (error) {
                console.log(error.message);
            }
        };

        fetchData();
    }, []);

    const inputRef = useRef(null);


    const uploadImage = async (file) => {
        const formData = new FormData();
        formData.append('file', file);

        try {
            const token = localStorage.getItem('token'); 
            const response = await axios.post('http://localhost:8000/auth/upload', formData, {
            headers: {
                'Content-Type': 'multipart/form-data',
                'Authorization': `Bearer ${token}` 
            }
            });
            console.log(response.data);
            setUserData({ ...formData, profileImage: response.data.profileImage });
        } catch (error) {
            console.log(error);
        }
    };

    const handleImageClick = () => {
        inputRef.current.click();
    };

    const handleImageChange = (event) => {
        const file = event.target.files[0];
        console.log(file);
        uploadImage(file);
    };

    const [activeItem, setActiveItem] = useState('historique');

    const handleItemClick = (itemName) => {
        setActiveItem(itemName);
    };
    return (
        <>
            <Header />
            <section id="historique">
                <div className="container py-5 mt-5">
                    <div className="row">
                        <div className="col-lg-4">
                            <div className="card1" style={{ height: "auto" }}>
                                <div className="card1-body text-center" style={{ paddingTop: '5px', paddingBottom: '5px' }}>
                                    <div className='profimg d-flex align-items-center mb-4' style={{ position: 'relative' }}>
                                        <img
                                            src={formData && formData.profileImage ? `http://localhost:8000/auth/pictures/${formData.profileImage}` : user}
                                            alt="avatar"
                                            className="rounded-circle img-fluid me-2"
                                            style={{ width: '110px', height: '100px', border: '2.5px solid #eea943' }}
                                        />
                                        <div>
                                            <h5 className="my-3">{formData && formData.name}</h5>
                                        </div>
                                    </div>
                                    <input type="file" ref={inputRef} onChange={handleImageChange} style={{ display: "none" }} />
                                    <ul className="list-group list-group-flush rounded-3" style={{margin: '0', padding: '0'}}>
                                    <li className={`list-group-item d-flex align-items-center border-0 ${activeItem === 'profil' ? 'active' : ''}`} 
                                        onClick={() => handleItemClick('profil')}>
                                        <i class="fa-solid fa-gauge-high p-2"></i>
                                        <Link to='/profil' className="link mb-0">Tableau de bord</Link>
                                    </li>
                                    <li className={`list-group-item d-flex align-items-center border-0 ${activeItem === 'mesads' ? 'active' : ''}`} 
                                        onClick={() => handleItemClick('mesads')}>
                                        <i class="fa-solid fa-car-rear p-2 "></i> 
                                        <Link to ="/mesannonces" className="link mb-0 ">Annonces</Link>
                                    </li>
                                    <li className={`list-group-item d-flex align-items-center border-0 ${activeItem === 'profilpayment' ? 'active' : ''}`} 
                                        onClick={() => handleItemClick('profilpayment')}>
                                        <i class="fa-solid fa-coins p-2 "></i>                                        
                                        <Link to='/profilpayment' className=" link mb-0 ">Paiement</Link>
                                    </li>
                                    <li className={`list-group-item d-flex align-items-center border-0 ${activeItem === 'favoris' ? 'active' : ''}`} 
                                        onClick={() => handleItemClick('favoris')}>
                                        <i class="fa-solid fa-heart p-2 "></i>         
                                        <Link to='/favoris' className=" link mb-0 ">Favoris</Link>
                                    </li>
                                    <li className={`list-group-item d-flex align-items-center border-0 ${activeItem === 'historique' ? 'active' : ''}`} 
                                        onClick={() => handleItemClick('historique')}>
                                        <i class="fa-solid fa-clock-rotate-left p-2 "></i>
                                        <Link to='/historique' className="link mb-0 ">Historique d'achat</Link>
                                    </li>
                                    <li className={`list-group-item d-flex align-items-center border-0 ${activeItem === 'notifs' ? 'active' : ''}`} 
                                        onClick={() => handleItemClick('notifs')}>
                                        <i class="fa-solid fa-bell p-2"></i>         
                                        <Link to='/notifications' className=" link mb-0 ">Notifications</Link>
                                    </li>
                                    <li className={`list-group-item d-flex align-items-center border-0 ${activeItem === 'messages' ? 'active' : ''}`} 
                                        onClick={() => handleItemClick('messages')}>
                                        <i class="fa-solid fa-message p-2 "></i>       
                                        <Link className="link mb-0 ">Messages</Link>
                                    </li>
                                    <li className={`list-group-item d-flex align-items-center border-0 ${activeItem === 'donneesprofil' ? 'active' : ''}`} 
                                        onClick={() => handleItemClick('donneesprofil')}>
                                        <i class="fa-solid fa-user-pen p-2 "></i>   
                                        <Link to="/donneesprofil" className="link mb-0">Profil</Link>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-8">
                        <div className>
                            <div className="">
                                <div class="card1 mb-4 mb-8">
                                <div class="tab-pane active show" id="tasks-tab" role="tabpanel">
                                    <h4 class="card-title mb-4">Mes Annonces</h4>
                                    <div class="row">
                                        <div class="col-xl-12">
                                            <div class="task-list-box" id="landing-task">
                                                <div id="task-item-1">
                                                    <div class="card mb-4 task-box rounded-3" style={{height:70}}>
                                                        <div class="card-body">
                                                            <div class="row align-items-center">
                                                                <div class="col-xl-6 col-sm-5">
                                                                    <div class="checklist form-check ">
                                                                        {/* <img className="mb-3 shadow-sm mt-1" src="https://bootdey.com/img/Content/avatar/avatar1.png" alt="Product" style={{maxHeight:60}} /> */}
                                                                        {/* <input type="checkbox" class="form-check-input" id="customCheck1"/> */}
                                                                        <label class="form-check-label ms-1 task-title" for="customCheck1">Citroen C3</label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xl-6 col-sm-7">
                                                                    <div class="row align-items-center">
                                                                        <div class="col-xl-11 col-md-7 col-sm-7">
                                                                            <div class="d-flex flex-wrap gap-3 mt-3 mt-xl-0 justify-content-md-end">
                                                                                <div>
                                                                                    <span class="badge rounded-pill badge-soft-warning font-size-11 task-status">En cours</span>
                                                                                </div>
                                                                                <div>
                                                                                    <a href style={{fontSize:14}}><i class="bi bi-calendar text-secondary me-2"></i>15/04/2024</a>

                                                                                </div>
                                                                                <div>
                                                                                    <a href class="mb-0 text-muted fw-medium" data-bs-toggle="modal" data-bs-target=".bs-example-new-task">
                                                                                        <i class="bi bi-heart pe-1 font-size-16 align-middle text-secondary"></i>0
                                                                                    </a>
                                                                                </div>
                                                                                <div>
                                                                                    <a href class="mb-0 text-muted fw-medium" data-bs-toggle="modal" data-bs-target=".bs-example-new-task">
                                                                                        <i class="bi bi-chat text-secondary pe-1 "></i>11
                                                                                    </a>
                                                                                </div>
                                                                                <div>
                                                                                    <a href class="delete-item" onclick="deleteProjects('task-item-1')">
                                                                                        <i class="bi bi-trash align-middle font-size-16 text-danger"></i>
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> 
                                            <div class="task-list-box" id="landing-task">
                                                <div id="task-item-2">
                                                <div class="card mb-4 task-box rounded-3" style={{height:70}}>
                                                        <div class="card-body">
                                                            <div class="row align-items-center">
                                                                <div class="col-xl-6 col-sm-5">
                                                                    <div class="checklist form-check ">
                                                                        {/* <img className="mb-3 shadow-sm mt-1" src="https://bootdey.com/img/Content/avatar/avatar1.png" alt="Product" style={{maxHeight:60}} /> */}
                                                                        {/* <input type="checkbox" class="form-check-input" id="customCheck1"/> */}
                                                                        <label class="form-check-label ms-1 task-title" for="customCheck1">Citroen C3</label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xl-6 col-sm-7">
                                                                    <div class="row align-items-center">
                                                                        <div class="col-xl-11 col-md-7 col-sm-7">
                                                                            <div class="d-flex flex-wrap gap-3 mt-3 mt-xl-0 justify-content-md-end">
                                                                                <div>
                                                                                    <span class="badge rounded-pill badge-soft-warning font-size-11 task-status">En cours</span>
                                                                                </div>
                                                                                <div>
                                                                                    <a href style={{fontSize:14}}><i class="bi bi-calendar text-secondary me-2"></i>15/04/2024</a>

                                                                                </div>
                                                                                <div>
                                                                                    <a href class="mb-0 text-muted fw-medium" data-bs-toggle="modal" data-bs-target=".bs-example-new-task">
                                                                                        <i class="bi bi-heart pe-1 font-size-16 align-middle text-secondary"></i>0
                                                                                    </a>
                                                                                </div>
                                                                                <div>
                                                                                    <a href class="mb-0 text-muted fw-medium" data-bs-toggle="modal" data-bs-target=".bs-example-new-task">
                                                                                        <i class="bi bi-chat text-secondary pe-1 "></i>11
                                                                                    </a>
                                                                                </div>
                                                                                <div>
                                                                                    <a href class="delete-item" onclick="deleteProjects('task-item-1')">
                                                                                        <i class="bi bi-trash align-middle font-size-16 text-danger"></i>
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>       
                                        </div>       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <Footer />
    </>
    )
}

export default Historique