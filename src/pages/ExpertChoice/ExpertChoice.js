import React, { useEffect, useState } from 'react';
import './ExpertChoice.css';
import Slider from 'react-slick';
import { FaStar} from 'react-icons/fa'
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import axios from 'axios';



const ModelLogin = ({ closeModal }) => {
    const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToScroll: 1,
    rows: 2, 
    slidesPerRow: 2,
    responsive: [
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: true,
                dots: true
            }
        },
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                slidesPerRow: 1,
    
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                rows: 1,
                slidesPerRow: 1,
            }
        }
    ]
};
    const [experts, setExperts]=useState(null)

    useEffect(() => {
        const fetchData = async () => {
            try {
                const response = await axios.get(`http://localhost:8000/auth/experts`);
                setExperts(response.data);
            } catch (error) {
                console.log(error.message);
            }
        };
    
        fetchData();
    }, []);

    const storeSelectedDemandForExpert = (expertId, postId) => {
        const selectedDemands = JSON.parse(localStorage.getItem(`selectedDemands_${expertId}`)) || [];
        if (!selectedDemands.includes(postId)) {
            selectedDemands.push(postId);
            localStorage.setItem(`selectedDemands_${expertId}`, JSON.stringify(selectedDemands));
        }
    };

    const handleChooseExpert = async (expertId) => {
    try {
        let postId = localStorage.getItem('postId');
        if (!postId) {
            console.error("Post ID is null");
            return;
        }
        localStorage.setItem('chosenExpert', JSON.stringify(expertId));

        
        const token = localStorage.getItem('token');
        const response = await axios.post('http://localhost:8000/auth/choix', { expertId, postId }, {
            headers: {
                'Authorization': `Bearer ${token}`
            }
        });
        console.log(response.data.message);
        storeSelectedDemandForExpert(expertId, postId); 
        alert("Demande envoyée");
        closeModal();
    } catch (error) {
        console.log(error.message);
    }
};




    return (
        <section className="model-login-overlay">
            <div className="model-login-container">
                <button className="btn-close" onClick={closeModal}>
                    &times;
                </button>
                <h5 className="title-expert mt-4 mb-3">Sélectionner un expert pour évaluer votre voiture</h5>
                <Slider {...settings}>
                    {experts &&
                        experts.map((expert, index) => (
                            <div key={index} className="col">
                                <div className="card h-60">
                                    <div className="card-body">
                                        <h6 className="card-title">{expert.name}</h6>
                                        <p className="card-text mb-2">Expert en voitures avec plus de 5 ans d'expérience dans l'évaluation et l'analyse de divers modèles de véhicules.</p>
                                        <div className='rating'>
                                            <FaStar className='star' size={15} />
                                            <FaStar className='star' size={15} />
                                            <FaStar className='star' size={15} />
                                            <FaStar className='star' size={15} />
                                            <FaStar className='star' size={15} />
                                        </div>
                                        <button onClick={() => handleChooseExpert(expert._id)} className="btn btn-expert">Choisir cet expert</button>
                                    </div>
                                </div>
                            </div>
                        ))}
                </Slider>
            </div>
        </section>
    )
}

export default ModelLogin;