/* eslint-disable no-unused-vars */
import React, { useEffect, useRef, useState } from 'react'
import CheckIcon from '@mui/icons-material/Check';

import CloseIcon from '@mui/icons-material/Close';
import DescriptionIcon from '@mui/icons-material/Description';
import Header from '../../components/Header/Header'
import './FormExpert.css';
import axios from 'axios';
const FormExpert = () => {
    const [formData, setUserData] = useState({
        name: '',
        email: '',
        phone: '',
        address:'',
        certif:''
    });

    
    const [user, setUser] = useState(null); 

    useEffect(() => {
        const fetchUserData = async () => {
            try {
                const userId = localStorage.getItem('userId');
                if (userId) {
                const response = await axios.get(`http://localhost:8000/auth/users/${userId}`)
                setUser(response.data); 
                }
            } catch (error) {
                console.error('Une erreur est survenue lors de la récupération des données de l\'utilisateur :', error);
            }
        };

        fetchUserData(); 
    }, []);


    useEffect(() => {
        if (user) {
            setUserData({
                name: user.name || '',
                email: user.email || '',
                phone: user.phone || '',
                address: user.address || '',
                certif: '' 
            });
        }
    }, [user]);




    const handleFileChange = (event) => {
        if (event.target.files && event.target.files.length > 0) setSelectedFile(event.target.files[0]);
    };

    const onChooseFile = () => {
        inputRef.current.click();
    };

    const clearFileInput = () => {
        inputRef.current.value = '';
        setSelectedFile(null);
        setProgress(0);
        setUploadStatus('select');
    };

    const handleUploadExp = async () => {
        if (uploadStatus === 'done') {
            clearFileInput();
            return;
        }

        try {
            setUploadStatus('uploading');
            const formData = new FormData();
            formData.append('file', selectedFile);
            const response = await axios.post('http://localhost:8000/auth/uploadexp', formData, {
                onUploadProgress: (progressEvent) => {
                    const percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
                    setProgress(percentCompleted);
                },
            });
            setUploadStatus('done');
        } catch (error) {
            setUploadStatus('select');
        }
    };

    const handleChange = (e) => {
        setUserData({
            ...formData,
            [e.target.name]: e.target.value
        });
        console.log(formData);
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        if (uploadStatus !== 'done' || !selectedFile) {
            return;
        }
    
        const formDataToSend = new FormData();
        
        try {
            formDataToSend.append('name', formData.name);
            formDataToSend.append('email', formData.email);
            formDataToSend.append('phone', formData.phone);
            formDataToSend.append('address', formData.address);
            formDataToSend.append('certif', selectedFile);
            const response = await axios.post('http://localhost:8000/auth/expertdemand', formDataToSend);
            console.log('Demande d\'expert créée avec succès :', response.data); 
            alert('La demande d\'expert a été créée avec succès.');
            resetForm(); 
            setShowConfirmation(true);
        } catch (error) {
            console.error('Une erreur est survenue lors de la soumission du formulaire :', error);
        }
    };
    
    const resetForm = () => {
        setUserData({
            name: '',
            email: '',
            phone: '',
            address: ''
        });
        setSelectedFile(null);
    };


    const inputRef=useRef();
    const [selectedFile, setSelectedFile]=useState(null);
    const [progress, setProgress]=useState(0);
    const [uploadStatus, setUploadStatus]=useState('select');
    const [showConfirmation, setShowConfirmation] = useState(false); 
    
    
    return (
        <div>
            <Header activeLink="expert" setActiveLink={() => {}} />
            {showConfirmation ? (
            <div className="card border-0" style={{display:'flex',justifyItems:'center',justifyContent:'center',alignContent:'center',alignItems:'center',padding:20,marginTop:150,margin:100}}>
                <div class="card-body conf">
                    <div style={{display: 'flex', alignItems: 'center'}}>
                        <CheckIcon style={{color: 'green', marginRight: 10}} />
                        <h5 class="card-title" style={{fontWeight:'bold'}}>Merci ! Votre demande a été envoyé avec succès</h5>
                    </div>
                    <p class="card-text">Nous vous enverrons un e-mail après avoir étudié votre demande.</p>
                </div>
            </div>
            ) : (
            <section id='devenirexpert' className='devenirexpert'>
                <div className="container expform ">
                    <form onSubmit={handleSubmit}>
                        <div className='user-details'>
                            <div className='input-box1'>
                                <span className='details'>Nom</span>
                                <input type='text' placeholder='Entrer votre nom ' name='name' onChange={handleChange} value={user ? user.name : formData.name} required/>
                            </div>
                            {/* <div className='input-box form-column'>
                                <span className='details'>Prénom</span>
                                <input type='text' placeholder='Entrer votre prenom ' required/>
                            </div> */}
                            <div className='input-box1'>
                                <span className='details'>Email</span>
                                <input type='text' placeholder='Entrer votre email' name='email' onChange={handleChange} value={user ? user.email : formData.email}  required/>
                            </div>
                            <div className='input-box1'>
                                <span className='details'>Téléphone</span>
                                <input type='text' placeholder='Entrer votre telephone ' name='phone' onChange={handleChange} value={user ? user.phone : formData.phone} required/>
                            </div>
                            <div className='input-box1'>
                                <span className='details'>Adresse</span>
                                <input type='text' placeholder='Entrer votre adresse 'name='address' onChange={handleChange} value={user ? user.address : formData.address}  required/>
                            </div>
                            <div className='input-box1'>
                                <span className='details'>Certificats</span>
                                <div className='upload'>
                                <input ref={inputRef} type="file" name="certif" onChange={handleFileChange} style={{ display: 'none' }} />
                                    {/* Bouton pour choisir le fichier */}
                                    {!selectedFile && (
                                        <button className="file-btn" onClick={onChooseFile} style={{ border: 'none', background: '#fff' }}>
                                            <span>
                                                <i className="fas fa-cloud-upload-alt"></i>
                                            </span>
                                            <p>Ajouter fichiers</p>
                                        </button>
                                    )}
                                    {/* Affichage du fichier sélectionné */}
                                    {selectedFile && (
                                        <>
                                            <div className="file-card">
                                                <DescriptionIcon className="material-symbols-outlined ficon" />
                                                <div className="file-info">
                                                    <div style={{ flex: 1 }}>
                                                        <h6>{selectedFile.name}</h6>
                                                        <div className="progress-bg">
                                                            <div className="progress" style={{ width: `${progress}%` }}>
                                                                {uploadStatus === 'uploading' && `${progress}%`}
                                                            </div>
                                                        </div>
                                                    </div>
                                                    {/* Bouton pour annuler le fichier sélectionné */}
                                                    {uploadStatus === 'select' ? (
                                                        <button onClick={clearFileInput}>
                                                            <CloseIcon className="material-symbols-outlined fclose-icon" />
                                                        </button>
                                                    ) : (
                                                        <div className="check-circle">
                                                            {uploadStatus === 'done' ? (
                                                                <span className="material-symbol-outlined" style={{ fontSize: '20px' }}>
                                                                    <i class="fa fa-check" aria-hidden="true"></i>
                                                                </span>
                                                            ) : null}
                                                        </div>
                                                    )}
                                                </div>
                                            </div>

                                            {/* Bouton pour télécharger le fichier */}
                                            <button className="upload-btn" onClick={handleUploadExp}>
                                                {uploadStatus === 'uploading' ? 'Téléchargement en cours...' : 'Ajouter'}
                                            </button>
                                        </>
                                    )}
                                </div>
                            </div>
                        </div>
                        {/* Bouton pour soumettre le formulaire */}
                        <button type="submit" value="Envoyer" className="upload-btn1">
                            Envoyer
                        </button>
                    </form>
                </div>
            </section>
            )}
        </div>
    )
    }

export default FormExpert