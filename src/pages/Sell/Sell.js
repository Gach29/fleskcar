/* eslint-disable no-unused-vars */
/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable jsx-a11y/anchor-is-valid */
// Importez la position souhaitée depuis 'react-toastify'
import React, { useEffect, useState } from 'react';
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/css';
import 'swiper/css/bundle'
import 'swiper/css/navigation';
import 'swiper/css/thumbs';
import { Thumbs ,Navigation, FreeMode} from 'swiper/modules';
import car from '../../images/c3.jpg';
import car1 from '../../images/c1.jpg';
import Header from '../../components/Header/Header';
import Footer from '../../components/Footer/Footer';
import './Sell.css';
import NavBreadcrumbs from '../../components/Breadcrumbs/NavBreadcrumbs';
import ExpertChoice from '../ExpertChoice/ExpertChoice';
import Payment from '../Payment/Payment';
import Slider from 'react-slick';
import Comments from '../Comments/Comments';
import { Element, Link as ScrollLink } from 'react-scroll';
import axios from 'axios'



const Sell = () => {
    const [thumbSwiper, setThumbSwiper]= useState(null)
    const [mainSwiper, setMainSwiper] = useState(null);
    const [openModal, setOpenModal] = useState(false);
    const [openModal2, setOpenModal2] = useState(false);
    const [activeLink, setActiveLink] = useState('Caractéristiques');
    const [activeThumbIndex, setActiveThumbIndex] = useState(0);
    const [isLoggedIn, setIsLoggedIn] = useState(!!localStorage.getItem('token')); 

    

    const handleLinkClick = (link) => {
        setActiveLink(link);
    };
    const [isLikedByCurrentUser, setIsLikedByCurrentUser] = useState(false);

    const [post, setPost] = useState([]);
    
    useEffect(() => {
        fetchPost();
        fetchData();
    }, []);
    
    const fetchPost = async () => {
        try {
            const userId = localStorage.getItem('userId');
            const postId = localStorage.getItem('postId');
            const response = await axios.get(`http://localhost:8000/posts/post/${postId}`);
            const postData = response.data;
            const postUserId = postData.user; 
            localStorage.setItem('postUserId', postUserId);
            console.log(postData);
            postData.likeCount = localStorage.getItem(`likeCount_${postId}`) ? parseInt(localStorage.getItem(`likeCount_${postId}`), 10) : 0;
            postData.isFavorite = localStorage.getItem(`isFavorite_${postId}`) === 'true';
            setIsLikedByCurrentUser(postData.favoris && postData.favoris.includes(userId));

            // setPost(prevPosts => [...prevPosts, postData]);
            setPost(postData);
        } catch (error) {
            console.error('Error fetching post:', error);
        }
    };


    const[userdata,setUserData]=useState()
    

    const fetchData = async () => {
            try {
                const postUserId = localStorage.getItem('postUserId');
                if (postUserId) {
                    const response = await axios.get(`http://localhost:8000/auth/users/${postUserId}`);
                    setUserData(response.data);
                }
            } catch (error) {
                console.log(error.message);
            }
    };


    
    const handleFavoriteClick = async (postId) => {
        const token = localStorage.getItem('token');
        if (!token) {
            alert("Vous devez être connecté pour créer une publication.");
            return;
        }
        try {
            const userId = localStorage.getItem('userId');
            const isPostLikedByCurrentUser = post.favoris && post.favoris.includes(userId);
            if (isPostLikedByCurrentUser) {
                alert("Le post est déjà dans vos favoris.");
                return;
            }
            const response = await axios.post(`http://localhost:8000/posts/favoris/${postId}`, userId, {
                headers: {
                    'Authorization': `Bearer ${token}`,
                }
            });
            console.log(response.data);
            post.favoris.push(userId)
    
            const updatedLikeCount = post.likeCount + 1;
    
            setPost(prevPost => ({ ...prevPost, likeCount: updatedLikeCount }));
    
            setIsLikedByCurrentUser(true);
    
            localStorage.setItem(`isFavorite_${postId}`, true);
            localStorage.setItem(`likeCount_${postId}`, updatedLikeCount);
    
        } catch (error) {
            console.error('Error adding post to favorites:', error);
        }
    };
    
    



    

    useEffect(() => {
        if (mainSwiper && thumbSwiper) {
            const handleSlideChange = () => {
                const newIndex = mainSwiper.activeIndex;
                setActiveThumbIndex(newIndex);
                thumbSwiper.slideTo(newIndex);
                console.log(newIndex); 
            };

            mainSwiper.on('slideChange', handleSlideChange);

            return () => {
                mainSwiper.off('slideChange', handleSlideChange);
            };
        }
    }, [mainSwiper, thumbSwiper]);


        const handleThumbClick = (index) => {
            if (mainSwiper !== null) {
                setActiveThumbIndex(index);
                mainSwiper.slideTo(index);
            }
            console.log(index);
        };

        const formatDateTime = (dateString) => {
            const date = new Date(dateString);
            const formattedDate = date.toLocaleDateString('fr-FR');
            const formattedTime = date.toLocaleTimeString('fr-FR', { hour: '2-digit', minute: '2-digit' });
            return `${formattedDate}`;
        };
        const isUserLikedPost = post.favoris && post.favoris.includes(localStorage.getItem('userId'));


    return (
        <div>
            <Header activeLink="anonces" setActiveLink={() => {}} />
            {post.marque && <NavBreadcrumbs selectedCar={post.marque} />}
            <section className='sell'>
                <div className='container' >
                    <div className='row gx-4'>
                        <div className='product-swiper col-lg-8'>
                        <Swiper
                                onSwiper={setMainSwiper}
                                grabCursor={true}
                                spaceBetween={10}
                                navigation={true}
                                thumbs={thumbSwiper ? { swiper: thumbSwiper } : false}
                                modules={[FreeMode,Navigation, Thumbs]}
                                className="mySwiper2"
                                // onSlideChange={handleSlideChange}
                            >
                                {post.files && post.files.length > 0 && post.files.map((thumb, index) => (
                                    <SwiperSlide key={index}>
                                        <img src={`http://localhost:8000/posts/carPhotos/${thumb}`} alt={`Thumb ${index}`}/>
                                        <div className="favcom position-absolute top-0 end-0 mt-3 me-3" style={{fontSize:'20px'}}>
                                            <i className={`bi bi-heart-fill fav me-1 ${isLikedByCurrentUser ? 'active' : ''}`}  onClick={() => handleFavoriteClick(post._id)}></i>
                                            <span className='' style={{fontSize:18,color:'#a09e9e',fontWeight:'bold',paddingLeft:1}}>{post.likeCount}</span>
                                            <ScrollLink to="comments" spy={true} smooth={true} duration={500}><i className="bi bi-chat-fill com ms-3"></i></ScrollLink>                                    
                                        </div>
                                    </SwiperSlide>
                                ))}
                            </Swiper>
                            <Swiper
                                onSwiper={setThumbSwiper}
                                spaceBetween={10}
                                slidesPerView={4}
                                freeMode={true}
                                watchSlidesVisibility={true}
                                watchSlidesProgress={true}
                                modules={[FreeMode,Navigation, Thumbs]}
                                className="mySwiper"
                                style={{ marginTop: "10px" }}
                            >
                                {post.files && post.files.length > 0 && post.files.map((thumb, index) => (
                                    <SwiperSlide key={index}>
                                        <img
                                            src={`http://localhost:8000/posts/carPhotos/${thumb}`}
                                            alt={`Thumb ${index}`}
                                            onClick={() => handleThumbClick(index)}
                                            className={activeThumbIndex === index ? 'active' : ''}
                                        />
                                    </SwiperSlide>
                                ))}
                            </Swiper>
                        </div>
                        <div className='col-lg-4'>
                            <div className="portfolio-info mt-0 mb-0">
                                <h4>{post.marque}</h4>
                                <p>{post.modele}</p>
                                <ul>
                                    <li className='d-flex justify-content-between'>Crée le <span><i className="bi bi-calendar-fill me-1" style={{fontWeight:'bold'}}></i> {formatDateTime(post.createdAt)}</span></li>
                                </ul>
                                <p className='prix d-flex justify-content-between'><span>Prix</span><span>{post.prix} DT</span></p>
                                <div className='garantie d-flex justify-content-between'><span>Financement</span><i className="bi bi-calculator"></i>
                                    <div className="d-flex flex-column">
                                        <span>117 €/Mois</span>
                                        <span style={{ fontSize: '13px'}}>après un premier loyer de 4 390€</span>
                                    </div>
                                </div>
                                <div className="optionv d-flex mt-1 ">
                                    <button className='btn btn-secondary btn-sm ' style={{ fontSize: '11px', width: 'fit-content', marginRight: '5px' }}>{post.energie}</button>
                                    <button className='btn btn-secondary btn-sm ' style={{ fontSize: '11px',  width: 'fit-content', marginRight: '5px' }}>{post.boiteDeVitesse}</button>
                                    <button className='btn btn-secondary btn-sm ' style={{ fontSize: '11px',  width: 'fit-content', marginRight: '5px' }}>{post.annee}</button>
                                    <button className='btn btn-secondary btn-sm ' style={{ fontSize: '11px',  width: 'fit-content' }}>{post.carrosserie}</button>
                                </div>
                                <span className='livraison border-0' style={{ fontSize: '15px' }}><i class="bi bi-house pe-2"></i>Livraison à domicile</span>
                                <div className="butsell">
                                    <button className='btns btns1' style={{ fontSize: '15px' }}  onClick={() => setOpenModal2(true)}>Réserver</button>
                                    <button className='btns' style={{ fontSize: '15px' }} onClick={() => setOpenModal(true)}>Passer par expert</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <section className="bg-white">
                            <div className="row gy-4">
                                <div className="col-md-12 mb-4 portfolio-info1">
                                    <div className="rounded-2 bg-white">
                                    <ul className="nav nav-pills nav-justified mb-3" id="ex1" role="tablist">
                                        <li className={`nav-item car-detail d-flex`} onClick={() => handleLinkClick('Caractéristiques')} >
                                            <a className={`nav-link d-flex border-0 align-items-center w-100 ${activeLink === 'Caractéristiques' ? 'active' : ''}`} aria-selected={activeLink === 'Caractéristiques'}>Caractéristiques</a>
                                        </li>
                                        <li className={`nav-item car-detail d-flex `} onClick={() => handleLinkClick('Financement')} >
                                            <a className={`nav-link d-flex border-0 align-items-center justify-content-center w-100 ${activeLink === 'Financement' ? 'active' : ''}`} aria-selected={activeLink === 'Financement'}>Financement</a>
                                        </li>
                                        <li className={`nav-item car-detail d-flex`} onClick={() => handleLinkClick('Etat')} >
                                            <a className={`nav-link d-flex border-0 align-items-center justify-content-center w-100 ${activeLink === 'Etat' ? 'active' : ''}`}  aria-selected={activeLink === 'Etat'}>Etat</a>
                                        </li>
                                    </ul>
                                        <div className="tab-content" id="ex1-content">
                                            <div className={`tab-pane fade ${activeLink === 'Caractéristiques' ? 'show active' : ''}`} id="ex1-pills-1" role="tabpanel" aria-labelledby="ex1-tab-1">
                                                <h5>Détails</h5>
                                                <div className="row mb-2">
                                                    <div className="col-6">
                                                        <div className="card border-1" style={{ height:'max-content' ,fontSize:'14.4px'}}>
                                                            <div className="card-body">
                                                                <span className="card-title">Spécifications</span>
                                                                <ul className="list-unstyled mb-0">
                                                                    <li className="d-flex justify-content-between border-bottom py-3">
                                                                        <span className='text-muted'>Marque</span>
                                                                        <span>{post.marque}</span>
                                                                    </li>
                                                                    <li className="d-flex justify-content-between border-bottom py-3">
                                                                        <span className='text-muted'>Modèle</span>
                                                                        <span>{post.modele}</span>
                                                                    </li>
                                                                    <li className="d-flex justify-content-between border-bottom py-3">
                                                                        <span className='text-muted'>Mise en circulation</span>
                                                                        <span>{post.annee}</span>
                                                                    </li>
                                                                    <li className="d-flex justify-content-between border-bottom py-3">
                                                                        <span className='text-muted'>Couleur intérieure</span>
                                                                        <span>{post.couleurInterieure}</span>
                                                                    </li>
                                                                    <li className="d-flex justify-content-between border-bottom py-3">
                                                                        <span className='text-muted'>Sellerie</span>
                                                                        <span>{post.sellerie}</span>
                                                                    </li>
                                                                    <li className="d-flex justify-content-between py-3">
                                                                        <span className='text-muted'>Nombre de places</span>
                                                                        <span>{post.nombrePortes}</span>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="col-6" style={{ paddingLeft: '30px' }}>
                                                        <div className="card border-1 "style={{ height:'max-content',fontSize:'14.4px' }}>
                                                            <div className="card-body">
                                                                <span className="card-title ">Motorisation</span>
                                                                <ul className="list-unstyled mb-0">
                                                                    <li className="d-flex justify-content-between border-bottom py-3">
                                                                        <span className='text-muted'>Energie</span>
                                                                        <span>{post.energie}</span>
                                                                    </li>
                                                                    <li className="d-flex justify-content-between border-bottom py-3">
                                                                        <span className='text-muted'>Kilométrage</span>
                                                                        <span>{post.kilometrage} Km</span>
                                                                    </li>
                                                                    <li className="d-flex justify-content-between border-bottom py-3">
                                                                        <span className='text-muted'>Boite de vitesses</span>
                                                                        <span>{post.boiteDeVitesse}</span>
                                                                    </li>
                                                                    <li className="d-flex justify-content-between border-bottom py-3">
                                                                        <span className='text-muted'>Transmission </span>
                                                                        <span>{post.transmission}</span>
                                                                    </li>
                                                                    <li className="d-flex justify-content-between border-bottom  py-3">
                                                                        <span className='text-muted'>Carosserie</span>
                                                                        <span>{post.carrosserie}</span>
                                                                    </li>
                                                                    <li className="d-flex justify-content-between py-3">
                                                                        <span className='text-muted'>Puissance fiscale</span>
                                                                        <span>{post.puissance}CV</span>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <h5 className='mt-3'>Equipements</h5>
                                                <div className="row mb-2">
                                                    <div className="col-6">
                                                        <div className="card border-1" style={{ height:'max-content' ,fontSize:'14.4px'}}>
                                                            <div className="card-body">
                                                            <span class="card-title">Sécurité</span>
                                                                <ul class="list-unstyled mb-0 pt-2">
                                                                    <li class="d-flex pt-2">
                                                                        <span><i class="fas fa-check pe-2"></i></span>
                                                                        <span>ABS</span>
                                                                    </li>
                                                                    <li class="d-flex pt-2">
                                                                        <span><i class="fas fa-check pe-2"></i></span>
                                                                        <span>Airbags frontaux</span>
                                                                    </li>
                                                                    <li class="d-flex pt-2 ">
                                                                        <span><i class="fas fa-check pe-2"></i></span>
                                                                        <span>Airbags latéraux</span>
                                                                    </li>
                                                                    <li class="d-flex pt-2">
                                                                    <span><i class="fas fa-check pe-2"></i></span>
                                                                        <span>Alarme anti-intrusion</span>
                                                                    </li>
                                                                    <li class="d-flex pt-2">
                                                                        <span><i class="fas fa-check pe-2"></i></span>
                                                                        <span>Anti-démarrage électronique</span>
                                                                    </li>
                                                                    <li class="d-flex pt-2">
                                                                        <span><i class="fas fa-check pe-2"></i></span>
                                                                        <span>Anti-patinage</span>
                                                                    </li>
                                                                    <li class="d-flex pt-2">
                                                                        <span><i class="fas fa-check pe-2"></i></span>
                                                                        <span>Contrôle de pression des pneus</span>
                                                                    </li>
                                                                    <li class="d-flex pt-2">
                                                                        <span><i class="fas fa-check pe-2"></i></span>
                                                                        <span>Fixations ISOFIX</span>
                                                                    </li>
                                                                    <li class="d-flex pt-2">
                                                                        <span><i class="fas fa-check pe-2"></i></span>
                                                                        <span>Radiateur tropicalisé</span>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="col-6" style={{ paddingLeft: '30px' }}>
                                                        <div className="card border-1 "style={{ height:'max-content',fontSize:'14.4px' }}>
                                                            <div className="card-body">
                                                                <span className="card-title ">Fonctionnels</span>
                                                                <ul class="list-unstyled mb-0 pt-2">
                                                                    <li class="d-flex pt-2">
                                                                        <span><i class="fas fa-check pe-2"></i></span>
                                                                        <span>Accès sans clé</span>
                                                                    </li>
                                                                    <li class="d-flex pt-2">
                                                                        <span><i class="fas fa-check pe-2"></i></span>
                                                                        <span>Allumage automatique des feux</span>
                                                                    </li>
                                                                    <li class="d-flex pt-2">
                                                                        <span><i class="fas fa-check pe-2"></i></span>
                                                                        <span>Bouton Start & Stop</span>
                                                                    </li>
                                                                    <li class="d-flex pt-2">
                                                                        <span><i class="fas fa-check pe-2"></i></span>
                                                                        <span>Climatisation automatique</span>
                                                                    </li>
                                                                    <li class="d-flex pt-2">
                                                                        <span><i class="fas fa-check pe-2"></i></span>
                                                                        <span>Direction assistée</span>
                                                                    </li>
                                                                    <li class="d-flex pt-2">
                                                                        <span><i class="fas fa-check pe-2"></i></span>
                                                                        <span>Fermeture centralisée</span>
                                                                    </li>
                                                                    <li class="d-flex pt-2">
                                                                        <span><i class="fas fa-check pe-2"></i></span>
                                                                        <span>Ordinateur de bord</span>
                                                                    </li>
                                                                    <li class="d-flex pt-2">
                                                                        <span><i class="fas fa-check pe-2"></i></span>
                                                                        <span>Régulateur de vitesse</span>
                                                                    </li>
                                                                    <li class="d-flex pt-2">
                                                                        <span><i class="fas fa-check pe-2"></i></span>
                                                                        <span>Rétroviseurs électriques</span>
                                                                    </li>
                                                                </ul>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            {/* <div className={`tab-pane fade ${activeLink === 'Etat' ? 'show active' : ''}`} id="ex1-pills-3" role="tabpanel" aria-labelledby="ex1-tab-3">
                                                <h5>Etat du voiture</h5>
                                                <div className="row mb-2">
                                                        <div className='col-12'>
                                                            <div className="card border-1" style={{ height:'max-content' ,fontSize:'14.4px'}}>
                                                                <div className="card-body">
                                                                    <ul className="list-unstyled mb-0">
                                                                        <li className="d-flex justify-content-between border-bottom py-3">
                                                                            <span className='text-muted'>Présence des justificatifs d'entretien</span>
                                                                            <span>{post.selectedOptions && post.selectedOptions[0]}</span>
                                                                        </li>
                                                                        <li className="d-flex justify-content-between border-bottom py-3">
                                                                            <span className='text-muted'>Fonctionnement parfait du moteur</span>
                                                                            <span>{post.selectedOptions && post.selectedOptions[1]}</span>
                                                                        </li>
                                                                        <li className="d-flex justify-content-between border-bottom py-3">
                                                                            <span className='text-muted'>Fonctionnement parfait du boite de vitesses</span>
                                                                            <span>{post.selectedOptions && post.selectedOptions[2]}</span>
                                                                        </li>
                                                                        <li className="d-flex justify-content-between border-bottom py-3">
                                                                            <span className='text-muted'>Tous les voyants du tableau de bord sont éteints</span>
                                                                            <span>{post.selectedOptions && post.selectedOptions[3]}</span>
                                                                        </li>
                                                                        <li className="d-flex justify-content-between py-3">
                                                                            <span className='text-muted'>La véhicule a subi un choc et/ou un accident </span>
                                                                            <span>{post.selectedOptions && post.selectedOptions[4]}</span>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>                                            
                                                </div> */}
                                            </div>
                                    </div>
                                </div>
                                <div className="col-lg-4">
                                        <div className="portfolio-info2">
                                            <div className="card-body">
                                                <div className="card-title">
                                                    <h5 >Informations sur le vendeur</h5>
                                                </div>
                                                <div className="d-flex mb-3 mt-4">
                                                    <a href="#" className="me-3">
                                                        <img src={userdata &&  `http://localhost:8000/auth/pictures/${userdata.profileImage}`}style={{ minWidth: '96px', height: '96px' }} className="img-md img-thumbnail" />
                                                    </a>
                                                    <div className="info text-center mt-4" style={{ fontSize: '24px' }}>
                                                        <span> {userdata && userdata.name}</span>
                                                    </div>
                                                </div>
                                                <div className='d-flex justify-content-center mb-5 mt-5'>
                                                    <button type="button" class="btn btn-outline-secondary me-2 btnv p-2"><i class="bi bi-car-front text-secondary me-2 "></i>Consulter le stock de vendeur</button>
                                                </div>

                                                <div className="d-flex mb-2">
                                                    <i class="bi bi-crosshair me-3"></i>
                                                    <div className="info">
                                                        <a href="#" className="nav-link mb-1">{userdata && userdata.address}</a>
                                                    </div>
                                                </div>
                                                <div className="d-flex mb-2">
                                                    <i class="bi bi-clock me-3"></i>
                                                    <div className="info">
                                                        <a href="#" className="nav-link mb-1">08H30 à 19H00</a>
                                                    </div>
                                                </div>
                                                <div className="d-flex mb-3">
                                                    <i className="bi bi-telephone me-3"></i>
                                                    <div className="info">
                                                        <a href="#" className="nav-link mb-1">+216 {userdata && userdata.phone}</a>
                                                    </div>
                                                </div>
                                                <div className="d-flex justify-content-center mt-4 mb-4">
                                                    <div className="d-flex align-items-center">
                                                        <a href="https://api.whatsapp.com/send?phone=+21699888555" className="btn btn-outline-secondary mx-2 btnv" style={{ width: '140px', fontSize: '14px', display: 'flex', alignItems: 'center' }}>
                                                            <i className="fab fa-whatsapp p-2 ps-2 me-2" style={{ color: 'rgb(13, 153, 13)', marginRight: '0.5rem' }}></i>
                                                            <span>WhatsApp</span>
                                                        </a>
                                                        <a href={`mailto:${userdata && userdata.email}`} className="btn btn-outline-secondary mx-2 btnv " style={{ width: '140px', fontSize: '14px', display: 'flex', alignItems: 'center' }}>
                                                            <i className="fas fa-envelope p-2 ps-3 me-2" style={{ color: 'rgb(233, 55, 55)', marginRight: '0.5rem' }}></i>
                                                            <span className="">E-mail</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                    </section>
                    <div className='comm mt-3 mb-5'>
                        <h4>Vos commentaires</h4>
                        <span>Donnez votre avis</span>
                    </div>
                    <Element name="comments" className="comments"><Comments /></Element>
                    
                </div>
            </section>
            {openModal && <ExpertChoice closeModal={() => setOpenModal(false)} />}
            {openModal2 && <Payment closeModal={() => setOpenModal2(false)} />}
            <Footer />
        </div>
    )
}

export default Sell;
