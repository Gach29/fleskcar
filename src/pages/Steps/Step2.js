import React, { useEffect} from "react";
import './Steps.css';
import { Typography, Button } from "@material-ui/core";
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import DeleteIcon from '@material-ui/icons/Delete';
import { makeStyles } from '@material-ui/core/styles';
import axios from "axios";

const useStyles = makeStyles({
  mainContainer: {
    display: "grid",
    justifyContent: "start",
    marginLeft:"3.8rem",
    fontSize:"1rem",
    position: "relative",
    zIndex: 5,
  },
  formContainer: {
    position: "relative",
    width: "40rem",
    height: "auto",
    padding: "2rem"
  },
  btn: {
    height: "3rem",
    color: "#fff",
    background: "#cc7b3d",
    marginTop: "2rem",
    "&:hover": {
      background: "#cc7b3d",
      opacity: ".7",
      transition: ".3s ease-in-out"
    }
  },
  btndisable:{
    background:"rgba(0,0,0,0.38)",
    height: "3rem",
    color: "#fff",
    marginTop: "2rem",
  }
});

const Step2 = ({ activeStep, steps, handleNext, handlePrevious, formData, saveFormData,selectedOptions,setSelectedOptions,files,setFiles}) => {

  const classes = useStyles();



  // useEffect(() => {
  //   const fileNames = Object.keys(files).map(key => files[key].name);
  //   console.log("Noms des fichiers sélectionnés :", fileNames);
  // }, [files]);

  useEffect(() => {
    console.log("Fichiers sélectionnées:", files);
  }, [files]);

  useEffect(() => {
    console.log("Options sélectionnées:", selectedOptions);
  }, [selectedOptions]);




  const removeFile = (type) => {
    const updatedFiles = { ...files };
    delete updatedFiles[type];
    setFiles(updatedFiles);
  }

  

  const handleOptionChange = (event) => {
    console.log("Option changed:", event.target.name, event.target.value);
    const { name, value } = event.target;
    setSelectedOptions({ ...selectedOptions, [name]: value });
  };

  // const handleNextClick = () => {
  //   const isAllSelected = Object.values().every(value => value !== null);
  //   const isAllOptionsSelected = Object.values(selectedOptions).every(value => value !== '');
    
  //   if (!isAllSelected && isAllOptionsSelected) {
  //     console.log("Fichiers sélectionnés :", );
  //     console.log("Options sélectionnées:", selectedOptions);
  //     saveFormData({ ...formData, ...selectedOptions, ...files }); 
  //     handleNext();
  //   } else {
  //     alert("Veuillez sélectionner tous les fichiers et options avant de passer à l'étape suivante.");
  //   }
  // };


  const handleFile = (event) => {
    const { name } = event.target;
    setFiles({ ...files, [name]: event.target.files[0] });
  };
  

  // const handleNextClick = async () => {
  //   const isAllFilesSelected = Object.values(files).every(value => value !== null);
  //   const isAllOptionsSelected = Object.values(selectedOptions).every(value => value !== '');
    
  //   if (!isAllFilesSelected || !isAllOptionsSelected) {
  //     alert("Veuillez sélectionner tous les fichiers et options avant de passer à l'étape suivante.");
  //     return;
  //   }

  
  //   console.log("Fichiers sélectionnés :", files);
  //   saveFormData(files)
  //   setFiles(files)
  //   console.log("Options sélectionnées:", selectedOptions);
  //   const updatedFormData = { ...formData};
  //   updatedFormData.selectedOptions = selectedOptions;
  //   updatedFormData.files=files;
  //   saveFormData(updatedFormData);
  //   handleNext(); 
  // };

  
  const handleNextClick = async () => {
  try {
    const token = localStorage.getItem('token');
    if (!token) {
      throw new Error("Vous devez être connecté pour télécharger les images.");
    }

    const formDataWithFiles = new FormData();
    saveFormData(formDataWithFiles);

    Object.values(files).forEach(file => {
      if (file) {
        formDataWithFiles.append('files',file);
      }
    });

    saveFormData(formDataWithFiles);
    const response = await axios.post('http://localhost:8000/posts/uploadcarphoto', formDataWithFiles, {
      headers: {
        'Content-Type': 'multipart/form-data',
        'Authorization': `Bearer ${token}`
      }
    });

    console.log(response)
    console.log(formDataWithFiles);
    const updatedFormData = { ...formData};
    setFiles(response.data.filePaths)
    updatedFormData.selectedOptions = selectedOptions;
    updatedFormData.files=response.data.filePaths;
    updatedFormData.filesTest=response.data.filePaths;
    saveFormData(updatedFormData)
    handleNext();

  } catch (error) {
    console.error('Erreur lors du téléchargement des images :', error);
  }
};
  

  return (
    <div className={classes.mainContainer}>
      <Typography
        variant="h5"
        style={{marginLeft:"2.2rem",marginTop:"2.8rem",marginBottom:"0.8rem",borderLeft:"7px solid #cc7b3d" ,paddingLeft:"8px"}}
      >
        Photos de véhicule
      </Typography>
      <div className={classes.formContainer}>
        <form>
          <div style={{ display: "flex", flexDirection: "column" }}>
            {/* Image "Avant" */}
            <div style={{ display: "flex", flexWrap: "wrap",alignItems: "center", marginTop: "1rem", position: 'relative' }}>
              <Typography variant="subtitle2" style={{ fontSize: "1.1rem", marginBottom: "0.8rem", marginRight: "1.5rem" }}>Avant</Typography>
              {files && files.avant  ? (
                <>
                  <img src={URL.createObjectURL(files.avant)} alt="Avant" style={{ maxWidth: "90%",maxHeight: "80%", marginTop: "1rem", marginBottom: "2rem" }} />
                  <DeleteIcon onClick={() => removeFile('avant')} className="deleteCar"/>
                </>
              ) : (
                <>
                  <label style={{ cursor: "pointer", width: "230px" ,marginLeft:"17.62rem"}}  className="boximg" htmlFor="fileInputAvant">
                    Choisir une image
                    <CloudUploadIcon style={{ fontSize: "20", marginLeft: "1rem" }} />
                  </label>
                  <input
                    id="fileInputAvant"
                    type="file"
                    name="avant"
                    style={{ display: "none" }}
                    onChange={handleFile}
                  />
                </>
              )}
            </div>


            {/* Image "Arrière" */}
            <div style={{ display: "flex", flexWrap: "wrap",alignItems: "center", marginTop: "1rem", position: 'relative' }}>
              <Typography variant="subtitle2" style={{ fontSize: "1.1rem", marginBottom: "0.8rem", marginRight: "1.5rem" }}>Arrière</Typography>
              {files && files.arriere  ?(
                <>
                  <img src={URL.createObjectURL(files.arriere)} alt="Arrière" style={{ maxWidth: "90%",maxHeight: "80%", marginTop: "1rem", marginBottom: "2rem" }} />
                  <DeleteIcon onClick={() => removeFile('arriere')} className="deleteCar"/>
                </>
              ) : (
                <>
                  <label style={{ cursor: "pointer", width: "230px" ,marginLeft:"17.15rem"}}  className="boximg" htmlFor="fileInputArriere">
                    Choisir une image
                    <CloudUploadIcon style={{ fontSize: "20", marginLeft: "1rem" }} />
                  </label>
                  <input
                    id="fileInputArriere"
                    type="file"
                    name="arriere"
                    style={{ display: "none" }}
                    onChange={handleFile}
                  />
                </>
              )}
            </div>

            {/* Image "Tableau de bord" */}
            <div style={{ display: "flex", flexWrap: "wrap", alignItems: "center", marginTop: "1rem", position: 'relative' }}>
              <Typography variant="subtitle2" style={{ fontSize: "1.1rem", marginBottom: "0.8rem", marginRight: "1.1rem" }}>Tableau de bord</Typography>
              {files && files.tableauDeBord ? (
                <>
                  <img src={URL.createObjectURL(files.tableauDeBord)} alt="Tableau de bord" style={{ maxWidth: "90%",maxHeight: "80%", marginTop: "1rem", marginBottom: "2rem" }} />
                  <DeleteIcon onClick={() => removeFile('tableauDeBord')} className="deleteCar" />
                </>
              ) : (
                <>
                  <label style={{ cursor: "pointer", width: "230px" ,marginLeft:"13.45em"}}  className="boximg" htmlFor="fileInputTableauDeBord">
                    Choisir une image
                    <CloudUploadIcon style={{ fontSize: "20", marginLeft: "1rem" }} />
                  </label>
                  <input
                    id="fileInputTableauDeBord"
                    type="file"
                    name="tableauDeBord"
                    style={{ display: "none" }}
                    onChange={handleFile}
                  />
                </>
              )}
            </div>

            {/* Image "Compteur kilométrique" */}
            <div style={{ display: "flex", flexWrap: "wrap", alignItems: "center", marginTop: "1rem", position: 'relative' }}>
              <Typography variant="subtitle2" style={{ fontSize: "1.1rem", marginBottom: "0.3rem", marginRight: "1.1rem" }}>Compteur kilométrique</Typography>
              {files && files.compteurKilometrique  ?(
                <>
                  <img src={URL.createObjectURL(files.compteurKilometrique)} alt="Compteur kilométrique" style={{maxWidth: "90%",maxHeight: "80%", marginTop: "1rem", marginBottom: "2rem" }} />
                  <DeleteIcon onClick={() => removeFile('compteurKilometrique')} className="deleteCar" />
                </>
              ) : (
                <>
                  <label style={{ cursor: "pointer", width: "230px" ,marginLeft:"10.45em"}}  className="boximg" htmlFor="fileInputCompteurKilometrique">
                    Choisir une image
                    <CloudUploadIcon style={{ fontSize: "20", marginLeft: "1rem" }} />
                  </label>
                  <input
                    id="fileInputCompteurKilometrique"
                    type="file"
                    name="compteurKilometrique"
                    style={{ display: "none" }}
                    onChange={handleFile}
                  />
                </>
              )}
            </div>


              <Typography 
                variant="h5"
                style={{marginTop:"2.8rem",marginBottom:"1.8rem",borderLeft:"7px solid #cc7b3d" ,paddingLeft:"8px"}}
                >
                Etat mécanique
              </Typography>

              <div style={{ display: "flex", alignItems: "center", marginBottom: "0.4rem" }}>
                  <label  style={{ marginRight: "9.4rem" }}>Avez-vous des justificatifs d'entretien ?</label>
                  <input
                    className="choix"
                    type="radio"
                    name="entretien"
                    value="oui"
                    checked={selectedOptions.entretien === 'oui'}
                    onChange={handleOptionChange}
                  />
                  <label style={{ marginRight: "1rem", marginBottom: "0" }} >Oui</label>
                  <input
                    className="choix"
                    type="radio"
                    name="entretien"
                    value="non"
                    checked={selectedOptions.entretien === 'non'}
                    onChange={handleOptionChange}
                  />
                  <label style={{marginBottom: "0" }}>Non</label>
              </div>
              <div style={{ display: "flex", alignItems: "center", marginBottom: "0.4rem" }}>
                  <label style={{ marginRight: "8.55rem" }}>Le moteur fonctionne-t-il parfaitement ?</label>
                  <input
                    className="choix"
                    type="radio"
                    name="moteur"
                    value="oui"
                    checked={selectedOptions.moteur === 'oui'}
                    onChange={handleOptionChange}
                  />
                  <label style={{ marginRight: "1rem", marginBottom: "0" }}>Oui</label>
                  <input
                      className="choix"
                      type="radio"
                      name="moteur"
                      value="non"
                      checked={selectedOptions.moteur === 'non'}
                      onChange={handleOptionChange}
                  />
                  <label style={{marginBottom: "0" }} >Non</label>  
              </div>
              <div style={{ display: "flex", alignItems: "center", marginBottom: "0.4rem" }}>
                  <label style={{ marginRight: "4rem" }}>La boite de vitesse fonctionne-t-elle parfaitement ?</label>
                  <input
                    className="choix"
                    type="radio"
                    name="boiteDeVitesses"
                    value="oui"
                    checked={selectedOptions.boiteDeVitesses === 'oui'}
                    onChange={handleOptionChange}
                  />
                  <label style={{ marginRight: "1rem", marginBottom: "0" }}>Oui</label>
                  <input
                    className="choix"
                    type="radio"
                    name="boiteDeVitesses"
                    value="non"
                    checked={selectedOptions.boiteDeVitesses === 'non'}
                    onChange={handleOptionChange}
                  />
                  <label style={{marginBottom: "0" }} >Non</label>
              </div>
              <div style={{ display: "flex", alignItems: "center", marginBottom: "0.4rem" }}>
                  <label style={{ marginRight: "3.5rem" }}>Les voyants du tableau de bord sont-ils tous éteints ?</label>
                  <input
                    className="choix"
                    type="radio"
                    name="voyants"
                    value="oui"
                    checked={selectedOptions.voyants === 'oui'}
                    onChange={handleOptionChange}
                    />
                  <label style={{ marginRight: "1rem", marginBottom: "0" }}>Oui</label>
                  <input
                    className="choix"
                    type="radio"
                    name="voyants"
                    value="non"
                    checked={selectedOptions.voyants === 'non'}
                    onChange={handleOptionChange}
                  />
                  <label style={{marginBottom: "0" }}>Non</label>
              </div>
              <div style={{ display: "flex", alignItems: "center", marginBottom: "0.4rem" }}>
                  <label style={{ marginRight: "4rem" }}>Votre véhicule a-t-il subi un choc et/ou un accident ?</label>
                  <input
                  className="choix"
                  type="radio"
                  name="accident"
                  value="oui"
                  checked={selectedOptions.accident=== 'oui'}
                  onChange={handleOptionChange}
                  />
                  <label style={{ marginRight: "1rem", marginBottom: "0" }}>Oui</label>
                  <input 
                    className="choix"
                    type="radio"
                    name="accident"
                    value="non"
                    checked={selectedOptions.accident === 'non'}
                    onChange={handleOptionChange}
                  />
                  <label style={{marginBottom: "0" }} >Non</label>
              </div>

              <div style={{display:'flex', justifyContent: 'space-between'}}>
                <Button
                    className={classes.btndisable}
                    variant="contained"
                    onClick={handlePrevious}
                    disabled={activeStep === 0}
                    style={{minWidth: '7rem'}} 
                >
                    Precedent
                </Button>

                <Button
                    className={classes.btn}
                    variant="contained"
                    onClick={handleNextClick}
                    style={{minWidth: '7rem'}} 
                >
                    {activeStep === steps.length ? "Finish" : "Continuer"}
                </Button>

                
              </div>

              
            </div>
            
            
    
        </form>
      </div>
    </div> 
  )
}

export default Step2;
