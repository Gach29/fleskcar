import React, { useState } from "react";
import './Steps.css';
import { Typography, Button } from "@material-ui/core";
import { makeStyles } from '@material-ui/core/styles';


const useStyles = makeStyles({
  mainContainer: {
    display: "grid",
    justifyContent: "start",
    marginLeft:"3.8rem",
    
  },
  formContainer: {
    position: "relative",
    width: "40rem",
    height: "auto",
    padding: "2rem",
    fontSize:"16px",
  },
  btn: {
    height: "2.6rem",
    color: "#fff",
    background: "#cc7b3d",
    marginLeft: "28.8rem",
    marginTop: "1rem",
    "&:hover": {
      background: "#ac5a0c",
      opacity: ".7",
      transition: ".3s ease-in-out"
    }
  },
});

const marquesModeles = {
  'Toyota': ['Corolla', 'Yaris', 'RAV4', 'Camry', ],
  'Ford': ['Fiesta', 'Focus', 'Mustang', 'Escape',],
  'Chevrolet': ['Camaro', 'Impala', 'Malibu', 'Equinox',],
  'Mini': ['Cooper', 'Countryman', 'Clubman', 'Convertible', ],
  'Audi': ['A1','A3', 'A4', 'A5', 'A6',],
  'BMW': ['3 Series', '5 Series', 'X3', 'X5',],
  'Mercedes-Benz': ['C-Class', 'E-Class', 'GLC', 'GLE', ],
  'Volkswagen': ['Golf', 'Passat', 'Tiguan', 'Atlas', ],
  'Hyundai': ['Elantra', 'Sonata', 'Tucson', 'Santa Fe', ],
  'Nissan': ['Altima', 'Sentra', 'Rogue', 'Pathfinder', ],
  'Jeep': ['Wrangler', 'Grand Cherokee', 'Cherokee', 'Compass', ],
  'Kia': ['Forte', 'Optima', 'Sportage', 'Sorento',],
  'Tesla': ['Model S', 'Model 3', 'Model X', 'Model Y',],
  'Fiat': ['500', '500X', '500L', '124 Spider',],
  'Porsche': ['911', 'Cayenne', 'Macan', 'Panamera', ],
  'Jaguar': ['XE', 'XF', 'F-Pace', 'E-Pace', ],
  'Lexus': ['ES', 'RX', 'NX', 'IS', ],
  'Land Rover': ['Discovery', 'Range Rover Evoque', 'Defender', 'Discovery Sport',],
  'Volvo': ['XC60', 'XC90', 'S60', 'V60', ],
  'Citroën': ['C3', 'C4', 'C5', 'Berlingo',],
};

const energies =[
  'Diesel',
  'Essence',
  "Hybride"
]

const carrosserieTypes = [
  "Berline",
  "Coupé",
  "Cabriolet",
  "Break",
  "SUV",
  "Monospace",
  "Crossover",
  "Compacte",
  "Camionnette",
  "Citadine",
];

const annees =[
  '2024',
  '2023',
  '2022',
  '2021',
  '2020',
  '2019',
  '2018',
  '2017',
  '2016',
  '2015',
  '2014',
  '2013',
  '2012',
  '2011',
  '2010',
]

const vitesse =[
  "Manuelle",
  "Automatique",
]

const transmission=[
  "Traction",
  "Intégrale",
  "Propulsion"
]

const marques = Object.keys(marquesModeles);


const Step1 = ({ activeStep, steps, handleNext, formData, handleFormDataChange, saveFormData }) => {
  
  const classes = useStyles();

 

    const handleNextClick = async () => {
    const isAllFieldsFilled = Object.values(formData).every(value => value !== '');
    if (isAllFieldsFilled) {
      console.log("Données du formulaire:", formData);
      saveFormData(formData);
      try {
        handleNext(); 
      } catch (error) {
        alert("Une erreur s'est produite lors de la création de l'annonce.");
      }
    } else {
      alert("Veuillez remplir tous les champs obligatoires avant de passer à l'étape suivante.");
    }
  };

  const [modeleOptions, setModeleOptions] = useState([]);
  
  const handleChange = (event) => {
    const { name, value } = event.target;
  
    if (["annee", "age", "kilometrage", "nombrePortes"].includes(name)) {
      const numericValue = parseInt(value);
      if (!isNaN(numericValue)) {
        handleFormDataChange({ [name]: numericValue });
      } else {
        alert("Veuillez entrer un nombre valide.");
      }
    } else {
      handleFormDataChange({ [name]: value });
      console.log("Données du formulaire:", { ...formData, [name]: value });
    }
  
    if (name === 'marque') {
      const selectedMarque = value;
      const selectedModeles = marquesModeles[selectedMarque] || [];
      setModeleOptions(selectedModeles);
    }
    handleFormDataChange({ [name]: value });
    console.log("Données du formulaire:", { ...formData, [name]: value });
  };
  

  
  
  

  return (
    <div className={classes.mainContainer}>
      <Typography
        variant="h5"
        style={{marginLeft:"2.2rem",marginTop:"2.8rem",marginBottom:"1rem",borderLeft:"7px solid #cc7b3d" ,paddingLeft:"8px"}}
      >
        Parlez nous de votre véhicule
      </Typography>
      <div className={classes.formContainer}>
        <form>
            <Typography variant="subtitle2" style={{ fontSize:"1.1rem" ,marginBottom:"0.8rem" }}>Marque</Typography>
            <select required class="form-select custom-select " onChange={handleChange}  value={formData.marque} name="marque" style={{ width: "100%", margin: "1rem 0" }}>
              <option value="" disabled hidden>Marque</option>
              {marques.map((name) => (
                <option key={name} value={name}>{name}</option>
              ))}
            </select>
            <Typography variant="subtitle2" style={{ fontSize:"1.1rem" ,marginBottom:"0.8rem" }}>Modele</Typography>
            <select required class="form-select custom-select " onChange={handleChange} value={formData.modele} name="modele" style={{ width: "100%", margin: "1rem 0" }}>
              <option value="" disabled hidden>Modèle</option>
              {modeleOptions.map((name) => (
                <option key={name} value={name}>{name}</option>
              ))}
            </select>
            <Typography variant="subtitle2" style={{ fontSize:"1.1rem" ,marginBottom:"0.8rem" }}>Année</Typography>
            <select required class="form-select custom-select " onChange={handleChange}  value={formData.annee} name="annee" style={{ width: "100%", margin: "1rem 0" }}>
              <option value="" disabled hidden>Année</option>
              {annees.map((name) => (
                <option key={name} value={name}>{name}</option>
              ))}
            </select>
            <Typography variant="subtitle2"   style={{ fontSize:"1.1rem" ,marginBottom:"0.8rem" }}>Age de voiture</Typography>
            <input required type="text" class="form-control"  onChange={handleChange} value={formData.age}  name="age"/>
            <Typography variant="subtitle2" style={{ fontSize:"1.1rem" ,marginBottom:"0.8rem" }}>Energie</Typography>
            <select required class="form-select custom-select " onChange={handleChange}  value={formData.energie}  name="energie" style={{ width: "100%", margin: "1rem 0" }}>
              <option value="" disabled hidden>Energie</option>
              {energies.map((name) => (
                <option key={name} value={name}>{name}</option>
              ))}
            </select>
            <Typography variant="subtitle2" style={{ fontSize:"1.1rem" ,marginBottom:"0.8rem" }}>Boite de vitesses</Typography>
            <select required class="form-select custom-select" onChange={handleChange} value={formData.boiteDeVitesse} name="boiteDeVitesse" style={{ width: "100%", margin: "1rem 0" }}>
              <option value="" disabled hidden>Boite de vitesses</option>
              {vitesse.map((type, index) => (
                  <option key={index} value={type}>{type}</option>
              ))}
            </select>
            <Typography variant="subtitle2" style={{ fontSize:"1.1rem" ,marginBottom:"0.8rem" }}>Carosserie</Typography>
            <select required class="form-select custom-select" onChange={handleChange} value={formData.carrosserie} name="carrosserie" style={{ width: "100%", margin: "1rem 0" }}>
              <option value="" disabled hidden>Carrosserie</option>
              {carrosserieTypes.map((type, index) => (
                  <option key={index} value={type}>{type}</option>
              ))}
            </select>
            <Typography variant="subtitle2" style={{ fontSize:"1.1rem" ,marginBottom:"0.8rem" }}>Transmission</Typography>
            <select required class="form-select custom-select " onChange={handleChange}  value={formData.transmission}  name="transmission" style={{ width: "100%", margin: "1rem 0" }}>
              <option value="" disabled hidden>Transmission</option>
              {transmission.map((name) => (
                <option key={name} value={name}>{name}</option>
              ))}
            </select>
            {/* <Typography variant="subtitle2" style={{ fontSize:"1.1rem" ,marginBottom:"0.8rem" }}>Puissance fiscale</Typography>
            <select required class="form-select custom-select " onChange={handleChange}  value={formData.puissance}  name="puissance" style={{ width: "100%", margin: "1rem 0" }}>
              <option value="" disabled hidden>Puissance fiscale</option>
              {energies.map((name) => (
                <option key={name} value={name}>{name}</option>
              ))}
            </select> */}
            <Typography variant="subtitle2"   style={{ fontSize:"1.1rem" ,marginBottom:"0.8rem" }}>Puissance fiscale</Typography>
            <input required type="text" class="form-control"  onChange={handleChange} value={formData.puissance} name="puissance" />
            <Typography variant="subtitle2"   style={{ fontSize:"1.1rem" ,marginBottom:"0.8rem" }}>Kilometrage</Typography>
            <input required type="text" class="form-control"  onChange={handleChange} value={formData.kilometrage} name="kilometrage" />
            <Typography variant="subtitle2"   style={{ fontSize:"1.1rem" ,marginBottom:"0.8rem" }}>Nombre de portes</Typography>
            <input required type="text" class="form-control"  onChange={handleChange} value={formData.nombrePortes}  name="nombrePortes"/>
            <Typography variant="subtitle2"  style={{ fontSize:"1.1rem" ,marginBottom:"0.8rem" }}>Couleur exterieure</Typography>
            <input required type="text" class="form-control"  onChange={handleChange} value={formData.couleurExterieure} name="couleurExterieure" />
            <Typography variant="subtitle2"  style={{ fontSize:"1.1rem" ,marginBottom:"0.8rem" }}>Couleur interieure</Typography>
            <input required type="text" class="form-control"  onChange={handleChange} value={formData.couleurInterieure} name="couleurInterieure" />
            <Typography variant="subtitle2"  style={{ fontSize:"1.1rem" ,marginBottom:"0.8rem" }}>Sellerie</Typography>
            <input required type="text" class="form-control"  onChange={handleChange} value={formData.sellerie} name="sellerie" />
            <Typography variant="subtitle2"  style={{ fontSize:"1.1rem" ,marginBottom:"0.8rem" }}>Location</Typography>
            <input required type="text" class="form-control"  onChange={handleChange} value={formData.location} name="location" />
            <Button
              className={classes.btn}
              variant="contained"
              onClick={handleNextClick}
              >
              {activeStep === steps.length ? "Finish" : "Continuer"}
            </Button>
          
            
    
        </form>
      </div>
    </div> 
  )
}

export default Step1;
