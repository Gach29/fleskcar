import React from "react";
import './Steps.css';
import { Typography, Button } from "@material-ui/core";
import { makeStyles } from '@material-ui/core/styles';
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import { Link } from "react-router-dom";

const useStyles = makeStyles({
    mainContainer: {
        display: "grid",
        justifyContent: "start",
        marginLeft: "3.8rem",
        fontSize: "1rem",
        position: "relative",
        zIndex: 5,
    },
    formContainer: {
        position: "relative",
        width: "40rem",
        height: "auto",
        padding: "2rem"
    },
    btn: {
        height: "3rem",
        width:"13rem",
        color: "#fff",
        background: "#cc7b3d",
        marginTop: "1.5rem",
        marginBottom:"6rem",
        marginLeft: "28.8rem",
        "&:hover": {
            background: "#cc7b3d",
            opacity: ".7",
            transition: ".3s ease-in-out"
        }
    },
});

const Success = ({ formData, files, selectedOptions,prix }) => {
    const classes = useStyles();

    return (
        <div className={classes.mainContainer}>
            <Typography variant="h4"  style={{marginTop:"2.8rem",marginBottom:"0.8rem",borderLeft:"7px solid #cc7b3d" ,paddingLeft:"8px"}}>Voici votre annonce</Typography>
            
            <div>
                <Card style={{ width: '18rem', marginTop: '1rem' }}>
                        <CardContent>
                            <h5>{formData.marque} {formData.modele}</h5>
                            <ul>
                                <li>Marque: {formData.marque}</li>
                                <li>Année: {formData.annee}</li>
                                <li>Modèle: {formData.modele}</li>
                                <li>Énergie: {formData.energie}</li>
                                <li>Boîte de vitesse: {formData.boiteDeVitesse}</li>
                                <li>Carrosserie: {formData.carrosserie}</li>
                                <li>Nombre de portes: {formData.nombrePortes}</li>
                                <li>Age de voiture: {formData.age}</li>
                                <li>Kilométrage: {formData.kilometrage}</li>
                                <li>Couleur extérieure: {formData.couleurExterieure}</li>
                                <li>Prix: {prix}</li>
                            </ul>
                        </CardContent>
                </Card>
            
            </div>
            
            {/* <div>
                <Typography variant="h5">Fichiers sélectionnés :</Typography>
                {Object.keys(files).map(key => (
                    <div key={key}>
                        <Typography variant="subtitle1">{key}</Typography>
                
                        <img src={URL.createObjectURL(files[key])} alt={key} style={{ maxWidth: "60%", marginTop: "1rem", marginBottom: "2rem" }} />
                    </div>
                ))}
            </div>  */}
            
            {/* <div>
                <Typography variant="h5">Etat de voiture :</Typography>
                <ul>
                    <li>Fait un entretien: {selectedOptions.entretien}</li>
                    <li>Moteur en bon etat ou pas: {selectedOptions.moteur}</li>
                    <li>Boîte de vitesse en bon etat ou pas:: {selectedOptions.boiteDeVitesse}</li>
                    <li>Voyants tous allumés ou pas: {selectedOptions.voyants}</li>
                    <li>Subi un accident ou pas: {selectedOptions.accident}</li>
                </ul>
            </div> */}
                <Button
                    className={classes.btn}
                    variant="contained"
                    style={{ minWidth: '7rem' }}
                >
                    <Link to="/">Retour à l'accueil</Link>
                </Button>
        </div>
    );
};

export default Success;
