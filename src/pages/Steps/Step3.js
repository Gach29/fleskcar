import React, { useEffect, useState } from "react";
import './Steps.css';
import { Typography, Button } from "@material-ui/core";
import ClipLoader from "react-spinners/ClipLoader";
import { makeStyles } from '@material-ui/core/styles';
// import axios from 'axios'

const useStyles = makeStyles({
  mainContainer: {
    display: "grid",
    justifyContent: "start",
    marginLeft:"3.8rem",
    fontSize:"1rem",
    position: "relative",
    zIndex: 5,
  },
  formContainer: {
    position: "relative",
    width: "40rem",
    height: "auto",
    padding: "2rem"
  },
  btn: {
    height: "2.6rem",
    color: "#fff",
    background: "#cc7b3d",
    marginLeft: "22rem",
    marginTop: "1rem",
    "&:hover": {
      background: "#ac5a0c",
      opacity: ".7",
      transition: ".3s ease-in-out"
    }
  },
  btndisable:{
    background:"rgba(0,0,0,0.38)",
    height: "3rem",
    color: "#fff",
    marginTop: "2rem",
  },
  spinnerContainer: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginTop:"30px",
    marginBottom:"13rem"
  }
});

const Step3 = ({ activeStep, steps, handleNext, formData, saveFormData,handleFormDataChange ,files,setFiles ,prix,setPrix, createPost}) => {
  const classes = useStyles();
  const [loading, setLoading] = useState(true);
  prix=88000;
  const [localPrix, setLocalPrix] = useState(prix);

  useEffect(() => {
    const timeout = setTimeout(() => {
      setLoading(false);
    }, 5000);
    return () => clearTimeout(timeout);
  }, []);

  const handleChange = (event) => {
    const { name, value } = event.target;
    if (name === "prix") {
      const numericValue = parseInt(value);
      if (!isNaN(numericValue)) {
        handleFormDataChange({ ...formData, [name]: numericValue });
        setLocalPrix(numericValue);
      } else {
        alert("Veuillez entrer un nombre valide pour le prix.");
      }
    }
  };

  const handleNextClick = async () => {
    let updatedPrix;
    if (localPrix === '') {
      updatedPrix = prix; 
    } else {
      updatedPrix = localPrix;
    }

    saveFormData({ ...formData,prix: updatedPrix });
    setPrix(updatedPrix)
    await createPost();
    handleNext();
  };

  return (
    <div>
      {loading ? (
        <div className={classes.spinnerContainer}>
          <>
            <ClipLoader
              color="#cc7b3d"
              loading={loading}
              size={30}
              aria-label="Loading Spinner"
              data-testid="loader"
            />
            <Typography variant="body1" style={{ marginLeft: "1rem" }}>Veuillez patienter...</Typography>
          </>
        </div>
      ) : (
        <div className={classes.mainContainer}>
          <Typography
            variant="h5"
            style={{ marginLeft: "2.2rem", marginTop: "2.8rem", marginBottom: "1.5rem", borderLeft: "7px solid #cc7b3d", paddingLeft: "8px" }}
          >
            Vous arrivez à la fin du parcours !
          </Typography>
          <div className={classes.formContainer}>
            <form>
              <div style={{ display: "flex", flexDirection: "column" }}>
                <div style={{ display: "flex", alignItems: "center" }}>
                  <Typography variant="h6" style={{ marginBottom: "0.8rem", fontSize: "1.3rem", marginRight: '0.8rem' }}>Voici votre estimation de prix : </Typography>
                  <Typography variant="body" name='prix' style={{ fontSize: "1.1rem", marginBottom: "0.7rem", fontWeight: 'bold' }}>
                    {prix} DT
                  </Typography>
                  
                </div>
                <Typography variant="h6" style={{ fontSize: "1.1rem", marginBottom: "0.8rem" }}>Le prix proposé ne vous satisfait pas ? Vous pouvez écrire votre propre prix ici</Typography>
                <input required type="text" className="form-control" onChange={handleChange} value={localPrix} name="prix" />
                <Button
                  className={classes.btn}
                  variant="contained"
                  onClick={handleNextClick}
                  style={{ minWidth: '10rem' }}
                >
                  {activeStep === steps.length ? "Finish" : "Créer l'annonce"}
                </Button>
              </div>
            </form>
          </div>
        </div>
      )}
    </div>
  )
};

export default Step3;

