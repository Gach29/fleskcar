import React, { useState } from 'react';
import './Payment.css'
import { PaymentElement, useElements , useStripe} from '@stripe/react-stripe-js';
import axios from 'axios'

const PaymentForm = () => {
    const [success, setSuccess] = useState(false);
    const [isProcessing, setIsProcessing] = useState(false);
    const [message, setMessage] = useState(null);
    const stripe = useStripe();
    const elements = useElements();

    const handleSubmit = async (e) => {
        e.preventDefault();

        if (!stripe || !elements) {
            return;
        }

        setIsProcessing(true);

        try {
            const token = localStorage.getItem('token');
            const response = await axios.post('http://localhost:8000/payment/create', {amount: 1000 },{
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            });

            const { payment} = response.data;

            const {error, paymentIntent} = await stripe.confirmPayment({
                elements,
                confirmParams : {
                    return_url : `${window.location.origin}/completion`
                },
                redirect: "if_required"
            });
            // const { error } = await stripe.confirmCardPayment(clientSecret, {
            //     payment_method: {
            //         card: elements.getElement('card')
            //     }
            // });


            if (error) {
                setIsProcessing(false);
                setMessage(error.message);
            } 
            else if (paymentIntent && paymentIntent.status === "succeeded") {
                // setMessage("Merci pour votre paiement. Votre reçu de paiement sera généré via votre email 🎉");
                setSuccess(true); 
                setIsProcessing(false);
                console.log('Payment successful:', payment);
            } 
            else {
                setMessage('Unexpected state');
            }
        } catch (error) {
            setIsProcessing(false);
            setMessage(error.message);
        }
    };

    return (
        <>
            {success ? (
                <div className="text-center txt-merci mb-4 mt-5 flex justify-content-center">
                    <h3>Merci pour votre paiement 🎉🎉</h3>
                    <p>Votre reçu de paiement sera généré via votre email.</p>
                </div>
            ) : (
                <>
                <div className="text-center mb-4">
                    <h3>Payer en ligne</h3>
                </div>
            
                <form id='payment-form' onSubmit={handleSubmit}>
                    <PaymentElement />
                    <button disabled={isProcessing} id="submit">
                        <span id="button-text">
                            {isProcessing ? "Traitement ..." : "Payer"}
                        </span>
                    </button>

                    {message && <div id="payment-message">{message}</div>}

                </form>
                </>
            )}
        </>
    );
}

export default PaymentForm;
