/* eslint-disable no-unused-vars */
import axios from 'axios';
import './Payment.css'
import React, { useEffect, useState } from 'react'

const Payment = ({ closeModal }) => {


    const [post, setPost] = useState([]);
    
    useEffect(() => {
        fetchPost();

    }, []);

    const fetchPost = async () => {
        try {
            const postId = localStorage.getItem('postId');
            const response = await axios.get(`http://localhost:8000/posts/post/${postId}`);
            const postData = response.data;
            const postUserId = postData.user; 
            localStorage.setItem('postUserId', postUserId);
            console.log(postData);
            setPost(postData);
        } catch (error) {
            console.error('Error fetching post:', error);
        }
    };


    const handlePayment = async () => {
        const userId = localStorage.getItem('userId');
        const token = localStorage.getItem('token'); 
    
        try {
            const amount = parseFloat(post.prix) * 20 / 100;
            const response = await axios.post(
                'http://localhost:8000/payment/add',
                {
                    amount: amount.toString(),
                    postId: post._id, 
                },
                {
                    headers: {
                        'Authorization': `Bearer ${token}`,
                    },
                }
            );
            console.log(response.data)
            window.location.href = response.data.result.link;
        } catch (error) {
            console.error('Failed to initiate payment:', error);
        }
    };
    


return (
    <div>
    <section id="payment" class="model-login-overlay">
        <div class="model-login-container">
            <button class="btn-close" onClick={closeModal}>
                &times;
            </button>
            <section className="p-4 p-md-5 mt-4">
                <div className="row d-flex justify-content-center">
                    <div className="col-md-8 col-lg-8 col-xl-12 me-2 ms-2">
                        <div class="row m-0">
                            <div className="p-text text-center mb-4" >
                                <h3>Réserver votre voiture</h3>
                            </div>
                            <div class="col-lg-5 p-0 ps-lg-8 align-self-center mb-5">
                                {post.files && post.files.length > 0 && ( 
                                    <img src={`http://localhost:8000/posts/carPhotos/${post.files[0]}`} alt="Product" class="img-fluid" style={{maxWidth: '105%', height: 'auto'}} />
                                )}
                                
                            </div>
                            <div class="col-lg-7 p-0 ps-lg-4">
                                <div class="row m-0">
                                    <div class="col-12 px-4">
                                                <div class="d-flex align-items-end  mb-3">
                                                    <p class="h4 m-0" style={{color:'#cc7b3d'}}><span class="pe-1">{post.marque}</span><span class="pe-1">{post.modele}</span></p>
                                                </div>
                                                <div class="d-flex justify-content-between mb-1">
                                                    <p class="textmuted">Montant total</p>
                                                    <p class="fs-14 fw-bold"><span class=" pe-1"></span>{post.prix} DT</p>
                                                </div>
                                                <div class="d-flex justify-content-between mb-1">
                                                    <p class="textmuted">Livraison</p>
                                                    <p class="fs-14 fw-bold">Gratuite</p>
                                                </div>
                                                <div class="d-flex justify-content-between mb-1">
                                                    <p class="textmuted">Premier Loyer (20%)</p>
                                                    <p class="fs-14 fw-bold">-<span class=" px-1"></span>{parseFloat(post.prix) * 20 / 100} DT</p>
                                                </div>
                                                <form onSubmit={(e) => e.preventDefault()}>
                                                    <div class="d-flex justify-content-between mb-1">
                                                        <p class="textmuted fw-bold">Montant à payer</p>
                                                        <div class="d-flex align-text-top ">
                                                            <span class=" mt-1 pe-1 fs-14 "></span><span class="h4">{parseFloat(post.prix) * 20 / 100} DT</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 px-0">
                                                        <div class="row m-0">
                                                            <div class="mb-4 p-0">
                                                                <button class="btn btn-primary" onClick={handlePayment}>Payer<span class="fas fa-arrow-right ps-2"></span>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </section>
        </div>
)
}

export default Payment
