import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { useSearchParams } from 'react-router-dom';

const Success = () => {
    const [searchParams] = useSearchParams();
    const [result, setResult] = useState("");

    useEffect (() => {
        const verifyPayment = async () => {
            try {
                const response = await axios.post(`http://localhost:8000/payment/verify/${searchParams.get("payment_id")}`);
                if (response.data && response.data.result && response.data.result.status) {
                    console.log(response.data); 
                    setResult(response.data.result.status);
                } else {
                    throw new Error("Invalid response format");
                }
            } catch(error) {
                console.error('Failed to verify payment:', error);
            }
        };

        verifyPayment();
    }, [searchParams]);

    return (
        <>
            {result === "SUCCESS" && (
                <div className="p-4">
                    <div className="alert alert-success"> Succès 🎉🎉 Merci pour votre paiement !!! </div>
                </div>
            )}
        </>
    );
}

export default Success;
