
// import React from 'react';

// export default React.createContext({
//     isAuthenticated:false,
//     setIsAuthenticated: value => {}
// });

import React, { createContext, useContext, useEffect, useState } from "react";

export const AuthContext = createContext(); 

export function useAuth() {
    return useContext(AuthContext);
}

export function AuthProvider(props) {
    const [authUser, setAuthUser] = useState(null);
    const [isLoggedIn, setIsLoggedIn] = useState(() => {
        return localStorage.getItem('isLoggedIn') === 'true';
    });

    useEffect(() => {
        localStorage.setItem('isLoggedIn', isLoggedIn);
    }, [isLoggedIn]);

    const value = {
        authUser,
        setAuthUser,
        isLoggedIn,
        setIsLoggedIn
    };

    return (
        <AuthContext.Provider value={value}>{props.children}</AuthContext.Provider>
    );
}
