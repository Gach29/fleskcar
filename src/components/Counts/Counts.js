import React, { useEffect, useState } from 'react';
import CountUp from 'react-countup';
import './Counts.css';

const Counts = () => {
   const [counted, setCounted] = useState(false);

    useEffect(() => {
        setCounted(true);
    }, []);

    return (
    <div className="counter-up"  data-aos="fade-up">
        <div className="content">
                <div className="box">
                <CountUp start={counted ? 0 : null} end={16} duration={3} redraw={true}>
                    {({ countUpRef }) => (
                        <div className="counter" ref={countUpRef} />
                    )}
                </CountUp>
                <div className="text">Agences</div>
                </div>
                <div className="box">
                <CountUp start={counted ? 0 : null} end={100} duration={3} redraw={true}>
                    {({ countUpRef }) => (
                        <div className="counter" ref={countUpRef} />
                    )}
                </CountUp>
                <div className="text">Experts</div>
            </div>
            <div className="box">
                <CountUp start={counted ? 0 : null} end={2000}  duration={3} redraw={true}>
                    {({ countUpRef }) => (
                        <div className="counter" ref={countUpRef} />
                    )}
                </CountUp>
                <div className="text">Voitures vendus</div>
            </div>
            <div className="box">
                <CountUp start={counted ? 0 : null} end={95}  suffix="%" duration={3} redraw={true}>
                    {({ countUpRef }) => (
                        <div className="counter" ref={countUpRef} />
                    )}
                </CountUp>
                <div className="text">Clients satisfaits</div>
            </div>
            </div>
        </div>
        );
    }

export default Counts;
