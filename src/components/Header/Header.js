/* eslint-disable no-unused-vars */

import React, {useEffect, useState } from 'react';
import './Header.css';
//import { Link } from 'react-scroll';
import ModelLogin from '../ModelLogin/ModelLogin';
import user from '../../images/userwhite.png';
import { Link, NavLink } from 'react-router-dom';
import { useAuth } from '../../context/AuthContext';
import axios from 'axios';

const Header = ({ activeLink, setActiveLink }) => {

    const [clicked, setClicked] = useState(false);
    const [openModal, setOpenModal] = useState(false);
    // eslint-disable-next-line no-unused-vars
    const handleClick = (linkId) => {
        setClicked(!clicked);
        setActiveLink(linkId); 
    };

    const{
        isLoggedIn,setIsLoggedIn
    }= useAuth()


    const handleLoginClick = () => {
        setOpenModal(true);
        window.history.pushState({}, '', '/connexion');
    };


    const handleCloseModal = () => {
        setOpenModal(false);
        window.history.pushState({}, '', '/');
    };

    const [formData, setFormData] = useState(null);
    const [open, setOpen]=useState(false)

    useEffect(() => {
        const fetchData = async () => {
            try {
                const userId = localStorage.getItem('userId');
                if (userId) {
                    const response = await axios.get(`http://localhost:8000/auth/users/${userId}`); 
                    setFormData(response.data);
                } 
            } catch (error) {
                console.log(error.message);
            }
        };
    
        fetchData();
    }, []);


    const  handleLogout = () =>{
        console.log("on est déconnecté");
        setIsLoggedIn(false);
        localStorage.removeItem('userId'); 
        localStorage.removeItem('token');
        localStorage.removeItem('likedPostId');
        // localStorage.clear();
        
    }

    const handleLogoutClick = () => {
        handleLogout(); 
        localStorage.setItem('isFavorite', 'false');
        localStorage.setItem('isLiked', 'false');
    };


    return (
        <header id="header" className="header fixed-top d-flex align-items-center">
            <div className="container d-flex align-items-center justify-content-between">
                <a href="/" className="logo d-flex me-auto me-lg-0"><img src="../../logo4.png" alt="" className="img-fluid" /></a>
                <nav className="navbar justify-content-end ms-auto">
                    <div>
                        <ul id="navbar" className={clicked ? "navbar active" : "navbar"}>
                            <li>
                                <NavLink to="/"
                                >Acceuil</NavLink>
                            </li>
                            <li>
                                <NavLink to="/about" spy="true" smooth="true" 
                                >A propos</NavLink>
                            </li>
                            <li>
                                <NavLink to="/voitures" spy="true" smooth="true" 
                                >Voitures</NavLink>
                            </li>
                            <li>
                                <NavLink to="/devenirexpert"
                                >Devenir Expert</NavLink>
                            </li>
                            <li>
                                <NavLink to="/vendrevoiture" spy="true" smooth="true" 
                                >Vendre votre voiture</NavLink>
                            </li>
                            {  (!isLoggedIn && (
                                <>
                                    <li>
                                        <button className='btn-login' onClick={handleLoginClick}>Se connecter</button>
                                    </li>
                                    {openModal && <ModelLogin closeModal={handleCloseModal}/>}
                                </>
                            )) ||  (
                                <>
                                    <li className='profile'>
                                        <img  src={formData && formData.profileImage ? `http://localhost:8000/auth/pictures/${formData.profileImage}` : user}
                                        alt='user' 
                                        onClick={() => setOpen(!open)} className='profile-icon-small ' /> 
                                    {open &&
                                        <div className='drop '>
                                            <ul>
                                                <Link to='/profil'><label><li  className='text'>Profil</li></label></Link>
                                                <button onClick={handleLogoutClick} style={{border:'none', background:"#fff"}}><li className='text'>Se déconnecter</li></button>
                                            </ul>
                                        </div>
                                    }
                                    </li>
                                </>
                            )}
                        </ul>
                    </div>
                    <div id='mobile' onClick={() => setClicked(!clicked)}>
                        <i id="bar" className={clicked ? 'fas fa-times' : 'fas fa-bars'}></i>
                    </div>
                </nav>
            </div>
        </header>
    );
}

export default Header;
