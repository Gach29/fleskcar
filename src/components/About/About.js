import React from 'react'
import './About.css'

const About = () => {
    return (
      <section id="about" class="about">
      <div class="container" data-aos="fade-up">

        <div class="section-header">
          <h2>A propos de <span>nous</span></h2>
        </div>

        <div class="row gy-4">
          <img class="col-lg-6 position-relative about-img" src='./about.jpg' alt=""/>
          <div class="col-lg-5 d-flex align-items-end" data-aos="fade-up" data-aos-delay="300">
            <div class="content ps-2 ps-lg-5">
              <h4>FleskCar: votre agence automobile</h4>
              <p class="fst-italic" >
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.
                </p>
              <ul>
                <li><i class="bi bi-check2-all"></i> Ullamco laboris nisi ut aliquip ex ea commodo consequat.</li>
                <li><i class="bi bi-check2-all"></i> Duis aute irure dolor in reprehenderit in voluptate velit.</li>
                <li><i class="bi bi-check2-all"></i> Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate.</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </section>

)
}

export default About