import React from 'react'
import './Hero.css';

const Hero = () => {
    return (
    <div id="hero" className='header-wraper'>
        <div className="hero-container" data-aos="fade-up" data-aos-delay="150">
            <h2>Avec FleskCar ...</h2>
            <p>Achetez, Vendez, Profitez !</p>
            <a href="#about" className="btn scrollto">Voir Nos Voitures</a>
        </div>
    </div>
)
}

export default Hero