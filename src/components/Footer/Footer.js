import React from 'react';
import './Footer.css';
const Footer = () => {
    return (
        <footer id="footer" className='footer'>
            <div className="footer-top">
                <div className="container">
                    <div className="row">
                        <div className=" desc col-lg-4 col-md-6 ms-6 ">
                            <div className="footer-info">
                            <img src="../../logo4.png" alt='logo' className='foot-logo img-fluid'/>
                                <p>Achat et vente des véhicules sur FleskCar l'un des meilleurs endroits pour acheter une voiture d'occasion en Tunisie.</p>
                                
                                <div className="social-links mt-3">
                                    <a href className="twitter"><i className="bi bi-twitter"></i></a>
                                    <a href className="facebook"><i className="bi bi-facebook"></i></a>
                                    <a href className="instagram"><i className="bi bi-instagram"></i></a>
                                </div>
                            </div>
                        </div>

                        <div className="col-lg-2 col-md-6 ms-auto ms-6 me-6  footer-links">
                            <h4>Liens</h4>
                            <ul>
                                <li><i className='bi bi-chevron-right'></i> <a href>A propos</a></li>
                                <li><i className='bi bi-chevron-right'></i> <a href>Anonces</a></li>
                                <li><i className='bi bi-chevron-right'></i> <a href>Vendre votre voiture</a></li>
                                <li><i className='bi bi-chevron-right'></i> <a href>Devenir Expert</a></li>
                            </ul>
                        </div>

                        <div className="col-lg-4 col-md-6 ms-auto me-6 ms-6 footer-newsletter">
                            <h4>Contact</h4>
                            <p>Besoin d'informations ! Contactez-nous</p>
                                <ul>
                                    <li><i class="bi bi-geo-alt-fill"></i><strong>Adresse: </strong> Monastir, Omran</li>
                                    <li><i class="bi bi-telephone-fill"></i><strong>Telephone: </strong> 216 5533322</li>
                                    <li><i class="bi bi-envelope-fill"></i><strong>Email: </strong> flesk@gmail.com</li>
                                </ul>
                
                                <form  method="post">
                                    <input type="email" name="email" />
                                    <input type="submit" value="Envoyer" />
                                </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="copyright">
                    &copy; Copyright <strong><span>FleskCar</span></strong>   .Tous Droits Réservés
                </div>
                <div class="credits">
                    
                </div>
            </div>
        </footer>
    )
}

export default Footer;
