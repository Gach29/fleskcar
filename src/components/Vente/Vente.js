import React, { useState } from 'react'
import './Vente.css'
import { Link } from 'react-router-dom'
import ModelLogin from '../ModelLogin/ModelLogin';

const About = () => {
  const [openModal, setOpenModal] = useState(false);
    return (
      <section id="vente" class="about">
      <div class="container" data-aos="fade-up">

        <div class="section-header">
          <h2>Vendre votre <span>Voiture</span></h2>
        </div>

        <div class="row gy-4">
          <img class="col-lg-6 position-relative img-fluid sell-img" src='./sell4.jpg' alt=""/>
          <div class="col-lg-5 d-flex align-items-end" data-aos="fade-up" data-aos-delay="300">
            <div class="content ps-2 ps-lg-5">
              <p>De l'estimation à la vente de votre voiture, nous réalisons toutes les démarches pour vous.</p>
              <p>
                <strong>C'est simple, rapide, et gratuit !</strong>
              </p>
              <div className='timeline'>
                  <div className='etape-box'>
                      <i class=" icon bi bi-people"></i>
                      <span className='title'>Etape 1</span>
                      <p>Demande d'estimation en ligne</p>
                  </div>
                  <div className='etape-box'>
                      <i class="bi bi-calendar2-week"></i>
                      <span className='title'>Etape 2</span>
                      <p>Validation de la demande en ligne.
                      Prise de rendez-vous pour une inspection à domicile</p>
                  </div>
                  <div className='etape-box'>
                      <i class="bi bi-check-square"></i>
                      <span className='title'>Etape 3</span>
                      <p>Confirmation de la valeur. Prise en charge de votre voiture</p>
                  </div>
                  <div className='etape-box'>
                      <i class="bi bi-cash-coin"></i>
                      <span className='title'>Etape 4</span>
                      <p>Passage en vente. Paiement du prix d'adjudication</p>
                  </div>
              </div>
              <button className='button mt-3'>
                <Link to='/vendrevoiture'>Vendre Maintenant</Link>
              </button> 
              {openModal && <ModelLogin closeModal={setOpenModal} />}
              </div>
            </div>
          </div>
        </div>
    </section>

)
}

export default About