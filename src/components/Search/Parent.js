import React, { useState } from 'react';
import Search from './Search';
import ListCars from '../ListCars/ListCars';

const ParentComponent = () => {
    const [filteredData, setFilteredData] = useState([]);

    const handleDataFiltered = (filteredData) => {
        setFilteredData(filteredData);
    };

    return (
        <div>
            <Search onDataFiltered={handleDataFiltered} />
            <ListCars filteredData={filteredData} />
        </div>
    );
};

export default ParentComponent;
