/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from 'react';
import { FaSearch } from "react-icons/fa";
import { Form, Row, Col, Button } from 'react-bootstrap';
import './Search.css';
import Box from '@mui/material/Box';
import Slider from '@mui/material/Slider';
import axios from 'axios';

function valueLabelFormat(value) {
    return `${value} DT`;
}

const Search = ({ onDataFiltered })=> {
    const [posts, setPosts] = useState([]);
    const [selectedMake, setSelectedMake] = useState('');
    const [selectedModel, setSelectedModel] = useState('');
    const [selectedDate, setSelectedDate] = useState('');
    const [prixvalue, setPrixValue] = useState([0, 250000]);
    const [showAdvanced, setShowAdvanced] = useState(false);
    const [selectedLocation, setSelectedLocation] = useState('');
    const [selectedMaxKilometers, setSelectedMaxKilometers] = useState('');
    const [selectedYear, setSelectedYear] = useState('');
    const [selectedCarrosserie, setSelectedCarrosserie] = useState('');
    const [noResults, setNoResults] = useState(false);

    useEffect(() => {
        fetchPosts();
    }, []);

    const fetchPosts = async () => {
        try {
            const response = await axios.get('http://localhost:8000/posts');
            const data = await response.data;
            setPosts(data);
        } catch (error) {
            console.error('Error fetching posts:', error);
        }
    };

    const carMakes = [...new Set(posts.map(car => car.marque))];
    const boite = [...new Set(posts.map(car => car.boiteDeVitesse))];
    const uniqueKilometrages = [...new Set(posts.map(car => car.kilometrage))];
    const uniqueYears = [...new Set(posts.map(car => car.annee))];
    const uniqueLocations = [...new Set(posts.map(car => car.location))];
    const carPrices = [...new Set(posts.map(car => car.prix))];

    const priceValues = [0, 250000];

    const carModels = {};
    posts.forEach(car => {
        if (!carModels[car.marque]) {
            carModels[car.marque] = [];
        }
        carModels[car.marque].push(car.modele);
    });

    const handleMakeChange = (event) => {
        setSelectedMake(event.target.value);
        setSelectedModel(''); 
    };

    const handleModelChange = (event) => {
        setSelectedModel(event.target.value);
    };

    const handleChange = (event, newValue) => {
        setPrixValue(newValue);
    };

    // const handleDateChange = (event) => {
    //     setSelectedDate(event.target.value);
    // };

    const handleLocationChange = (event) => {
        setSelectedLocation(event.target.value);
    };

    const handleYearChange = (event) => {
        setSelectedYear(event.target.value);
    };

    const handleKilometreChange = (event) => {
        setSelectedMaxKilometers(event.target.value);
    };

    const handleCarosserieChange = (event) => {
        setSelectedCarrosserie(event.target.value);
    };

    const handleSubmit = (event) => {
        event.preventDefault();
        const filteredData = posts.filter((car) => {
            return (
                (selectedMake === '' || car.marque=== selectedMake) &&
                (selectedModel === '' || car.modele === selectedModel) &&
                (car.prix >= prixvalue[0] && car.prix <= prixvalue[1]) &&
                (car.location === selectedLocation || selectedLocation === '') &&
                (car.kilometrage <= selectedMaxKilometers || selectedMaxKilometers === '') &&
                (car.annee === selectedYear || selectedYear === '') &&
                (car.carrosserie === selectedCarrosserie || selectedCarrosserie === '')
            );
        });
        if (filteredData.length === 0) {
            setNoResults(true);
        } else {
            setNoResults(false);
        }
        onDataFiltered(filteredData); 
    }

    return (
        <div className='search-container'>
            <Form onSubmit={handleSubmit}>
                <Row className='row justify-content-between  gap-3 position-relative lg-top-minus-4 shadow-lg bg-white bg-blur'>
                    <Col lg='auto' className='col1'>
                        <Form.Select  class="form-select custom-select " value={selectedMake} onChange={handleMakeChange}>
                            <option value="" disabled hidden>Marque</option>
                            {carMakes.map((make) => (
                                <option key={make} value={make}>{make}</option>
                            ))}
                        </Form.Select>
                    </Col>
                    <Col lg='auto' className='col1'>
                        <Form.Select  class="form-select custom-select" value={selectedModel} disabled={!selectedMake} onChange={handleModelChange}>
                            <option value="" disabled hidden >Modèle</option>
                            {selectedMake && carModels[selectedMake]?.map((model) => (
                                <option key={model} value={model}>{model}</option>
                            ))}
                        </Form.Select>
                    </Col>
                    <Col lg='auto' className='col'>
                        <Box sx={{ width: 250 }}>
                            <span id='price-slider'>
                                Prix: {valueLabelFormat(prixvalue[0])} - {valueLabelFormat(prixvalue[1])}
                            </span>
                            <Slider
                                style={{ color: '#cd7632e8' }}
                                value={prixvalue}
                                min={priceValues[0]}
                                step={1000}
                                max={priceValues[1]}
                                onChange={handleChange}
                                valueLabelDisplay="auto"
                                aria-labelledby="price-slider"
                            />
                        </Box>
                    </Col>
                    <br />

                    {showAdvanced && (
                        <>
                            <Col lg='auto' className='col1 me-auto'>
                            <Form.Select className="form-select" style={{width:190}}   value={selectedCarrosserie} onChange={handleCarosserieChange}>
                                <option value="" disabled hidden>Boite de vitesses</option>
                                {boite.map((carrosserie, index) => (
                                    <option key={index} value={carrosserie}>{carrosserie}</option>
                                ))}
                            </Form.Select>
                            </Col> 
                            <Col lg='auto' className='col'>
                                <Form.Select  className="form-select" value={selectedMaxKilometers} onChange={handleKilometreChange}>
                                    <option value="" disabled hidden>Kilométrage</option>
                                    {uniqueKilometrages.map((kilometrage, index) => (
                                        <option key={index} value={kilometrage}>{kilometrage}</option>
                                    ))}
                                </Form.Select>
                            </Col>
                            <Col lg='auto' className='col'>
                                <Form.Select  className="form-select" value={selectedYear} onChange={handleYearChange}>
                                    <option value="" disabled hidden>Année</option>
                                    {uniqueYears.map((year, index) => (
                                        <option key={index} value={year}>{year}</option>
                                    ))}
                                </Form.Select>
                            </Col>
                            <Col lg='auto' className='col'>
                                <Form.Select  className="form-select" value={selectedLocation} onChange={handleLocationChange}>
                                    <option value="" disabled hidden>Localisation</option>
                                    {uniqueLocations.map((location, index) => (
                                        <option key={index} value={location}>{location}</option>
                                    ))}
                                </Form.Select>
                            </Col>
                            {/* <Col lg='auto' className='col'>
                                <Form.Select  class="form-select custom-select " value={selectedDate} onChange={handleDateChange}>
                                    <option value="" disabled hidden>Date</option>
                                    {posts.map((car) => (
                                        <option key={car.date} value={car.date}>{car.date}</option>
                                    ))}
                                </Form.Select>
                            </Col> */}
                        </>
                    )}

                    <Col className='justify-content-between gap-3 ms-auto col1' lg='auto'>
                        {!showAdvanced && (
                            <Button className='search-button1' onClick={() => setShowAdvanced(true)}>
                                <FaSearch className='me-2' />Recherche avancée
                            </Button>
                        )}
                        <Button className='search-button' type='submit'>
                            <FaSearch className='me-2' />Rechercher
                        </Button>
                    </Col>
                </Row>
                {noResults && (
                    <Row className='justify-content-center mt-5'>
                        <Col>
                            <h5 style={{fontWeight:"bold",display:"flex",justifyContent:"center",alignItems:"center"}}>Aucune voiture trouvée avec ces critères de recherche.</h5>
                        </Col>
                    </Row>
                )}
            </Form>
        </div>
    );
};

export default Search;
