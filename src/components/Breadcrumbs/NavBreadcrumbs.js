import React from 'react';
import { Link} from 'react-router-dom';
import './Breadcrumbs.css';

const NavBreadcrumbs = ({ selectedCar }) => {
    // const [selectedCar, setSelectedCar] = useState(null);
    // const location = useLocation();

    // useEffect(() => {
    //     const pathname = location.pathname;
    //     const carName = decodeURIComponent(pathname.substring(pathname.lastIndexOf('/')+1));
    //     setSelectedCar(carName);
    // }, [location]);
    

    return (
        <section id="breadcrumbs" className="breadcrumbs">
            <div className="container">
                <ol>
                    <li><Link to='/'>Accueil</Link></li>
                    <li><Link to='/voitures' href style={{textDecoration:"underline",color:'#d38041',fontWeight:'bold'}}>Voitures</Link></li>
                </ol>
                <h3>{selectedCar}</h3>
            </div>
        </section>
    );
};

export default NavBreadcrumbs;
