import React, { useState } from 'react'
import {Accordion,AccordionItem,AccordionItemHeading,AccordionItemButton,AccordionItemPanel, AccordionItemState} from 'react-accessible-accordion';
import "react-accessible-accordion/dist/fancy-example.css";
import './Expert.css';
import { Link } from 'react-router-dom';


const Expert = () => {
    const [className, setClassName]= useState(null);
    return (
        <section id="expert" class="expert">
            <div class="container" data-aos="fade-up">
                <div class="content ">
                        <div class="section-header">
                            <h2>Devenir <span>Expert </span></h2>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 order-2 order-lg-1 ms-auto mt-3 mt-lg-1 pl-5">
                                <p class="fst-italic">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
                                magna aliqua.
                                </p>
                                    <Accordion
                                        className='accordion'
                                        allowMultipleExpanded={false}
                                        preExpanded={[0]}
                                    >
                                        <AccordionItem className={`accordionItem ${className}`}>
                                            <AccordionItemHeading>
                                                <AccordionItemButton className=' flexCenter accordionButton'>
                                                <AccordionItemState>
                                                    {({expanded}) => expanded ? setClassName("expanded") : setClassName ("collapsed")}
                                                </AccordionItemState>
                                                    <div className='flexCenter'>
                                                        <i class="bi bi-1-circle  icon"></i>
                                                        <span className='primaryText'>Maîtrise des technologies automobiles</span>
                                                    </div>
                                                    <div className='flexCenter icon'>
                                                        <i class="bi bi-caret-down-fill "></i>
                                                    </div>
                                                </AccordionItemButton>
                                            </AccordionItemHeading>
                                            <AccordionItemPanel>
                                                <p className='secondaryText'></p>
                                            </AccordionItemPanel>
                                        </AccordionItem>
                                        <AccordionItem className={`accordionItem ${className}`}>
                                            <AccordionItemHeading>
                                                <AccordionItemButton className=' flexCenter accordionButton'>
                                                <AccordionItemState>
                                                    {({expanded}) => expanded ? setClassName("expanded") : setClassName ("collapsed")}
                                                </AccordionItemState>
                                                    <div className='flexCenter '>
                                                        <i class="bi bi-2-circle icon"></i>
                                                        <span className='primaryText'>Compétences en diagnostic et réparation</span>
                                                    </div>
                                                    
                                                    <div className='flexCenter icon'>
                                                        <i class="bi bi-caret-down-fill "></i>
                                                    </div>
                                                </AccordionItemButton>
                                            </AccordionItemHeading>
                                            <AccordionItemPanel>
                                                <p className='secondaryText'></p>
                                            </AccordionItemPanel>
                                        </AccordionItem>
                                        <AccordionItem className={`accordionItem ${className}`}>
                                            <AccordionItemHeading>
                                                <AccordionItemButton className=' flexCenter accordionButton'>
                                                <AccordionItemState>
                                                    {({expanded}) => expanded ? setClassName("expanded") : setClassName ("collapsed")}
                                                </AccordionItemState>
                                                    <div className='flexCenter'>
                                                        <i class="bi bi-3-circle  icon"></i>
                                                        <span className='primaryText'>Capacité à conseiller les clients</span>
                                                    </div>
                                                    <div className='flexCenter icon'>
                                                        <i class="bi bi-caret-down-fill "></i>
                                                    </div>
                                                </AccordionItemButton>
                                            </AccordionItemHeading>
                                            <AccordionItemPanel>
                                                <p className='secondaryText'></p>
                                            </AccordionItemPanel>
                                        </AccordionItem>
                                    </Accordion>
                                    <button className='button mt-3'>
                                        <Link to='/devenirexpert'>Devenir Expert</Link>
                                    </button> 
                            
                            </div>
                            <div class="col-lg-5 order-1 ms-auto order-lg-2 mb-3">
                                <img src="./expert.jpg" alt="" class="img-fluid rounded float-end" />
                            </div>
                        </div>
                </div>
            </div>
        </section>
    )
}
export default Expert