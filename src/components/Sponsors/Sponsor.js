import React from 'react'
import Slider from "react-slick";
import './Sponsor.css';
import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'
import bmw from "../../images/bmw.png";
import cit from "../../images/cit.png";
import audi from "../../images/audi.png";
import jag from "../../images/jag.png";
import ren from "../../images/ren.png";
import mini from "../../images/mini.png";
import toy from "../../images/toy.png";
import fer from "../../images/fer.png";
import nis from "../../images/nisan.png";
import tes from "../../images/tes.png";
import ford from "../../images/ford.png";
import merc from "../../images/merc.png";


const Sponsor = () => {
  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 3,
    initialSlide: 0,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          initialSlide: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  };
    return (
    <section id="clients" class="clients">
        <div className='container'>
          <Slider {...settings}>
            <div>
              <img src={bmw} alt=''className=' img-fluid sp  w-100'/>
            </div>
            <div >
              <img src={audi} alt='' className='img-fluid sp w-100'/>
            </div>
            <div>
              <img src={merc} alt='' className='img-fluid sp w-100'/>
            </div>
            <div>
              <img src={fer} alt='' className='img-fluid sp w-100'/>
            </div>
            <div>
              <img src={toy} alt='' className='img-fluid sp w-100'/>
            </div>
            <div>
              <img src={nis} alt='' className=' img-fluid sp w-100'/>
            </div>
            <div >
              <img src={ren} alt='' className=' img-fluid sp w-100'/>
            </div>
            <div>
              <img src={jag} alt=''className=' img-fluid sp w-100'/>
            </div>
            <div >
              <img src={mini} alt='' className=' img-fluid sp w-100'/>
            </div>
            <div>
              <img src={cit} alt=''className='  img-fluid sp w-100'/>
            </div>
            <div>
              <img src={tes} alt='' className='img-fluid sp w-100'/>
            </div>
            <div>
              <img src={ford} alt='' className='img-fluid sp w-100'/>
            </div>
            
          </Slider>
      </div>
    </section>
)
}

export default Sponsor