/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import data from '../../utils/slider.json';
import "./ListCars.css";
import trans from '../../images/transmission-manuelle.png';
import boite from '../../images/boit.png';
import energ from '../../images/le-carburant.png';
import kil from '../../images/kil.png';
import puis from '../../images/moteur.png';
import date from '../../images/calendrier.png';
import loc from '../../images/location.png';
import Header from '../Header/Header'
import NavBreadcrumbs from '../Breadcrumbs/NavBreadcrumbs';
import Search from '../Search/Search';
import axios from 'axios';

const ListCars = () => {
    const [filteredCars, setFilteredCars] = useState([]);

    useEffect(() => {
        fetchPosts();
    }, []);

    const fetchPosts = async () => {
        try {
            const response = await axios.get('http://localhost:8000/posts');
            const data = await response.data;
            setFilteredCars(data); 
        } catch (error) {
            console.error('Error fetching posts:', error);
        }
    };

    const handleDataFiltered = (filteredData) => {
        setFilteredCars(filteredData);
    };

    const handleLinkClick = (postId) => {
        localStorage.setItem('postId', postId);
    };

    return (
        <>
            <Header activeLink="anonces" setActiveLink={() => {}} />
            <NavBreadcrumbs />
            <Search onDataFiltered={handleDataFiltered} />
            <section id="anonces" className='r-wrapper'>
                <div className='paddings innerWidth r-container' data-aos="fade-up">
                    <div className="r-grid">
                    {filteredCars.map((card) => (
                        <Link to={`/voitures/${encodeURIComponent(card._id)}`} key={card._id} onClick={() => handleLinkClick(card._id)}>
                                <div className='flexColStart r-card'>
                                            {card.files && card.files.length > 0 && ( 
                                                <img src={`http://localhost:8000/posts/carPhotos/${card.files[0]}`} style ={{height:170}}alt="" />
                                            )}
                                            <span className='primaryText'>{card.marque} {card.modele}</span>
                                            <div className='flexRow' style={{ color: 'grey', fontSize: 'smaller' }}>
                                            <ul className="specs-preview list-unstyled" style={{ fontSize:'12px',display: 'grid', gridTemplateColumns: 'repeat(2, 1fr)', gridGap: '7px' ,justifyContent: 'space-between' }}>
                                                <li aria-label="Kms" className="road" style={{ textAlign: 'center', display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
                                                    <div style={{ display: 'flex', alignItems: 'center' }}>
                                                        <img src={kil} alt='' style={{ width: "19px" }} />
                                                        <span style={{ marginLeft: '5px' }}>{card.kilometrage} Km</span>
                                                    </div>
                                                </li>
                                                <li aria-label="Année" className="year" style={{ textAlign: 'center', display: 'flex', alignItems: 'center', justifyContent: 'space-between',marginLeft:'35px'  }}>
                                                    <div style={{ display: 'flex', alignItems: 'center' }}>
                                                        <img src={date} alt='' style={{ width: "19px" }} />
                                                        <span style={{ marginLeft: '5px' }}>{card.annee}</span>
                                                    </div>
                                                </li>
                                                <li aria-label="Boîte vitesses" className="boite" style={{ textAlign: 'center', display: 'flex', alignItems: 'center', justifyContent: 'space-between'  }}>
                                                    <div style={{ display: 'flex', alignItems: 'center' }}>
                                                        <img src={boite} alt='' style={{ width: "19px" }} />
                                                        <span style={{ marginLeft: '5px' }}>{card.boiteDeVitesse}</span>
                                                    </div>
                                                </li>
                                                <li aria-label="Transmission" className="transmission" style={{ textAlign: 'center', display: 'flex', alignItems: 'center', justifyContent: 'space-between',marginLeft:'35px'  }}>
                                                    <div style={{ display: 'flex', alignItems: 'center' }}>
                                                        <img src={trans} alt='' style={{ width: "22px" }} />
                                                        <span style={{ marginLeft: '5px' }}>{card.transmission}</span>
                                                    </div>
                                                </li>
                                                <li aria-label="Puissance fiscale" className="horsepower" style={{ textAlign: 'center', display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
                                                    <div style={{ display: 'flex', alignItems: 'center' }}>
                                                        <img src={puis} alt='' style={{ width: "22px" }} />
                                                        <span style={{ marginLeft: '5px' }}>{card.puissance}</span>
                                                    </div>
                                                </li>
                                                <li aria-label="Energie" className="fuel" style={{ textAlign: 'center', display: 'flex', alignItems: 'center', justifyContent: 'space-between' ,marginLeft:'35px'}}>
                                                    <div style={{ display: 'flex', alignItems: 'center' }}>
                                                        <img src={energ} alt='' style={{ width: "22px" }} />
                                                        <span style={{ marginLeft: '5px' }}>{card.energie}</span>
                                                    </div>
                                                </li>
                                            </ul>
                                            </div>
                                            <div style={{ display: 'flex', alignItems:'center',marginTop:'-10px'}}>
                                                <img src={loc} alt='' style={{ width: "19px" }} />
                                                <span style={{ marginLeft: '5px' ,color:'black',fontSize:"14px",paddingTop:'8px'}}>{card.location}</span>
                                            </div>
                                            <hr style={{ margin: '0', border: 'none', borderTop: '1px solid black', width: '100%' }} />
                                            <div style={{ textAlign: 'right', width: '100%' }}>
                                                <span className='r-price'>
                                                    <span>{card.prix}</span>
                                                    <span style={{ color: "#d38041" }}> DT</span>
                                                </span>
                                            </div>
                                            
                                        </div>
                                    </Link>
                        ))}
                    </div>
                </div>
            </section>
        </>
    );
};

export default ListCars;
