/* eslint-disable no-unused-vars */
import React, { useEffect, useRef, useState } from 'react'
import user from '../../images/user.png'
import {FaPen } from 'react-icons/fa';
import './Profil.css'
import { Link }from 'react-router-dom'
import Header from '../../components/Header/Header';
import Footer from '../../components/Footer/Footer'
import axios from 'axios'
const Profil = () => { 

    const [formData, setUserData] = useState(null);
    const [message, setMessage] = useState('');
    const [imagePreview, setImagePreview] = useState(null);

    useEffect(() => {
        const fetchData = async () => {
            try {
                const userId = localStorage.getItem('userId');
                if (userId) {
                    const response = await axios.get(`http://localhost:8000/auth/users/${userId}`);
                    setUserData(response.data);
                    handleExpertise();
                }
            } catch (error) {
                console.log(error.message);
            }
        };

        fetchData();
    }, []);

    const inputRef = useRef(null);


    const uploadImage = async (file) => {
        const formData = new FormData();
        formData.append('file', file);

        try {
            const token = localStorage.getItem('token'); 
            const response = await axios.post('http://localhost:8000/auth/upload', formData, {
            headers: {
                'Content-Type': 'multipart/form-data',
                'Authorization': `Bearer ${token}` 
            }
            });
            console.log(response.data);
            setUserData({ ...formData, profileImage: response.data.profileImage });
        } catch (error) {
            console.log(error);
        }
    };

    const handleImageClick = () => {
        inputRef.current.click();
    };

    const handleImageChange = (event) => {
        const file = event.target.files[0];
        console.log(file);
        uploadImage(file);
    };

    const [activeItem, setActiveItem] = useState('profil');

    const handleItemClick = (itemName) => {
        setActiveItem(itemName);
    };


    const [expertise, setExpertise]=useState(false)

    const handleExpertise = () => {
        const role = localStorage.getItem('role');
        if (role === 'expert') {
            setExpertise(true);
        }
    };

    // const inputRef=useRef(null)
    // const [image, setImage]=useState("")

    
    // const [imageUrls, setImageUrls] = useState([]);

    // const [imagePreview, setImagePreview] = useState(null);
    // const handleImageChange = (event) => {
    //     const file = event.target.files[0];
    //     console.log(file);
    //     setImage(file);
    //     setImagePreview(URL.createObjectURL(file)); 
    // };
    
    // const handleImageClick = () => {
    //     inputRef.current.click();
    // };
    
    

    return (
    <>
        <Header />
        <section>
            <div className="container py-5 mt-5">
                <div className="row">
                    <div className="col-lg-4 ">
                        <div className="card1 " style={{ height: "auto" }}>
                            <div className="card1-body text-center " style={{ paddingTop: '5px', paddingBottom: '5px' }}>
                                <div className='profimg d-flex align-items-center mb-4' style={{ position: 'relative' }}>
                                        <img
                                            src={formData && formData.profileImage ? `http://localhost:8000/auth/pictures/${formData.profileImage}` : user}
                                            alt="avatar"
                                            className="rounded-circle img-fluid me-2"
                                            style={{ width: '110px', height: '100px', border: '2.5px solid #eea943' }}
                                        />
                                        <div>
                                            <h5 className="my-3">{formData && formData.name}</h5>
                                        </div>
                                </div>
                                <input type="file" ref={inputRef} onChange={handleImageChange} style={{ display: "none" }} />
                                <ul className="list-group list-group-flush rounded-3" style={{margin: '0', padding: '0'}}>
                                    <li className={`list-group-item d-flex align-items-center border-0 ${activeItem === 'profil' ? 'active' : ''}`} 
                                        onClick={() => handleItemClick('profil')}>
                                        <i class="fa-solid fa-gauge-high p-2"></i>
                                        <Link to='/profil' className="link mb-0">Tableau de bord</Link>
                                    </li>
                                    {expertise && (
                                            <li
                                                className={`list-group-item d-flex align-items-center border-0 ${
                                                    activeItem === 'mesads' ? 'active' : ''
                                                }`}
                                                onClick={() => handleItemClick('mesads')}
                                            >
                                                <i className="fa-solid fa-screwdriver-wrench p-2 "></i>
                                                <Link to="/mesdemandes" className="link mb-0 ">
                                                    Demandes d'expertise
                                                </Link>
                                            </li>
                                        )}
                                    <li className={`list-group-item d-flex align-items-center border-0 ${activeItem === 'mesads' ? 'active' : ''}`} 
                                        onClick={() => handleItemClick('mesads')}>
                                        <i class="fa-solid fa-car-rear p-2 "></i> 
                                        <Link to="/mesannonces" className="link mb-0 ">Annonces</Link>
                                    </li>
                                    <li className={`list-group-item d-flex align-items-center border-0 ${activeItem === 'profilpayment' ? 'active' : ''}`} 
                                        onClick={() => handleItemClick('profilpayment')}>
                                        <i class="fa-solid fa-coins p-2 "></i>                                        
                                        <Link to='/profilpayment' className=" link mb-0 ">Paiement</Link>
                                    </li>
                                    <li className={`list-group-item d-flex align-items-center border-0 ${activeItem === 'favoris' ? 'active' : ''}`} 
                                        onClick={() => handleItemClick('favoris')}>
                                        <i class="fa-solid fa-heart p-2 "></i>         
                                        <Link to='/favoris' className=" link mb-0 ">Favoris</Link>
                                    </li>
                                    <li className={`list-group-item d-flex align-items-center border-0 ${activeItem === 'historique' ? 'active' : ''}`} 
                                        onClick={() => handleItemClick('historique')}>
                                        <i class="fa-solid fa-clock-rotate-left p-2 "></i>
                                        <Link to='/historique' className="link mb-0 ">Historique d'achat</Link>
                                    </li>
                                    <li className={`list-group-item d-flex align-items-center border-0 ${activeItem === 'notifs' ? 'active' : ''}`} 
                                        onClick={() => handleItemClick('notifs')}>
                                        <i class="fa-solid fa-bell p-2"></i>         
                                        <Link to='/notifications' className=" link mb-0 ">Notifications</Link>
                                    </li>
                                    <li className={`list-group-item d-flex align-items-center border-0 ${activeItem === 'messages' ? 'active' : ''}`} 
                                        onClick={() => handleItemClick('messages')}>
                                        <i class="fa-solid fa-message p-2 "></i>       
                                        <Link className="link mb-0 ">Messages</Link>
                                    </li>
                                    <li className={`list-group-item d-flex align-items-center border-0 ${activeItem === 'donneesprofil' ? 'active' : ''}`} 
                                        onClick={() => handleItemClick('donneesprofil')}>
                                        <i class="fa-solid fa-user-pen p-2 "></i>   
                                        <Link to="/donneesprofil" className="link mb-0">Profil</Link>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-8">
                        <div className="card1 mb-4">
                            <div class="row">
                                <div style={{marginLeft:'5px',marginBottom:'10px',fontSize:'18px'}}>
                                    <span>Bonjour <strong>{formData && formData.name}</strong>, que voulez-vous faire aujourd'hui?</span>
                                </div>
                                <h6 className='p-3' style={{marginLeft:'5px',fontSize:'18.5px'}}>Vendre</h6>
                                <div class="col-lg-6">
                                    <div class="card mb-4 custom-card">
                                        <Link to="/myads"  style={{ color: 'inherit', textDecoration: 'none' }}><p className='mt-2'>Voir mes annonces</p></Link>
                                        <i class="fa-solid fa-car-rear p-2 card-icon "></i> 
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="card mb-4 custom-card">
                                        <p className='mt-2'>Voir mes notifications</p>
                                        <i class="fa-solid fa-bell p-2 card-icon"></i>    
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <h6 className='p-3' style={{marginLeft:'5px',fontSize:'18.5px'}}>Acheter</h6>
                                <div class="col-lg-6">
                                    <div class="card mb-4 custom-card">
                                        <p className='mt-2'>Voir mes favoris</p>
                                        <i class="fa-solid fa-heart p-2 card-icon"></i>  
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="card mb-4 custom-card">
                                        <Link to="/historique"  style={{ color: 'inherit', textDecoration: 'none' }}><p className='mt-2'>Consulter mon historique d'achat</p></Link>
                                        <i class="fa-solid fa-clock-rotate-left p-2 card-icon"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <h6 className='p-3' style={{marginLeft:'5px',fontSize:'18.5px'}}>Gérer mon profil</h6>
                                <div class="col-lg-6">
                                    <div class="card mb-4 custom-card">
                                        <Link to="/donneesprofil"  style={{ color: 'inherit', textDecoration: 'none' }}><p className='mt-2'>Editer mon profil</p></Link>
                                        <i class="fa-solid fa-user-pen p-2 card-icon"></i> 
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="card mb-4 custom-card">
                                        <p className='mt-2'>Voir mes messages</p>
                                        <i class="fa-solid fa-message p-2 card-icon"></i>    
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-8">
                        <div className="row">
                            <div className="">
                                {/* <div className="">
                                    <div class="card1 ribbon-box">
                                        <div class="ribbon ribbon-primary">Messages</div>
                                        <div class="clearfix"></div>
                                        <div class="inbox-widget">
                                            <a href>
                                                <div class="inbox-item">
                                                    <div class="inbox-item-img"><img src="https://bootdey.com/img/Content/avatar/avatar2.png" class="rounded-circle" alt=""/></div>
                                                    <p class="inbox-item-author">Tomaslau</p>
                                                    <p class="inbox-item-text">I've finished it! See you so...</p>
                                                    <p class="inbox-item-date">
                                                        <button type="button" class="btn btn-icon btn-sm waves-effect waves-light btn-success">Reply</button>
                                                    </p>
                                                </div>
                                            </a>
                                            <a href>
                                                <div class="inbox-item">
                                                    <div class="inbox-item-img"><img src="https://bootdey.com/img/Content/avatar/avatar3.png" class="rounded-circle" alt=""/></div>
                                                    <p class="inbox-item-author">Stillnotdavid</p>
                                                    <p class="inbox-item-text">This theme is awesome!</p>
                                                    <p class="inbox-item-date">
                                                        <button type="button" class="btn btn-icon btn-sm waves-effect waves-light btn-success">Reply</button>
                                                    </p>
                                                </div>
                                            </a>
                                            <a href>
                                                <div class="inbox-item">
                                                    <div class="inbox-item-img"><img src="https://bootdey.com/img/Content/avatar/avatar4.png" class="rounded-circle" alt=""/></div>
                                                    <p class="inbox-item-author">Kurafire</p>
                                                    <p class="inbox-item-text">Nice to meet you</p>
                                                    <p class="inbox-item-date">
                                                        <button type="button" class="btn btn-icon btn-sm waves-effect waves-light btn-success">Reply</button>
                                                    </p>
                                                </div>
                                            </a>
                                            <a href>
                                                <div class="inbox-item">
                                                    <div class="inbox-item-img"><img src="https://bootdey.com/img/Content/avatar/avatar5.png" class="rounded-circle" alt=""/></div>
                                                    <p class="inbox-item-author">Shahedk</p>
                                                    <p class="inbox-item-text">Hey! there I'm available...</p>
                                                    <p class="inbox-item-date">
                                                        <button type="button" class="btn btn-icon btn-sm waves-effect waves-light btn-success">Reply</button>
                                                    </p>
                                                </div>
                                            </a>
                                            <a href>
                                                <div class="inbox-item">
                                                    <div class="inbox-item-img"><img src="https://bootdey.com/img/Content/avatar/avatar6.png" class="rounded-circle" alt=""/></div>
                                                    <p class="inbox-item-author">Adhamdannaway</p>
                                                    <p class="inbox-item-text">This theme is awesome!</p>
                                                    <p class="inbox-item-date">
                                                        <button type="button" class="btn btn-icon btn-sm waves-effect waves-light btn-success">Reply</button>
                                                    </p>
                                                </div>
                                            </a>
                                            <a href>
                                                <div class="inbox-item">
                                                    <div class="inbox-item-img"><img src="https://bootdey.com/img/Content/avatar/avatar2.png" class="rounded-circle" alt=""/></div>
                                                    <p class="inbox-item-author">Tomaslau</p>
                                                    <p class="inbox-item-text">I've finished it! See you so...</p>
                                                    <p class="inbox-item-date">
                                                        <button type="button" class="btn btn-icon btn-sm waves-effect waves-light btn-success">Reply</button>
                                                    </p>
                                                </div>
                                            </a>
                                            <a href>
                                                <div class="inbox-item">
                                                    <div class="inbox-item-img"><img src="https://bootdey.com/img/Content/avatar/avatar3.png" class="rounded-circle" alt=""/></div>
                                                    <p class="inbox-item-author">Stillnotdavid</p>
                                                    <p class="inbox-item-text">This theme is awesome!</p>
                                                    <p class="inbox-item-date">
                                                        <button type="button" class="btn btn-icon btn-sm waves-effect waves-light btn-success">Reply</button>
                                                    </p>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div> */}
                            </div>
                    {/* <div className="col-md-6">
                        <div className="card1 mb-4 mb-md-0">
                            <div className="card1-body">
                                <p className="mb-4"><span className="text-primary font-italic me-1">assigment</span> Project Status
                                </p>
                                <p className="mb-1" style={{ fontSize: '.77rem' }}>Web Design</p>
                                <div className="progress rounded" style={progressBarStyle}>
                                    <div className="progress-bar" role="progressbar" style={{ width: '80%' }} aria-valuenow="80"
                                        aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <p className="mt-4 mb-1" style={{ fontSize: '.77rem' }}>Website Markup</p>
                                <div className="progress rounded" style={progressBarStyle}>
                                    <div className="progress-bar" role="progressbar" style={{ width: '72%' }} aria-valuenow="72"
                                        aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <p className="mt-4 mb-1" style={{ fontSize: '.77rem' }}>One Page</p>
                                <div className="progress rounded" style={progressBarStyle}>
                                    <div className="progress-bar" role="progressbar" style={{ width: '89%' }} aria-valuenow="89"
                                        aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <p className="mt-4 mb-1" style={{ fontSize: '.77rem' }}>Mobile Template</p>
                                <div className="progress rounded" style={progressBarStyle}>
                                    <div className="progress-bar" role="progressbar" style={{ width: '55%' }} aria-valuenow="55"
                                        aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <p className="mt-4 mb-1" style={{ fontSize: '.77rem' }}>Backend API</p>
                                <div className="progress rounded mb-2" style={progressBarStyle}>
                                    <div className="progress-bar" role="progressbar" style={{ width: '66%' }} aria-valuenow="66"
                                        aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                    </div> */}
                    </div>
                </div>
            </div>
        </div>
    </section>
    <Footer />
    </>
    )
}

export default Profil