/* eslint-disable no-unused-vars */
import React, { useContext, useEffect, useState} from 'react';
import axios from 'axios'
import login from '../../images/login.png'
import {useNavigate } from 'react-router-dom';
import './ModelLogin.css';
import { useAuth } from '../../context/AuthContext';
//import { Link, NavLink } from 'react-router-dom';

const ModelLogin = ({ closeModal}) => {
    const [action] = useState("Se connecter");
    const modalClass = action === "Créer votre compte" ? "modalContainer expanded" : "modalContainer";
    const [showResetPasswordForm, setShowResetPasswordForm] = useState(false);

    const{
        authUser,setAuthUser,isLoggedIn,setIsLoggedIn
    }= useAuth()
    
    const [formData, setFormData] = useState({
        name: '',
        email: '',
        phone: '',
        password: '',
        address:'',
        profileImage:'',
        expertCertif:'',
        code:'',
        newPassword:''
    });


    const [errors, setErrors]=useState({});
    const history = useNavigate();
    //const[isSubmit, setIsSubmit]=useState(false);
    // const [backendError, setBackendError] = useState("");


    const handleChange = (e) => {
        setFormData({
            ...formData,
            [e.target.name]: e.target.value
        });
        console.log(formData);
    };

    const ClickSignup = () => {
        window.history.pushState({}, '', '/inscription');
    }

    const ClickLogin = () => {
        window.history.pushState({}, '', '/connexion');
    }

    const handleCloseModal = () => {
        closeModal(false); 
    };

    const handleSubmitSignUp = async (e) => {
        e.preventDefault();
        setErrors({}); 
    
        try {
            const response = await axios.post('http://localhost:8000/auth/signup', formData);
            console.log('Signup successful:', response.data); 
            const userId = response.data.newUser.userId; 
            const token =response.data.token;
            console.log(token)
            console.log(userId);
            localStorage.setItem('userId', userId);
            localStorage.setItem('token', token);
            localStorage.setItem('role', response.data.newUser.role);
            if (response.data.newUser.role === 'expert' && response.data.newUser.expert) {
                localStorage.setItem('idExpert', response.data.newUser.expert._id);
            }           
            setAuthUser(response.data.newUser);
            setIsLoggedIn(true);
            history(`/profil`, { formData });      
            
        } catch (error) {
            console.log('Error response:', error.response);
            if (error.response && error.response.data && error.response.data.error) {
                const responseData = error.response.data.error;
                const newErrors = {};
                Object.keys(responseData).forEach((field) => {
                    newErrors[field] = responseData[field][0]; 
                });
                setErrors({ ...newErrors, backend: error.response.data.message });
            } else {
                const errorMessage = error.response.data.message || "Une erreur s'est produite lors de l'inscription.";

                alert(errorMessage); 
            }
            console.error('Signup error:', error); 
        }
    };

    const handleSubmitLogin = async (e) => {
        e.preventDefault();
        setErrors({}); 
    
        try {
            const response = await axios.post('http://localhost:8000/auth/login', formData);
            console.log('Login successful:', response.data); 
            const userId = response.data.user.userId; 
            const token =response.data.token;
            console.log(token)
            console.log(userId);
            localStorage.setItem('userId', userId);
            localStorage.setItem('token', token);
            localStorage.setItem('role', response.data.user.role);
            if (response.data.user.role === 'expert' && response.data.user.expert) {
                localStorage.setItem('idExpert', response.data.user.expert._id);
            }           
            setAuthUser(response.data.user);
            setIsLoggedIn(true);
            history(`/profil`, { formData });         
    
        } catch (error) {
            console.log('Error response:', error.response);
            if (error.response && error.response.data && error.response.data.error) {
                const responseData = error.response.data.error;
                const newErrors = {};
                Object.keys(responseData).forEach((field) => {
                    newErrors[field] = responseData[field][0]; 
                });
                setErrors({ ...newErrors, backend: error.response.data.message }); 
            } else if (error.response && error.response.data && error.response.data.message) {
                setErrors({ backend: error.response.data.message });
            } else {
                const errorMessage = "Une erreur s'est produite lors de la connexion.";
                alert(errorMessage); 
            }
            console.error('Login error:', error); 
        }
    };

       // const handleSubmitResetPasswordDemand = async (e) => {
    //     e.preventDefault();
    //     try {
    //         const response = await axios.post('http://localhost:8000/auth/reset-password', formData);
    //         console.log('Reset password demand sent:', response.data); 
    //         // Afficher le deuxième formulaire pour la confirmation de réinitialisation de mot de passe
    //         setShowConfirmationForm(true);
    //     } catch (error) {
    //         console.error('Reset password demand error:', error); 
    //         // Gérer les erreurs
    //     }
    // };

    const [showConfirmationForm, setShowConfirmationForm] = useState(false);


    const handleSubmitResetPasswordDemand = async (e) => {
        e.preventDefault();
        setErrors({}); 
    
        try {
            const response = await axios.post('http://localhost:8000/auth/reset-password', formData);
            console.log('Password reset email sent:', response.data); 
            localStorage.setItem('code',response.data.code);
            setShowConfirmationForm(true); 
            // setFormData({ ...formData, code: '' });
        } catch (error) {
            console.log('Error response:', error.response);
            if (error.response && error.response.data && error.response.data.error) {
                const responseData = error.response.data.error;
                const newErrors = {};
                Object.keys(responseData).forEach((field) => {
                    newErrors[field] = responseData[field][0]; 
                });
                setErrors({ ...newErrors, backend: error.response.data.message }); 
            } else {
                const errorMessage = error.response.data.message || "Une erreur s'est produite .";
                alert(errorMessage); 
            }
            console.error('Password reset error:', error); 
        }
    };
    
    
    const handleSubmitResetPasswordConfirmation = async (e) => {
        e.preventDefault();
        setErrors({});
        try {
            if (formData.code !== localStorage.getItem('code')) {
                setErrors({ code: "Incorrect code" });
                return;
            }
            const response = await axios.post('http://localhost:8000/auth/reset-password-confirmation', formData);
            console.log('Password is reseted:', response.data); 
            setShowConfirmationForm(true); 
            setFormData({ email: '', password: '', code: '' });
            window.history.pushState({}, '', '/connexion'); 
            setShowResetPasswordForm(false);
        } catch (error) {
            console.log('Error response:', error.response);
            if (error.response && error.response.data && error.response.data.error) {
                const responseData = error.response.data.error;
                const newErrors = {};
                Object.keys(responseData).forEach((field) => {
                    newErrors[field] = responseData[field][0]; 
                });
                setErrors({ ...newErrors, backend: error.response.data.message });
            } else {
                const errorMessage = error.response.data.message || "Une erreur s'est produite .";
                alert(errorMessage); 
            }
            console.error('Reset password confirmation error:', error); 
        }
    };
    


    const handleShowResetPasswordForm = () => {
        setShowResetPasswordForm(true);
        window.history.pushState({}, '', '/motdepasseoublié');
    };

    const handleHideResetPasswordForm = () => {
        setShowResetPasswordForm(false);
        window.history.pushState({}, '', '/connexion');
    };

    const [showPassword, setShowPassword] = useState(false);
    const togglePasswordVisibility = () => {
        setShowPassword(!showPassword);
    };


    return (
        <div className='modalBackground'>
            <div className={modalClass}>
                <div className='CloseBtn'>
                    <button onClick={handleCloseModal}>&#x2716;</button>
                </div>
                <div className="containerLog modalContainer">
                    <input type="checkbox" id="flip" />
                    <div className="cover">
                        <div className="front">
                            <img src={login} alt="" />
                        </div>
                        <div className="back">
                            <img src={login} alt="" />
                        </div>
                    </div>
                    <div className="forms">
                        <div className="login-signup-forms">
                        <div className="form-content">
                        {showResetPasswordForm ? (
                        <div className="reset-password-form">
                            <div className='rowback' onClick={handleHideResetPasswordForm}><i className="bi bi-arrow-left"></i></div>
                            <div className="title">Mot de passe oublié</div>
                            {showResetPasswordForm && !showConfirmationForm ? (
                                <form onSubmit={handleSubmitResetPasswordDemand}>
                                    <div className="input-boxes">
                                        <div className="input-box">
                                            <i className="fas fa-envelope"></i>
                                            <input 
                                                type="text" 
                                                placeholder="Email"
                                                required
                                                value={formData.email}
                                                onChange={handleChange}
                                                name='email' 
                                            />
                                        </div>
                                        <div className="input-box">
                                            <button type="submit">Envoyer</button>
                                        </div>
                                    </div>
                                </form>             
                            ) : (
                                <form onSubmit={handleSubmitResetPasswordConfirmation}>
                                    <div className="input-boxes">
                                        <div className="input-box">
                                            <i className="fas fa-lock"></i>
                                            <input type="text" placeholder="Code de réinitialisation"
                                                required
                                                value={formData.code}
                                                onChange={handleChange}
                                                name='code' />
                                        </div>
                                        {formData.code !== '' && formData.code !== localStorage.getItem('code') && (
                                            <p className="error">Le code est incorrect. <a style={{color:"black", textDecoration:'underline' , cursor:'pointer', padding:8}}href onClick={handleSubmitResetPasswordDemand}>Renvoyer le code</a></p>
                                        )}
                                        {errors.code && <p className="error">{errors.code}</p>}
                                        {formData.code === localStorage.getItem('code') && (
                                            <>
                                                <div className="input-box">
                                                    <i className="fas fa-lock"></i>
                                                    <input type="password" placeholder="Nouveau mot de passe"
                                                        required
                                                        value={formData.password}
                                                        onChange={handleChange}
                                                        name='password' />
                                                </div>
                                                {errors.password && <p className="error">{errors.password}</p>}
                                                <div className="input-box">
                                                    <button type="submit" onClick={ClickLogin}>Réinitialiser le mot de passe</button>
                                                </div>
                                            </>
                                        )}
                                    </div>
                                </form>
                            )}
                        </div>
                    ) : (
                        <div className="login-form">
                                    <div className="title">Se connecter</div>
                                    <form onSubmit={handleSubmitLogin} >
                                        <div className="input-boxes">
                                            <div className="input-box">
                                                <i className="fas fa-envelope"></i>
                                                <input type="text" placeholder="Email"
                                                    required
                                                    value={formData && formData.email}
                                                    onChange={handleChange}
                                                    name='email' />
                                            </div>
                                            {errors.email && <p className="error">{errors.email}</p>}
                                            <div className="input-box">
                                                <i className="fas fa-lock"></i>
                                                <input 
                                                    type={showPassword ? "text" : "password"} 
                                                    placeholder="Mot de passe"
                                                    required
                                                    value={formData && formData.password}
                                                    onChange={handleChange}
                                                    name='password' />
                                                <i className={`fas ${showPassword ? "fa-eye" : "fa-eye-slash"} eye-icon`} style={{color:"rgb(191, 188, 188)", fontSize:14 , cursor:'pointer'}} onClick={togglePasswordVisibility}></i> 
                                            </div>

                                            {errors.password && <p className="error">{errors.password}</p>}                                        
                                            {/* Condition pour afficher le message d'erreur de backend */}
                                            {errors.backend && <p className="error"><i class="bi bi-exclamation-triangle-fill prob"></i>{errors.backend}</p>}
                                            <div className="text" onClick={handleShowResetPasswordForm}><a href>Mot de passe oublié ?</a></div>
                                            <div className="input-box">
                                                <button type="submit">Se connecter</button>
                                            </div>
                                            <div className="text sign-up-text">Vous n'avez pas de compte ? <label htmlFor="flip" onClick={ClickSignup}>S'inscrire</label></div>
                                        </div>
                                    </form>
                                </div>
                            )}
                            <div className="signup-form">
                                <div className="title">S'inscrire</div>
                                <form onSubmit={handleSubmitSignUp}>
                                    <div className="input-boxes">
                                        <div className="input-box">
                                            <i className="fas fa-user"></i>
                                            <input type="text" placeholder="Nom"
                                                required
                                                value={formData && formData.name}
                                                onChange={handleChange}
                                                name='name' />
                                        </div>
                                        {errors.name && <p className="error">{errors.name}</p>}
                                            <div className="input-box">
                                            <i className="fas fa-envelope"></i>
                                            <input type="text" placeholder="Email"
                                                required
                                                value={formData && formData.email}
                                                onChange={handleChange}
                                                name='email' />
                                        </div>
                                        {errors.email && <p className="error">{errors.email}</p>}
                                        <div className="input-box">
                                            <i className="fas fa-phone"></i>
                                            <input type="text" placeholder="Téléphone"
                                                required
                                                value={formData && formData.phone}
                                                onChange={handleChange}
                                                name="phone" />
                                        </div>
                                        {errors.phone && <p className="error">{errors.phone}</p>}
                                        <div className="input-box">
                                            <i className="fas fa-lock"></i>
                                            <input 
                                                    type={showPassword ? "text" : "password"} 
                                                    placeholder="Mot de passe"
                                                    required
                                                    value={formData && formData.password}
                                                    onChange={handleChange}
                                                    name='password' />
                                            <i className={`fas ${showPassword ? "fa-eye" : "fa-eye-slash"} eye-icon`} style={{color:"rgb(191, 188, 188)", fontSize:14 , cursor:'pointer'}} onClick={togglePasswordVisibility}></i> 
                                        </div>
                                        {errors.password && <p className="error">{errors.password}</p>}                                        
                                        {/* Condition pour afficher le message d'erreur de backend */}
                                        {errors.backend && <p className="error"><i class="bi bi-exclamation-triangle-fill prob"></i>{errors.backend}</p>}
                                        <div className="input-box">
                                            <button type="submit">S'inscrire</button>
                                        </div>
                                        
                                        <div className="text sign-up-text pb-5">Vous avez déjà un compte ? <label htmlFor="flip" onClick={ClickLogin}>Se connecter</label></div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ModelLogin;