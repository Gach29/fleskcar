import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css';
import Header from './components/Header/Header'
import Hero from './components/Hero/Hero'
import About from './components/About/About'
import Counts from './components/Counts/Counts'
import Car from './components/Cars/Car'
import Expert from './components/Expert/Expert'
import Sponsor from'./components/Sponsors/Sponsor'
import Vente from './components/Vente/Vente'
import Footer from './components/Footer/Footer'

export default function Home() {
    return (
        <div>
            <div className='home' id='acceuil'>
                <Header />
                <Hero />
            </div>
            <About />
            <Counts/>
            <Car />
            <Expert />
            <Sponsor />
            <Vente  />
            <Footer />
        </div>
    )   
    }
